<?php
return array (
  'ActionHistoryRecord' => 
  array (
    'fields' => 
    array (
      'user' => 'Usuario',
      'action' => 'Acción',
      'createdAt' => 'Fecha',
      'userType' => 'User Type',
      'target' => 'Interés',
      'targetType' => 'Tipo de Interés',
      'authToken' => 'Clave de Autorización',
      'ipAddress' => 'Dirección IP',
      'authLogRecord' => 'Registro en Hist. de Aut.',
    ),
    'links' => 
    array (
      'authToken' => 'Clave de Autorización',
      'authLogRecord' => 'Registro en Hist. de Aut.',
      'user' => 'Usuario',
      'target' => 'Interés',
    ),
    'presetFilters' => 
    array (
      'onlyMy' => 'Sólo para Mi',
    ),
    'options' => 
    array (
      'action' => 
      array (
        'read' => 'Leer',
        'update' => 'Actualizar',
        'delete' => 'Borrar',
        'create' => 'Crear',
      ),
    ),
  ),
  'Admin' => 
  array (
    'labels' => 
    array (
      'Enabled' => 'Activado',
      'Disabled' => 'Desactivado',
      'System' => 'Sistema',
      'Users' => 'Usuarios',
      'Email' => 'Correo',
      'Data' => 'Datos',
      'Customization' => 'Personalizar',
      'Available Fields' => 'Campos Disponibles',
      'Layout' => 'Diseño',
      'Entity Manager' => 'Entidades',
      'Add Panel' => 'Agregar Panel',
      'Add Field' => 'Agregar Campo',
      'Settings' => 'Ajustes',
      'Scheduled Jobs' => 'Tareas Agendadas',
      'Upgrade' => 'Actualizar',
      'Clear Cache' => 'Borrar Cache',
      'Rebuild' => 'Reconstruir',
      'Teams' => 'Equipos',
      'Roles' => 'Roles',
      'Portal' => 'Portal',
      'Portals' => 'Portales',
      'Portal Roles' => 'Roles',
      'Portal Users' => 'Usuarios',
      'API Users' => 'Usuarios de la API',
      'Outbound Emails' => 'Correos Salientes',
      'Group Email Accounts' => 'Grupo de Cuentas de Correo',
      'Personal Email Accounts' => 'Cuentas Personales',
      'Inbound Emails' => 'Correos Entrantes',
      'Email Templates' => 'Plantillas de Correo',
      'Import' => 'Importación',
      'Layout Manager' => 'Formatos',
      'User Interface' => 'Interfaz de Usuario',
      'Auth Tokens' => 'Clave de Aut.',
      'Auth Log' => 'Historial de Autorizaciones',
      'Authentication' => 'Autorización',
      'Currency' => 'Moneda',
      'Integrations' => 'Integracion',
      'Extensions' => 'Extensiones',
      'Webhooks' => 'Webhooks',
      'Dashboard Templates' => 'Dashboard Templates',
      'Upload' => 'Subir',
      'Installing...' => 'Instalando...',
      'Upgrading...' => 'Actualizando',
      'Upgraded successfully' => 'Actualización exitosa',
      'Installed successfully' => 'Instalado exitosamente',
      'Ready for upgrade' => 'Listo para actualizar',
      'Run Upgrade' => 'Ejecutar actualización',
      'Install' => 'Instalar',
      'Ready for installation' => 'Listo para instalación',
      'Uninstalling...' => 'Desinstalando...',
      'Uninstalled' => 'Desinstalado',
      'Create Entity' => 'Crear Entidad',
      'Edit Entity' => 'Editar Entidad',
      'Create Link' => 'Crear Enlace',
      'Edit Link' => 'Editar Enlace',
      'Notifications' => 'Notificaciones',
      'Jobs' => 'Trabajos',
      'Reset to Default' => 'Restablecer valores default',
      'Email Filters' => 'Filtros de Correo',
      'Action History' => 'Historial',
      'Label Manager' => 'Etiquetas',
      'Template Manager' => 'Administrador de Plantillas',
      'Lead Capture' => 'Capturar Referencia',
      'Attachments' => 'Adjuntos ',
      'System Requirements' => 'Requerimientos del Sistema',
      'PDF Templates' => 'PDF Templates',
      'PHP Settings' => 'Configuración PHP',
      'Database Settings' => 'Configuración de la Base de Datos',
      'Permissions' => 'Permisos',
      'Email Addresses' => 'Email Addresses',
      'Phone Numbers' => 'Phone Numbers',
      'Success' => 'Correcto',
      'Fail' => 'Falló',
      'is recommended' => 'es recomendado',
      'extension is missing' => 'falta la extensión ',
      'Workflow Manager' => 'Workflows',
    ),
    'layouts' => 
    array (
      'list' => 'Lista',
      'detail' => 'Detalle',
      'listSmall' => 'Lista (Pequeña)',
      'detailSmall' => 'Detalle (Pequeño)',
      'detailPortal' => 'Detalle (Portal)',
      'detailSmallPortal' => 'Detalle (Pequeño, Portal)',
      'listSmallPortal' => 'Lista (Pequeño, Portal)',
      'listPortal' => 'Lista (Portal)',
      'relationshipsPortal' => 'Paneles de Relaciones (Portal)',
      'filters' => 'Filtros de Búsqueda',
      'massUpdate' => 'Actualización Masiva',
      'relationships' => 'Paneles de Relaciones',
      'defaultSidePanel' => 'Side Panel Fields',
      'sidePanelsDetail' => 'Paneles auxiliares (detalle)',
      'sidePanelsEdit' => 'Paneles auxiliares (editar)',
      'sidePanelsDetailSmall' => 'Paneles auxiliares (detalle pequeño)',
      'sidePanelsEditSmall' => 'Paneles auxiliares (editar pequeño)',
      'kanban' => 'Tarjetas',
      'detailConvert' => 'Convertir Referencia',
      'listForAccount' => 'Listado (por Cuentas)',
      'listForContact' => 'Lista (para Contactos)',
    ),
    'fieldTypes' => 
    array (
      'address' => 'Dirección',
      'array' => 'Arreglo',
      'foreign' => 'Externo',
      'duration' => 'Periodo',
      'password' => 'Contraseña',
      'personName' => 'Nombre',
      'autoincrement' => 'Auto-incremento',
      'bool' => 'Sí/No',
      'currency' => 'Moneda',
      'currencyConverted' => 'Moneda (Convertida)',
      'date' => 'Fecha',
      'datetime' => 'Fecha-Hr',
      'datetimeOptional' => 'Fecha/Fecha-Hr',
      'email' => 'Correo',
      'enum' => 'Lista',
      'enumInt' => 'Lista Enteros',
      'enumFloat' => 'Lista Numérica',
      'float' => 'Numérico',
      'int' => 'Entero',
      'link' => 'Liga',
      'linkMultiple' => 'Ligas',
      'linkParent' => 'Liga Orígen',
      'linkOne' => 'Link One',
      'phone' => 'Teléfono',
      'text' => 'Texto',
      'url' => 'Dirección Web',
      'varchar' => 'Varchar',
      'file' => 'Archivo',
      'image' => 'Imagen',
      'multiEnum' => 'Lista Múltiple',
      'attachmentMultiple' => 'Adjuntos',
      'rangeInt' => 'Rango Entero',
      'rangeFloat' => 'Rango Numérico',
      'rangeCurrency' => 'Rango de Moneda',
      'wysiwyg' => 'Wysiwyg',
      'map' => 'Mapa',
      'number' => 'Número (auto-incremeto)',
      'colorpicker' => 'Selector de Colores',
      'checklist' => 'Checklist',
      'barcode' => 'Barcode',
      'jsonArray' => 'Arreglo Json',
      'jsonObject' => 'Objeto Json',
    ),
    'fields' => 
    array (
      'type' => 'Tipo',
      'name' => 'Nombre',
      'label' => 'Etiqueta',
      'tooltipText' => 'Texto de Ayuda',
      'required' => 'Requerido',
      'default' => 'Default',
      'maxLength' => 'Longitud máxima',
      'options' => 'Opciones',
      'after' => 'Posterior al Campo',
      'before' => 'Anterior al Campo',
      'link' => 'Enlace',
      'field' => 'Campo',
      'min' => 'Mínimo',
      'max' => 'Máximo',
      'translation' => 'Traducción',
      'previewSize' => 'Tamaño de Vista Previa',
      'noEmptyString' => 'No se permite el campo vacío',
      'defaultType' => 'Tipo Default',
      'seeMoreDisabled' => 'Desactivar cortar texto',
      'cutHeight' => 'Cut Height (px)',
      'entityList' => 'Lista de Entidades',
      'isSorted' => 'Esta ordenado (alfabeticamente)',
      'audited' => 'Auditada',
      'trim' => 'Recortado',
      'height' => 'Altura (px)',
      'minHeight' => 'Altura Min (px)',
      'provider' => 'Proveedor',
      'typeList' => 'Lista de Tipos',
      'rows' => 'Num. de renglones del área de texto',
      'lengthOfCut' => 'Longitud del recorte',
      'sourceList' => 'Lista de Fuentes',
      'prefix' => 'Prefijo',
      'nextNumber' => 'Siguiente Número',
      'padLength' => 'Longitud del Panel',
      'disableFormatting' => 'Desactivar Formateo',
      'dynamicLogicVisible' => 'Condiciones que hacen visible al campo',
      'dynamicLogicReadOnly' => 'Condiciones que hacen el campo de solo-lectura',
      'dynamicLogicRequired' => 'Condiciones que hacen el campo obligatorio',
      'dynamicLogicOptions' => 'Opciones condicionales',
      'probabilityMap' => 'Probabilidades de la Etapa (%)',
      'notActualOptions' => 'Not Actual Options',
      'readOnly' => 'Solo-lectura',
      'maxFileSize' => 'Tamaño máximo (Mb)',
      'isPersonalData' => 'Son Datos Personales',
      'useIframe' => 'Usar iFrame',
      'useNumericFormat' => 'Use Formato numérico ',
      'strip' => 'Limpiar',
      'minuteStep' => 'Minutes Step',
      'inlineEditDisabled' => 'Deshabilitar edición en linea',
      'allowCustomOptions' => 'Allow Custom Options',
      'displayAsLabel' => 'Mostrar como etiqueta',
      'displayAsList' => 'Display as List',
      'maxCount' => 'Max Item Count',
      'accept' => 'Accept',
      'viewMap' => 'View Map Button',
      'codeType' => 'Code Type',
      'lastChar' => 'Last Character',
      'displayRawText' => 'Display raw text (no markdown)',
    ),
    'messages' => 
    array (
      'upgradeVersion' => 'EspoCRM se actualizará a la versión &lt;strong&gt;{version}&lt;/strong&gt;.  Por favor espere unos minutos.',
      'upgradeDone' => 'EspoCRM fué actualizado a la versión &lt;strong&gt;{version}&lt;/strong&gt;.',
      'upgradeBackup' => 'Le recomendamos hacer un respaldo de sus datos y sistema EspoCRM antes de actualizarlo.',
      'thousandSeparatorEqualsDecimalMark' => 'El caracter separador de miles no puede ser el mismo que el separador decimal.',
      'userHasNoEmailAddress' => 'Este usuario no tiene correo de contacto.',
      'selectEntityType' => 'Seleccione el tipo de entidad en el menú de la izquierda.',
      'selectUpgradePackage' => 'Seleccione el Paquete de Actualización',
      'downloadUpgradePackage' => 'Descargue los paquetes de actualización desde &lt;a href="{url}"&gt;aquí&lt;/a&gt;.',
      'selectLayout' => 'Seleccione el diseño en el menú de la izquierda, para editarlo.',
      'selectExtensionPackage' => 'Seleccionar extensión del paquete',
      'extensionInstalled' => 'La Extensión {name} {version} ha sido instalada',
      'installExtension' => 'La Extensión {name} {version} está lista para instalar.',
      'cronIsNotConfigured' => 'No se están ejecutando las tareas programadas. Por lo cual los correos enviados, notificaciones y alarmas no están funcionando.  Por favor siga las {instructions}
(https://www.espocrm.com/documentation/administration/server-configuration/#user-content-setup-a-crontab) para activar los cron jobs. ',
      'newVersionIsAvailable' => 'Hay una nueva versión disponible de EspoCRM. ({latestVersion}).',
      'newExtensionVersionIsAvailable' => 'Nueva versión {latestVersion} disponible para {extensionName}. ',
      'uninstallConfirmation' => '¿Realmente quiere desinstalar esta extensión?',
      'upgradeInfo' => 'Check the [documentation]({url}) about how to upgrade your EspoCRM instance.',
      'upgradeRecommendation' => 'This way of upgrading is not recommended. It\'s better to upgrade from CLI.',
    ),
    'descriptions' => 
    array (
      'settings' => 'Configuración del sistema de aplicación.',
      'scheduledJob' => 'Trabajos que se ejecutan automáticamente (cron Jobs).',
      'jobs' => 'Jobs execute tasks in the background.',
      'upgrade' => 'Actualizar EspoCRM.',
      'clearCache' => 'Borrar Cache del Servidor.',
      'rebuild' => 'Borrar y regenerar el Cache del Servidor.',
      'users' => 'Administración de Usuarios.',
      'teams' => 'Administración de Equipos',
      'roles' => 'Administración de Roles',
      'portals' => 'Manejo de Portales',
      'portalRoles' => 'Roles en el Portal',
      'portalUsers' => 'Usuarios del portal.',
      'outboundEmails' => 'Opciones SMTP para correo saliente.',
      'groupEmailAccounts' => 'Grupo de Cuentas Correo IMAP, importación de correos y correos por caso.',
      'personalEmailAccounts' => 'Cuentas de correo de Usuarios',
      'emailTemplates' => 'Plantillas para mensajes de Correo de salida.',
      'import' => 'Importar desde archivo CSV.',
      'layoutManager' => 'Personalizar diseños (listas, detalles, editar, buscar, actualización masiva).',
      'entityManager' => 'Crear y editar entidades personalizadas.  Administrar campos y relaciones.',
      'userInterface' => 'Configurar la Interfaz del Usuario',
      'authTokens' => 'Sesiones certificas activas. Direcciones IP y última fecha de acceso',
      'authentication' => 'Opciones de autorización',
      'currency' => 'Opciones y tarifas de Moneda',
      'extensions' => 'Instalar o desinstalar extensiones',
      'integrations' => 'Integración con servicios de terceros.',
      'notifications' => 'Ajustes de notificaciones del correo y la aplicación.',
      'inboundEmails' => 'Configuración de cuentas de Correo de entrada.',
      'emailFilters' => 'Los mensajes de correo que cumplan con el filtro indicado, no se importarán.',
      'actionHistory' => 'Historial de acciones del usuario.',
      'labelManager' => 'Personalizar etiquetas de aplicación',
      'templateManager' => 'Personalizar plantillas de mensajes.',
      'authLog' => 'Historial de Ingresos',
      'leadCapture' => 'Puntos de entrada de la API para Web-a-Ref',
      'attachments' => 'Todos los archivos adjuntos fueron guardados en el sistema. ',
      'systemRequirements' => 'Requerimientos del Sistema para EspoCRM.',
      'apiUsers' => 'Separar usuarios para integración de grupos.',
      'webhooks' => 'Manage webhooks.',
      'emailAddresses' => 'All emailes addresses stored in the system.',
      'phoneNumbers' => 'All phone numbers stored in the system.',
      'dashboardTemplates' => 'Deploy dashboards to users.',
      'pdfTemplates' => 'Templates for printing to PDF.',
      'workflowManager' => 'Configure Workflow rules.',
    ),
    'options' => 
    array (
      'previewSize' => 
      array (
        'x-small' => 'Muy Pequeño',
        'small' => 'Pequeño',
        'medium' => 'Mediano',
        'large' => 'Grande',
      ),
    ),
    'logicalOperators' => 
    array (
      'and' => 'Y',
      'or' => 'O',
      'not' => 'NO',
    ),
    'systemRequirements' => 
    array (
      'requiredPhpVersion' => 'Versión PHP',
      'requiredMysqlVersion' => 'Versión MySQL',
      'requiredMariadbVersion' => 'MariaDB version',
      'host' => 'Nombre del Hospedaje',
      'dbname' => 'Nombre de la Base de Datos',
      'user' => 'Nombre del Usuario',
      'writable' => 'Permite grabar',
      'readable' => 'Permite leer',
    ),
    'templates' => 
    array (
      'accessInfo' => 'Información de Acceso',
      'accessInfoPortal' => 'Información de Acceso a Portales',
      'assignment' => 'Asignación',
      'mention' => 'Mención',
      'noteEmailRecieved' => 'Nota sobre el Correo Recibido',
      'notePost' => 'Nota sobre la Publicación',
      'notePostNoParent' => 'Nota sobre la Publicación (No el Padre)',
      'noteStatus' => 'Nota sobre el Estado de la Actualización',
      'passwordChangeLink' => 'Liga para Cambiar Contraseña',
      'invitation' => 'Invitación',
      'reminder' => 'Recordatorio',
    ),
  ),
  'ApiUser' => 
  array (
    'labels' => 
    array (
      'Create ApiUser' => 'Crear Usuario de la API',
    ),
  ),
  'Attachment' => 
  array (
    'fields' => 
    array (
      'role' => 'Rol',
      'related' => 'Relacionado ',
      'file' => 'Archivo ',
      'type' => 'Tipo ',
      'field' => 'Campo ',
      'sourceId' => 'ID Origen',
      'storage' => 'Almacenamiento ',
      'size' => 'Tamaño ',
    ),
    'options' => 
    array (
      'role' => 
      array (
        'Attachment' => 'Adjunto ',
        'Inline Attachment' => 'Adjunto inmediato ',
        'Import File' => 'Importar Archivo',
        'Export File' => 'Exportar Archivo',
        'Mail Merge' => 'Generar Correos',
        'Mass Pdf' => 'PDF Masivo',
      ),
    ),
    'insertFromSourceLabels' => 
    array (
      'Document' => 'Insertar documento',
    ),
    'presetFilters' => 
    array (
      'orphan' => 'Huérfano ',
    ),
  ),
  'AuthLogRecord' => 
  array (
    'fields' => 
    array (
      'username' => 'Nombre del Usuario',
      'ipAddress' => 'Dirección IP',
      'requestTime' => 'Hr. de la Solicitud',
      'createdAt' => 'Fecha de la Solicitud',
      'isDenied' => 'Fue denegado',
      'denialReason' => 'Razón de denegación',
      'portal' => 'Portal',
      'user' => 'Usuario',
      'authToken' => 'Clave de Aut. creada',
      'requestUrl' => 'URL de la Solicitud',
      'requestMethod' => 'Método de la Solicitud',
      'authTokenIsActive' => 'La clave de aut. está activa',
      'authenticationMethod' => 'Método de Autenticación',
    ),
    'links' => 
    array (
      'authToken' => 'Clave de aut. creada',
      'user' => 'Usuario',
      'portal' => 'Portal',
      'actionHistoryRecords' => 'Historial de Acciones',
    ),
    'presetFilters' => 
    array (
      'denied' => 'Denegado',
      'accepted' => 'Aceptado',
    ),
    'options' => 
    array (
      'denialReason' => 
      array (
        'CREDENTIALS' => 'Credenciales inválidas',
        'INACTIVE_USER' => 'Usuario inactivo',
        'IS_PORTAL_USER' => 'Usuario del Portal',
        'IS_NOT_PORTAL_USER' => 'No es un usuario del portal',
        'USER_IS_NOT_IN_PORTAL' => 'El usuario no se relaciona con el portal',
      ),
    ),
  ),
  'AuthToken' => 
  array (
    'fields' => 
    array (
      'user' => 'Usuario',
      'ipAddress' => 'Dirección IP',
      'lastAccess' => 'Fecha Último Acceso',
      'createdAt' => 'Fecha de Creación',
      'isActive' => 'Está Activo',
      'portal' => 'Portal',
    ),
    'links' => 
    array (
      'actionHistoryRecords' => 'Historial',
    ),
    'presetFilters' => 
    array (
      'active' => 'Activo',
      'inactive' => 'Inactivo',
    ),
    'labels' => 
    array (
      'Set Inactive' => 'Activar',
    ),
    'massActions' => 
    array (
      'setInactive' => 'Desactivar',
    ),
  ),
  'Currency' => 
  array (
    'names' => 
    array (
      'AED' => 'United Arab Emirates Dirham',
      'AFN' => 'Afghan Afghani',
      'ALL' => 'Albanian Lek',
      'AMD' => 'Armenian Dram',
      'ANG' => 'Netherlands Antillean Guilder',
      'AOA' => 'Angolan Kwanza',
      'ARS' => 'Argentine Peso',
      'AUD' => 'Australian Dollar',
      'AWG' => 'Aruban Florin',
      'AZN' => 'Azerbaijani Manat',
      'BAM' => 'Bosnia-Herzegovina Convertible Mark',
      'BBD' => 'Barbadian Dollar',
      'BDT' => 'Bangladeshi Taka',
      'BGN' => 'Bulgarian Lev',
      'BHD' => 'Bahraini Dinar',
      'BIF' => 'Burundian Franc',
      'BMD' => 'Bermudan Dollar',
      'BND' => 'Brunei Dollar',
      'BOB' => 'Bolivian Boliviano',
      'BOV' => 'Bolivian Mvdol',
      'BRL' => 'Brazilian Real',
      'BSD' => 'Bahamian Dollar',
      'BTN' => 'Bhutanese Ngultrum',
      'BWP' => 'Botswanan Pula',
      'BYN' => 'Belarusian Ruble',
      'BZD' => 'Belize Dollar',
      'CAD' => 'Canadian Dollar',
      'CDF' => 'Congolese Franc',
      'CHE' => 'WIR Euro',
      'CHF' => 'Swiss Franc',
      'CHW' => 'WIR Franc',
      'CLF' => 'Chilean Unit of Account (UF)',
      'CLP' => 'Chilean Peso',
      'CNH' => 'Chinese Yuan (offshore)',
      'CNY' => 'Chinese Yuan',
      'COP' => 'Colombian Peso',
      'COU' => 'Colombian Real Value Unit',
      'CRC' => 'Costa Rican Colón',
      'CUC' => 'Cuban Convertible Peso',
      'CUP' => 'Cuban Peso',
      'CVE' => 'Cape Verdean Escudo',
      'CZK' => 'Czech Koruna',
      'DJF' => 'Djiboutian Franc',
      'DKK' => 'Danish Krone',
      'DOP' => 'Dominican Peso',
      'DZD' => 'Algerian Dinar',
      'EGP' => 'Egyptian Pound',
      'ERN' => 'Eritrean Nakfa',
      'ETB' => 'Ethiopian Birr',
      'EUR' => 'Euro',
      'FJD' => 'Fijian Dollar',
      'FKP' => 'Falkland Islands Pound',
      'GBP' => 'British Pound',
      'GEL' => 'Georgian Lari',
      'GHS' => 'Ghanaian Cedi',
      'GIP' => 'Gibraltar Pound',
      'GMD' => 'Gambian Dalasi',
      'GNF' => 'Guinean Franc',
      'GTQ' => 'Guatemalan Quetzal',
      'GYD' => 'Guyanaese Dollar',
      'HKD' => 'Hong Kong Dollar',
      'HNL' => 'Honduran Lempira',
      'HRK' => 'Croatian Kuna',
      'HTG' => 'Haitian Gourde',
      'HUF' => 'Hungarian Forint',
      'IDR' => 'Indonesian Rupiah',
      'ILS' => 'Israeli New Shekel',
      'INR' => 'Indian Rupee',
      'IQD' => 'Iraqi Dinar',
      'IRR' => 'Iranian Rial',
      'ISK' => 'Icelandic Króna',
      'JMD' => 'Jamaican Dollar',
      'JOD' => 'Jordanian Dinar',
      'JPY' => 'Japanese Yen',
      'KES' => 'Kenyan Shilling',
      'KGS' => 'Kyrgystani Som',
      'KHR' => 'Cambodian Riel',
      'KMF' => 'Comorian Franc',
      'KPW' => 'North Korean Won',
      'KRW' => 'South Korean Won',
      'KWD' => 'Kuwaiti Dinar',
      'KYD' => 'Cayman Islands Dollar',
      'KZT' => 'Kazakhstani Tenge',
      'LAK' => 'Laotian Kip',
      'LBP' => 'Lebanese Pound',
      'LKR' => 'Sri Lankan Rupee',
      'LRD' => 'Liberian Dollar',
      'LSL' => 'Lesotho Loti',
      'LYD' => 'Libyan Dinar',
      'MAD' => 'Moroccan Dirham',
      'MDL' => 'Moldovan Leu',
      'MGA' => 'Malagasy Ariary',
      'MKD' => 'Macedonian Denar',
      'MMK' => 'Myanmar Kyat',
      'MNT' => 'Mongolian Tugrik',
      'MOP' => 'Macanese Pataca',
      'MRO' => 'Mauritanian Ouguiya',
      'MUR' => 'Mauritian Rupee',
      'MWK' => 'Malawian Kwacha',
      'MXN' => 'Mexican Peso',
      'MXV' => 'Mexican Investment Unit',
      'MYR' => 'Malaysian Ringgit',
      'MZN' => 'Mozambican Metical',
      'NAD' => 'Namibian Dollar',
      'NGN' => 'Nigerian Naira',
      'NIO' => 'Nicaraguan Córdoba',
      'NOK' => 'Norwegian Krone',
      'NPR' => 'Nepalese Rupee',
      'NZD' => 'New Zealand Dollar',
      'OMR' => 'Omani Rial',
      'PAB' => 'Panamanian Balboa',
      'PEN' => 'Peruvian Sol',
      'PGK' => 'Papua New Guinean Kina',
      'PHP' => 'Philippine Piso',
      'PKR' => 'Pakistani Rupee',
      'PLN' => 'Polish Zloty',
      'PYG' => 'Paraguayan Guarani',
      'QAR' => 'Qatari Rial',
      'RON' => 'Romanian Leu',
      'RSD' => 'Serbian Dinar',
      'RUB' => 'Russian Ruble',
      'RWF' => 'Rwandan Franc',
      'SAR' => 'Saudi Riyal',
      'SBD' => 'Solomon Islands Dollar',
      'SCR' => 'Seychellois Rupee',
      'SDG' => 'Sudanese Pound',
      'SEK' => 'Swedish Krona',
      'SGD' => 'Singapore Dollar',
      'SHP' => 'St. Helena Pound',
      'SLL' => 'Sierra Leonean Leone',
      'SOS' => 'Somali Shilling',
      'SRD' => 'Surinamese Dollar',
      'SSP' => 'South Sudanese Pound',
      'STN' => 'São Tomé & Príncipe Dobra (2018)',
      'SYP' => 'Syrian Pound',
      'SZL' => 'Swazi Lilangeni',
      'SVC' => 'Salvadoran Colón',
      'THB' => 'Thai Baht',
      'TJS' => 'Tajikistani Somoni',
      'TND' => 'Tunisian Dinar',
      'TOP' => 'Tongan Paʻanga',
      'TRY' => 'Turkish Lira',
      'TTD' => 'Trinidad & Tobago Dollar',
      'TWD' => 'New Taiwan Dollar',
      'TZS' => 'Tanzanian Shilling',
      'UAH' => 'Ukrainian Hryvnia',
      'UGX' => 'Ugandan Shilling',
      'USD' => 'US Dollar',
      'USN' => 'US Dollar (Next day)',
      'UYI' => 'Uruguayan Peso (Indexed Units)',
      'UYU' => 'Uruguayan Peso',
      'UZS' => 'Uzbekistani Som',
      'VEF' => 'Venezuelan Bolívar',
      'VND' => 'Vietnamese Dong',
      'VUV' => 'Vanuatu Vatu',
      'WST' => 'Samoan Tala',
      'XAF' => 'Central African CFA Franc',
      'XCD' => 'East Caribbean Dollar',
      'XOF' => 'West African CFA Franc',
      'XPF' => 'CFP Franc',
      'YER' => 'Yemeni Rial',
      'ZAR' => 'South African Rand',
      'ZMW' => 'Zambian Kwacha',
      'ZWL' => 'Zimbabwe Dollar',
    ),
  ),
  'DashboardTemplate' => 
  array (
    'fields' => 
    array (
      'layout' => 'Layout',
      'append' => 'Append (don\'t remove user\'s tabs)',
    ),
    'links' => 
    array (
    ),
    'labels' => 
    array (
      'Create DashboardTemplate' => 'Create Template',
      'Deploy to Users' => 'Deploy to Users',
      'Deploy to Team' => 'Deploy to Team',
    ),
  ),
  'DashletOptions' => 
  array (
    'fields' => 
    array (
      'title' => 'Título',
      'dateFrom' => 'Fecha desde',
      'dateTo' => 'Fecha hasta',
      'autorefreshInterval' => 'Intervalo de actualización',
      'displayRecords' => 'Mostrar Registros',
      'isDoubleHeight' => 'Altitud 2x',
      'mode' => 'Modo',
      'enabledScopeList' => 'Qué mostrar',
      'users' => 'Usuarios',
      'entityType' => 'Tipo de Entidad',
      'primaryFilter' => 'Filtro Primario',
      'boolFilterList' => 'Filtros Adicionales',
      'sortBy' => 'Campo para Ordenar',
      'sortDirection' => 'Ordenar (dirección)',
      'expandedLayout' => 'Formato',
      'skipOwn' => 'Don\'t show own records',
      'url' => 'URL',
      'dateFilter' => 'Filtro de Fecha',
      'futureDays' => 'Siguientes \'n\' Días',
      'useLastStage' => 'Agrupar por la última etapa lograda',
      'report' => 'Report',
      'column' => 'Summation Column',
    ),
    'options' => 
    array (
      'mode' => 
      array (
        'agendaWeek' => 'Semana (agenda)',
        'basicWeek' => 'Semana',
        'month' => 'Mes',
        'basicDay' => 'Día',
        'agendaDay' => 'Día (agenda)',
        'timeline' => 'Cronograma',
      ),
    ),
    'messages' => 
    array (
      'selectEntityType' => 'Seleccionar el Tipo de Entidad en las opciones del panel.',
    ),
    'tooltips' => 
    array (
      'skipOwn' => 'Actions made by your user account won\'t be displayed.',
    ),
  ),
  'DynamicLogic' => 
  array (
    'labels' => 
    array (
      'Field' => 'Campo',
    ),
    'options' => 
    array (
      'operators' => 
      array (
        'equals' => 'Igual a',
        'notEquals' => 'Diferente de',
        'greaterThan' => 'Mayor que',
        'lessThan' => 'Menor que',
        'greaterThanOrEquals' => 'Mayor o igual a',
        'lessThanOrEquals' => 'Menor o igual que',
        'in' => 'En',
        'notIn' => 'No en',
        'inPast' => 'En Pasado',
        'inFuture' => 'Es Futuro',
        'isToday' => 'Es Hoy',
        'isTrue' => 'Es Verdadero',
        'isFalse' => 'Es Falso',
        'isEmpty' => 'Está Vacío',
        'isNotEmpty' => 'No está vacío',
        'contains' => 'Contiene',
        'notContains' => 'No Contiene',
        'has' => 'Contiene',
        'notHas' => 'No Contiene',
      ),
    ),
  ),
  'Email' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre (Sujeto)',
      'parent' => 'Padre',
      'status' => 'Estátus',
      'dateSent' => 'Enviado',
      'from' => 'De',
      'to' => 'Para',
      'cc' => 'CC',
      'bcc' => 'BCC',
      'replyTo' => 'Responder a',
      'replyToString' => 'Responder a (String)',
      'personStringData' => 'Person String Data',
      'isHtml' => 'Es Html',
      'body' => 'Cuerpo',
      'bodyPlain' => 'Cuerpo (plano)',
      'subject' => 'Asunto',
      'attachments' => 'Adjuntos',
      'selectTemplate' => 'Seleccione una Plantilla',
      'fromEmailAddress' => 'From Address (link)',
      'toEmailAddresses' => 'Direcciones (Para)',
      'emailAddress' => 'Dirección de Correo',
      'deliveryDate' => 'Fecha Entrega',
      'account' => 'Cuenta',
      'users' => 'Usuarios',
      'replied' => 'Respondió',
      'replies' => 'Respuestas',
      'isRead' => 'Fue leído',
      'isNotRead' => 'No Leído',
      'isImportant' => 'Es Importante',
      'isReplied' => 'Tiene Respuesta',
      'isNotReplied' => 'No Tiene Respuesta',
      'isUsers' => 'Es del Usuario',
      'inTrash' => 'En el Basurero',
      'sentBy' => 'Enviado por',
      'folder' => 'Carpeta',
      'inboundEmails' => 'Cuentas de Grupo',
      'emailAccounts' => 'Cuentas Personales',
      'hasAttachment' => 'Tiene Adjuntos',
      'assignedUsers' => 'Usuarios Asignados',
      'ccEmailAddresses' => 'Direcciones CC',
      'bccEmailAddresses' => 'Direcciones (CCO)',
      'replyToEmailAddresses' => 'Direcciones (Responder)',
      'messageId' => 'Id del Mensaje',
      'messageIdInternal' => 'Id del Mensaje (Interna)',
      'folderId' => 'Id de la Carpeta',
      'fromName' => 'Nombre (De)',
      'fromString' => 'String (De)',
      'fromAddress' => 'De la dirección',
      'replyToName' => 'Reply-To Name',
      'replyToAddress' => 'Reply-To Address',
      'isSystem' => 'Es del Sistema',
    ),
    'links' => 
    array (
      'replied' => 'Respondió',
      'replies' => 'Respuestas',
      'inboundEmails' => 'Cuentas de Grupo',
      'emailAccounts' => 'Cuentas Personales',
      'assignedUsers' => 'Usuarios Asignados',
      'sentBy' => 'Enviado por',
      'attachments' => 'Adjuntos',
      'fromEmailAddress' => 'Cuentas de Correo (De)',
      'toEmailAddresses' => 'Cuentas de Correo (Para)',
      'ccEmailAddresses' => 'Cuentas de Correo (CC)',
      'bccEmailAddresses' => 'Cuentas de Correo (CCO)',
      'replyToEmailAddresses' => 'Direcciones (Responder)',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Draft' => 'Borrador',
        'Sending' => 'Enviando',
        'Sent' => 'Enviado',
        'Archived' => 'Archivado',
        'Received' => 'Recibido',
        'Failed' => 'Falló',
      ),
    ),
    'labels' => 
    array (
      'Create Email' => 'Archivar Correo',
      'Archive Email' => 'Archivar Correo',
      'Compose' => 'Nuevo',
      'Reply' => 'Responder',
      'Reply to All' => 'Responder a Todos',
      'Forward' => 'Reenviar',
      'Original message' => 'Mensaje Original',
      'Forwarded message' => 'Mensaje reenviado',
      'Email Accounts' => 'Cuentas de Correo Personales',
      'Inbound Emails' => 'Agrupar Cuentas de Correo',
      'Email Templates' => 'Plantillas de Correo',
      'Send Test Email' => 'Enviar Correo de Prueba',
      'Send' => 'Enviar',
      'Email Address' => 'Correo',
      'Mark Read' => 'Marcar como Leído',
      'Sending...' => 'Enviando...',
      'Save Draft' => 'Guardar Borrador',
      'Mark all as read' => 'Marcar todos como leídos',
      'Show Plain Text' => 'Ver en texto plano',
      'Mark as Important' => 'Marcar como Importante',
      'Unmark Importance' => 'Marcar como No Importante',
      'Move to Trash' => 'Mover al Basurero',
      'Retrieve from Trash' => 'Recuperar del Basurero',
      'Move to Folder' => 'Mover a la Carpeta',
      'Filters' => 'Filtros',
      'Folders' => 'Carpetas',
      'No Subject' => 'No Subject',
      'View Users' => 'View Users',
      'Create Lead' => 'Crear Referencia',
      'Create Contact' => 'Crear Contacto',
      'Add to Contact' => 'Agregar a Contactos',
      'Add to Lead' => 'Agregar a Referencias',
      'Create Task' => 'Crear Tarea',
      'Create Case' => 'Crear Caso',
    ),
    'messages' => 
    array (
      'noSmtpSetup' => 'No está configurado el SMTP. {link}.',
      'testEmailSent' => 'Correo de prueba enviado',
      'emailSent' => 'Correo enviado',
      'savedAsDraft' => 'Guardado como borrador',
      'sendConfirm' => 'Send the email?',
      'confirmInsertTemplate' => 'El cuerpo del correo se perderá. ¿Realmente desea insertar la plantilla?',
    ),
    'presetFilters' => 
    array (
      'sent' => 'Enviado',
      'archived' => 'Archivado',
      'inbox' => 'Bandeja de Entrada',
      'drafts' => 'Borradores',
      'trash' => 'Basurero',
      'important' => 'Importante',
    ),
    'massActions' => 
    array (
      'markAsRead' => 'Mark as Read',
      'markAsNotRead' => 'Marcar como No Leído',
      'markAsImportant' => 'Marcar como Importante',
      'markAsNotImportant' => 'Marcar como No Importante',
      'moveToTrash' => 'Mover al Basurero',
      'moveToFolder' => 'Mover a la Carpeta',
      'retrieveFromTrash' => 'Recuperar del Basurero',
    ),
  ),
  'EmailAccount' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estado',
      'host' => 'Servidor',
      'username' => 'Nombre de Usuario',
      'password' => 'Contraseña',
      'port' => 'Puerto',
      'monitoredFolders' => 'Carpetas Supervisadas',
      'ssl' => 'SSL',
      'fetchSince' => 'Obtener Desde',
      'emailAddress' => 'Dirección de Correo',
      'sentFolder' => 'Carpeta de Enviados',
      'storeSentEmails' => 'Almacenar Correos Enviados',
      'keepFetchedEmailsUnread' => 'Mantener los correos obtenidos sin leer',
      'emailFolder' => 'Poner en la Carpeta',
      'useImap' => 'Obtener Correos',
      'useSmtp' => 'Use SMTP',
      'smtpHost' => 'Servidor SMTP',
      'smtpPort' => 'Puerto SMTP',
      'smtpAuth' => 'Cuenta SMTP',
      'smtpSecurity' => 'Seguridad SMTP',
      'smtpAuthMechanism' => 'SMTP Auth Mechanism',
      'smtpUsername' => 'Usuario SMTP',
      'smtpPassword' => 'Contraseña SMTP',
    ),
    'links' => 
    array (
      'filters' => 'Filtros',
      'emails' => 'Correos',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Active' => 'Activo',
        'Inactive' => 'Inactivo',
      ),
      'smtpAuthMechanism' => 
      array (
        'plain' => 'PLAIN',
        'login' => 'LOGIN',
        'crammd5' => 'CRAM-MD5',
      ),
    ),
    'labels' => 
    array (
      'Create EmailAccount' => 'Crear Cuenta de Correo',
      'IMAP' => 'IMAP',
      'Main' => 'Principal',
      'Test Connection' => 'Probar conexión',
      'Send Test Email' => 'Enviar Correo de Prueba',
      'SMTP' => 'SMTP',
    ),
    'messages' => 
    array (
      'couldNotConnectToImap' => 'No se pudo conectar con el servidor IMAP',
      'connectionIsOk' => 'Conexión correcta',
    ),
    'tooltips' => 
    array (
      'monitoredFolders' => 'Si usa varias carpetas, debe separarlas con coma',
      'storeSentEmails' => 'Los correos enviados serán guardados en el servidor IMAP.  El campo de  dirección del correo deberá coincidir con las direcciones de los correos que serán enviados.',
    ),
  ),
  'EmailAddress' => 
  array (
    'labels' => 
    array (
      'Primary' => 'Primario',
      'Opted Out' => 'Rechazado',
      'Invalid' => 'Inválido',
    ),
    'fields' => 
    array (
      'optOut' => 'Opted Out',
      'invalid' => 'Invalid',
    ),
    'presetFilters' => 
    array (
      'orphan' => 'Orphan',
    ),
  ),
  'EmailFilter' => 
  array (
    'fields' => 
    array (
      'from' => 'De',
      'to' => 'Para',
      'subject' => 'Asunto',
      'bodyContains' => 'Contenido del Cuerpo',
      'action' => 'Acción',
      'isGlobal' => 'Es Global',
      'emailFolder' => 'Carpeta',
    ),
    'labels' => 
    array (
      'Create EmailFilter' => 'Crear Filtro de Correo',
      'Emails' => 'Correos',
    ),
    'options' => 
    array (
      'action' => 
      array (
        'Skip' => 'Ignorar',
        'Move to Folder' => 'Carpeta',
      ),
    ),
    'tooltips' => 
    array (
      'name' => 'Indique un nombre descriptivo del filtro.',
      'subject' => 'Use el comodín *:

texto*  - inicia con \'texto\',
*texto* - contiene \'texto\',
*texto  - termina en \'text\'.',
      'bodyContains' => 'El cuerpo del correo contiene alguna de la palabras o frases especificadas.',
      'from' => 'Los correos enviados desde la dirección especificada. Dejar en blanco si no es necesario.',
      'to' => 'Los correos electrónicos que se envían a la dirección especificada. Dejar en blanco si no es necesario.',
      'isGlobal' => 'Aplicar este filtro a todos los correos entrantes del sistema.',
    ),
  ),
  'EmailFolder' => 
  array (
    'fields' => 
    array (
      'skipNotifications' => 'Saltar Notificaciones',
    ),
    'labels' => 
    array (
      'Create EmailFolder' => 'Crear Carpeta',
      'Manage Folders' => 'Carpetas',
      'Emails' => 'Correos',
    ),
  ),
  'EmailTemplate' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estado',
      'isHtml' => 'Es HTML',
      'body' => 'Cuerpo',
      'subject' => 'Asunto',
      'attachments' => 'Adjuntos',
      'insertField' => 'Insertar Campo',
      'oneOff' => 'Único',
      'category' => 'Categoría',
    ),
    'links' => 
    array (
    ),
    'labels' => 
    array (
      'Create EmailTemplate' => 'Crear Plantilla de Correo',
      'Info' => 'Información',
      'Available placeholders' => 'Marcadores disponibles',
    ),
    'messages' => 
    array (
      'infoText' => 'Marcadores disponibles:

{optOutUrl} &#8211; Dirección URL para deslistarse;

{optOutLink} &#8211; una liga para deslistarse.',
    ),
    'tooltips' => 
    array (
      'oneOff' => 'Compruebe si usted va a utilizar esta plantilla sólo una vez. Por ejemplo: para Correo Masivo.',
    ),
    'presetFilters' => 
    array (
      'actual' => 'Actuales',
    ),
    'placeholderTexts' => 
    array (
      'today' => 'Fecha de hoy',
      'now' => 'Fecha y hora actual',
      'currentYear' => 'Año actual',
      'optOutUrl' => 'Dirección URL para deslistarse',
      'optOutLink' => 'una liga para deslistarse',
    ),
  ),
  'EmailTemplateCategory' => 
  array (
    'labels' => 
    array (
      'Create EmailTemplateCategory' => 'Crear Categoría',
      'Manage Categories' => 'Administrar Categorías',
      'EmailTemplates' => 'Formatos de Correo',
    ),
    'fields' => 
    array (
      'order' => 'Ordenar',
      'childList' => 'Lista de Hijos',
    ),
    'links' => 
    array (
      'emailTemplates' => 'Formatos de Correo',
    ),
  ),
  'EntityManager' => 
  array (
    'labels' => 
    array (
      'Fields' => 'Campos',
      'Relationships' => 'Relaciones',
      'Schedule' => 'Agenda',
      'Log' => 'Historial',
      'Formula' => 'Fórmula',
    ),
    'fields' => 
    array (
      'name' => 'Nombre',
      'type' => 'Tipo',
      'labelSingular' => 'Etiqueta en Singular',
      'labelPlural' => 'Etiqueta en Plural',
      'stream' => 'Flujo',
      'label' => 'Etiqueta',
      'linkType' => 'Tipo de enlace',
      'entityForeign' => 'Entidad Foránea',
      'linkForeign' => 'Enlace Foráneo',
      'link' => 'Enlace',
      'labelForeign' => 'Etiqueta Foránea',
      'sortBy' => 'Orden Default (campo)',
      'sortDirection' => 'Orden Default (dirección)',
      'relationName' => 'Nombre de la Tabla Intermedia',
      'linkMultipleField' => 'Ligar Varios Campos',
      'linkMultipleFieldForeign' => 'Ligar Varios Campos Foráneos',
      'disabled' => 'Desactivado',
      'textFilterFields' => 'Campos de Filtros de Texto',
      'audited' => 'Auditado',
      'auditedForeign' => 'Auditado Externamente',
      'statusField' => 'Campo de Estátus',
      'beforeSaveCustomScript' => 'Antes de Guardar el Código Personalizado',
      'color' => 'Color',
      'kanbanViewMode' => 'Vista por Tarjetas',
      'kanbanStatusIgnoreList' => 'Grupos ignorados en la vista por Tarjetas',
      'iconClass' => 'Icono',
      'countDisabled' => 'Disable record count',
      'fullTextSearch' => 'Búsqueda por Texto',
      'parentEntityTypeList' => 'Parent Entity Types',
      'foreignLinkEntityTypeList' => 'Foreign Links',
    ),
    'options' => 
    array (
      'type' => 
      array (
        '' => '(vacío)',
        'Base' => 'Base',
        'Person' => 'Persona',
        'CategoryTree' => 'Árbol de Categorías',
        'Event' => 'Evento',
        'BasePlus' => 'Base Plus',
        'Company' => 'Empresa',
      ),
      'linkType' => 
      array (
        'manyToMany' => 'Muchos-a-Muchos',
        'oneToMany' => 'Uno-a-Muchos',
        'manyToOne' => 'Muchos-a-uno',
        'oneToOneRight' => 'One-to-One Right',
        'oneToOneLeft' => 'One-to-One Left',
        'parentToChildren' => 'Padres-a-Hijos',
        'childrenToParent' => 'Hijos-a-Padres',
      ),
      'sortDirection' => 
      array (
        'asc' => 'Ascendente',
        'desc' => 'Descendente',
      ),
    ),
    'messages' => 
    array (
      'entityCreated' => 'La entidad ha sido creada',
      'linkAlreadyExists' => 'Conflicto de nombres en el enlace.',
      'linkConflict' => 'Ya existe un enlace con el mismo nombra.',
    ),
    'tooltips' => 
    array (
      'statusField' => 'Los cambios en este campo serán registrados en su flujo',
      'textFilterFields' => 'Campos usados por la búsqueda de texto',
      'stream' => 'Si la entidad tiene Flujo.',
      'disabled' => 'Verifique si ya no necesita esta entidad en su sistema.',
      'linkAudited' => 'La creación de registros relacionados y su liga con con registros existentes se registrará en su flujo.',
      'linkMultipleField' => 'El campo \'Multi-Ligas\' es una forma fácil de editar relaciones.  No lo uses si tienes muchos registros.',
      'entityType' => 'Base Plus - tiene páneles de Actividades, Historial y Tareas.

Evento - disponible en los páneles de Calendario y Actividades',
      'countDisabled' => 'Total number won\'t be displayed on the list view. Can decrease loading time when the DB table is big.',
      'fullTextSearch' => 'Se requiere regenerar',
    ),
  ),
  'Export' => 
  array (
    'fields' => 
    array (
      'exportAllFields' => 'Exportar todos los campos',
      'fieldList' => 'Lista de Campos',
      'format' => 'Formato',
    ),
    'options' => 
    array (
      'format' => 
      array (
        'csv' => 'CSV',
        'xlsx' => 'XLSX (Excel)',
      ),
    ),
  ),
  'Extension' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'version' => 'Version',
      'description' => 'Descripción',
      'isInstalled' => 'Instalado',
      'checkVersionUrl' => 'URL para buscar nuevas versiones ',
    ),
    'labels' => 
    array (
      'Uninstall' => 'Desinstalar',
      'Install' => 'Instalar',
    ),
    'messages' => 
    array (
      'uninstalled' => 'La extension {name} ha sido desinstalada',
    ),
  ),
  'ExternalAccount' => 
  array (
    'labels' => 
    array (
      'Connect' => 'Conectar',
      'Disconnect' => 'Disconnect',
      'Disconnected' => 'Disconnected',
      'Connected' => 'Conectado',
    ),
    'help' => 
    array (
    ),
    'options' => 
    array (
      'calendarDefaultEntity' => 
      array (
        'Call' => 'Call',
        'Meeting' => 'Meeting',
      ),
      'calendarDirection' => 
      array (
        'EspoToGC' => 'One-way: EspoCRM -&gt; Google Calendar',
        'GCToEspo' => 'One-way: Google Calendar -&gt; EspoCRM',
        'Both' => 'Two-way',
      ),
    ),
    'fields' => 
    array (
      'calendarStartDate' => 'Sync since',
      'calendarEntityTypes' => 'Sync Entities and Identification Labels',
      'calendarDirection' => 'Direction',
      'calendarMonitoredCalendars' => 'Other Calendars',
      'calendarMainCalendar' => 'Main Calendar',
      'calendarDefaultEntity' => 'Default Entity',
      'removeGoogleCalendarEventIfRemovedInEspo' => 'Remove Google Calendar Event upon removal in EspoCRM',
    ),
    'tooltips' => 
    array (
      'calendarEntityTypes' => 'For type recognizing  event\'s name has to start from identification label. Label for default entity can be empty. Recommendation: Do not change identification labels after you saved synchronization setting',
      'calendarDefaultEntity' => 'Unrecognized Event will be loaded as selected Entity',
    ),
  ),
  'FieldManager' => 
  array (
    'labels' => 
    array (
      'Dynamic Logic' => 'Lógica Dinámica',
      'Name' => 'Nombre',
      'Label' => 'Etiqueta',
      'Type' => 'Tipo',
    ),
    'options' => 
    array (
      'dateTimeDefault' => 
      array (
        '' => 'Ninguno',
        'javascript: return this.dateTime.getNow(1);' => 'Hoy',
        'javascript: return this.dateTime.getNow(5);' => 'Hoy (5m)',
        'javascript: return this.dateTime.getNow(15);' => 'Hoy (15 m)',
        'javascript: return this.dateTime.getNow(30);' => 'Hoy (30 m)',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(1, \'hours\', 15);' => '+1 hora',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(2, \'hours\', 15);' => '+2 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(3, \'hours\', 15);' => '+3 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(4, \'hours\', 15);' => '+4 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(5, \'hours\', 15);' => '+5 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(6, \'hours\', 15);' => '+6 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(7, \'hours\', 15);' => '+7 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(8, \'hours\', 15);' => '+8 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(9, \'hours\', 15);' => '+9 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(10, \'hours\', 15);' => '+10 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(11, \'hours\', 15);' => '+11 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(12, \'hours\', 15);' => '+12 horas',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(1, \'days\', 15);' => '+1 día',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(2, \'days\', 15);' => '+2 días',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(3, \'days\', 15);' => '+3 días',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(4, \'days\', 15);' => '+4 días',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(5, \'days\', 15);' => '+5 días',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(6, \'days\', 15);' => '+6 días',
        'javascript: return this.dateTime.getDateTimeShiftedFromNow(1, \'week\', 15);' => '+1 semana',
      ),
      'dateDefault' => 
      array (
        '' => 'Ninguno',
        'javascript: return this.dateTime.getToday();' => 'Hoy',
        'javascript: return this.dateTime.getDateShiftedFromToday(1, \'days\');' => '+1 día',
        'javascript: return this.dateTime.getDateShiftedFromToday(2, \'days\');' => '+2 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(3, \'days\');' => '+3 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(4, \'days\');' => '+4 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(5, \'days\');' => '+5 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(6, \'days\');' => '+6 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(7, \'days\');' => '+7 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(8, \'days\');' => '+8 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(9, \'days\');' => '+9 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(10, \'days\');' => '+10 días',
        'javascript: return this.dateTime.getDateShiftedFromToday(1, \'weeks\');' => '+1 semana',
        'javascript: return this.dateTime.getDateShiftedFromToday(2, \'weeks\');' => '+2 semanas',
        'javascript: return this.dateTime.getDateShiftedFromToday(3, \'weeks\');' => '+3 semanas',
        'javascript: return this.dateTime.getDateShiftedFromToday(1, \'months\');' => '+1 mes',
        'javascript: return this.dateTime.getDateShiftedFromToday(2, \'months\');' => '+2 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(3, \'months\');' => '+3 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(4, \'months\');' => '+4 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(5, \'months\');' => '+5 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(6, \'months\');' => '+6 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(7, \'months\');' => '+7 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(8, \'months\');' => '+8 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(9, \'months\');' => '+9 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(10, \'months\');' => '+10 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(11, \'months\');' => '+11 meses',
        'javascript: return this.dateTime.getDateShiftedFromToday(1, \'year\');' => '+1 año',
      ),
      'barcodeType' => 
      array (
        'EAN13' => 'EAN-13',
        'EAN8' => 'EAN-8',
        'EAN5' => 'EAN-5',
        'EAN2' => 'EAN-2',
        'UPC' => 'UPC (A)',
        'UPCE' => 'UPC (E)',
        'pharmacode' => 'Pharmacode',
        'QRcode' => 'QR code',
      ),
    ),
    'tooltips' => 
    array (
      'audited' => 'Las actualizaciones se registrarán en el Flujo',
      'required' => 'El campo será obligatorio.  No puede ir vacío.',
      'default' => 'Se asignará el valor default al crearlo.',
      'min' => 'Valor mínimo permitido.',
      'max' => 'Valor máximo permitido.',
      'seeMoreDisabled' => 'Si no se marca, los textos largos serán recortados.',
      'lengthOfCut' => 'Que tan largo puede ser el texto antes de ser recortado.',
      'maxLength' => 'Tamaño máximo acepable del texto.',
      'before' => 'La fecha capturada debe ser anterior a la del campo que indique aquí.',
      'after' => 'La fecha capturada debe ser posterior a la del campo que indique aquí',
      'readOnly' => 'El valor del campo no puede ser especificado por el usuario.  Pero puede ser calculado por formula.',
      'fileAccept' => 'Which file types to accept. It\'s possible to add custom items.',
      'barcodeLastChar' => 'For EAN-13 type.',
      'maxFileSize' => 'Vacío o es 0, ilimitado.',
    ),
    'fieldParts' => 
    array (
      'address' => 
      array (
        'street' => 'Calle',
        'city' => 'Ciudad',
        'state' => 'Estado',
        'country' => 'País',
        'postalCode' => 'Código Postal',
        'map' => 'Mapa',
      ),
      'personName' => 
      array (
        'salutation' => 'Saludo',
        'first' => 'Nombre',
        'middle' => 'Middle',
        'last' => 'Apellido',
      ),
      'currency' => 
      array (
        'converted' => '(Convertido)',
        'currency' => '(Moneda)',
      ),
      'datetimeOptional' => 
      array (
        'date' => 'Fecha',
      ),
    ),
  ),
  'Global' => 
  array (
    'scopeNames' => 
    array (
      'Email' => 'Correo electrónico',
      'User' => 'Usuario',
      'Team' => 'Equipo',
      'Role' => 'Rol',
      'EmailTemplate' => 'Plantilla de Correo',
      'EmailTemplateCategory' => 'Categorías de Formatos de Correo',
      'EmailAccount' => 'Cuenta de Correo',
      'EmailAccountScope' => 'Cuenta de Correo',
      'OutboundEmail' => 'Correo Saliente',
      'ScheduledJob' => 'Tarea Agendada',
      'ExternalAccount' => 'Cuenta Externa',
      'Extension' => 'Extension',
      'Dashboard' => 'Tablero',
      'InboundEmail' => 'Correo Entrante',
      'Stream' => 'Flujo',
      'Import' => 'Importar',
      'Template' => 'Plantilla',
      'Job' => 'Trabajo',
      'EmailFilter' => 'Filtro de correo',
      'Portal' => 'Portal',
      'PortalRole' => 'Rol del Portal',
      'Attachment' => 'Datos adjuntos',
      'EmailFolder' => 'Carpeta del Correo',
      'PortalUser' => 'Portal del Usuario',
      'ApiUser' => 'Usuario de la API',
      'ScheduledJobLogRecord' => 'Historial de Tareas Agendadas',
      'PasswordChangeRequest' => 'Solicitar Cambio de Contraseña',
      'ActionHistoryRecord' => 'Historial de Acciones',
      'AuthToken' => 'Clave de Autorización',
      'UniqueId' => 'ID Único',
      'LastViewed' => 'Ultimo Visto',
      'Settings' => 'Configuración',
      'FieldManager' => 'Campos',
      'Integration' => 'Integración',
      'LayoutManager' => 'Formatos',
      'EntityManager' => 'Entidades',
      'Export' => 'Exportar',
      'DynamicLogic' => 'Lógica Dinámica',
      'DashletOptions' => 'Opciones del Panel',
      'Admin' => 'Admin',
      'Global' => 'Global',
      'Preferences' => 'Preferencias',
      'EmailAddress' => 'Dirección de Correo',
      'PhoneNumber' => 'Teléfono',
      'AuthLogRecord' => 'Registro en Hist. de Aut.',
      'AuthFailLogRecord' => 'Registro en Hist. de Fallos de Aut.',
      'LeadCapture' => 'Punto de Entrada para Captura de Referencias',
      'LeadCaptureLogRecord' => 'Historial de Captura de Referencias',
      'ArrayValue' => 'Valor del Arreglo ',
      'DashboardTemplate' => 'Dashboard Template',
      'Currency' => 'Currency',
      'Webhook' => 'Webhook',
      'Account' => 'Cuenta',
      'Contact' => 'Contacto',
      'Lead' => 'Referencia',
      'Target' => 'Interés',
      'Opportunity' => 'Oportunidad',
      'Meeting' => 'Presentación',
      'Calendar' => 'Calendario',
      'Call' => 'Llamada',
      'Task' => 'Tarea',
      'Case' => 'Caso',
      'Document' => 'Documento',
      'DocumentFolder' => 'Carpeta de Documento',
      'Campaign' => 'Campaña',
      'TargetList' => 'Lista de Intereses',
      'MassEmail' => 'Correo Masivo',
      'EmailQueueItem' => 'Item en Cola de Correo',
      'CampaignTrackingUrl' => 'Seguimiento de URLs',
      'Activities' => 'Actividades',
      'KnowledgeBaseArticle' => 'Artículo de la Base de Conocimientos',
      'KnowledgeBaseCategory' => 'Categoría de la Base de Conocimientos',
      'CampaignLogRecord' => 'Historial de Campañas',
      'Workflow' => 'Workflow',
      'Report' => 'Report',
      'Product' => 'Product',
      'ProductCategory' => 'Product Category',
      'ProductBrand' => 'Product Brand',
      'Quote' => 'Quote',
      'QuoteItem' => 'Quote Item',
      'Tax' => 'Tax',
      'ShippingProvider' => 'Shipping Provider',
      'OpportunityItem' => 'Opportunity Item',
      'MailChimpCampaign' => 'MailChimp Campaign',
      'MailChimpList' => 'MailChimp List',
      'MailChimpListGroup' => 'MailChimp List Group',
      'WorkflowLogRecord' => 'Workflow Log Record',
      'Soluciones' => 'Solucion',
    ),
    'scopeNamesPlural' => 
    array (
      'Email' => 'Correos',
      'User' => 'Usuarios',
      'Team' => 'Equipos',
      'Role' => 'Roles',
      'EmailTemplate' => 'Plantillas de Correo',
      'EmailTemplateCategory' => 'Categorías de Formatos de Correo',
      'EmailAccount' => 'Cuentas de Correo Electrónico',
      'EmailAccountScope' => 'Cuentas de Correo Electrónico',
      'OutboundEmail' => 'Correos Salientes',
      'ScheduledJob' => 'Tareas Agendadas',
      'ExternalAccount' => 'Cuentas Externas',
      'Extension' => 'Extensiones',
      'Dashboard' => 'Tablero',
      'InboundEmail' => 'Grupo de Cuentas de Correo',
      'EmailAddress' => 'Email Addresses',
      'PhoneNumber' => 'Phone Numbers',
      'Stream' => 'Flujo',
      'Import' => 'Importar ',
      'Template' => 'Plantillas',
      'Job' => 'Trabajos',
      'EmailFilter' => 'Filtros de Correo',
      'Portal' => 'Portales',
      'PortalRole' => 'Roles del Portal',
      'Attachment' => 'Datos adjuntos',
      'EmailFolder' => 'Carpetas del Correo',
      'PortalUser' => 'Usuarios del Portal',
      'ApiUser' => 'Usuarios de la API',
      'ScheduledJobLogRecord' => 'Historial de Tareas Agendadas',
      'PasswordChangeRequest' => 'Solicitudes de Cambio de contraseña',
      'ActionHistoryRecord' => 'Historial de Acciones',
      'AuthToken' => 'Clave de Autorización',
      'UniqueId' => 'IDs Unicos',
      'LastViewed' => 'Ultimos Revisados',
      'AuthLogRecord' => 'Historial de Autorizaciones',
      'AuthFailLogRecord' => 'Hist. de Fallos de Aut.
',
      'LeadCapture' => 'Capturar Referencia',
      'LeadCaptureLogRecord' => 'Historial de Captura de Referencias',
      'ArrayValue' => 'Valores del Arreglo',
      'DashboardTemplate' => 'Dashboard Templates',
      'Currency' => 'Currency',
      'Webhook' => 'Webhooks',
      'Account' => 'Cuentas',
      'Contact' => 'Contactos',
      'Lead' => 'Referencias',
      'Target' => 'Intereses',
      'Opportunity' => 'Oportunidades',
      'Meeting' => 'Presentaciones',
      'Calendar' => 'Calendario',
      'Call' => 'Llamadas',
      'Task' => 'Tareas',
      'Case' => 'Casos',
      'Document' => 'Documentos',
      'DocumentFolder' => 'Carpetas de Documentos',
      'Campaign' => 'Campañas',
      'TargetList' => 'Listas de Intereses',
      'MassEmail' => 'Correos Masivos',
      'EmailQueueItem' => 'Items en Cola de Correo',
      'CampaignTrackingUrl' => 'URLs de Seguimiento',
      'Activities' => 'Actividades',
      'KnowledgeBaseArticle' => 'Base de Conocimientos',
      'KnowledgeBaseCategory' => 'Categorías de la Base de Conocimientos',
      'CampaignLogRecord' => 'Historial de Campañas',
      'Workflow' => 'Workflows',
      'Report' => 'Reports',
      'Product' => 'Products',
      'ProductCategory' => 'Product Categories',
      'Quote' => 'Quotes',
      'QuoteItem' => 'Quote Items',
      'Tax' => 'Taxes',
      'ShippingProvider' => 'Shipping Providers',
      'OpportunityItem' => 'Opportunity Items',
      'ProductBrand' => 'Product Brands',
      'MailChimpCampaign' => 'MailChimp Campaigns',
      'MailChimpList' => 'MailChimp Lists',
      'MailChimpListGroup' => 'MailChimp List Groups',
      'WorkflowLogRecord' => 'Workflows Log',
      'Soluciones' => 'Soluciones',
    ),
    'labels' => 
    array (
      'Misc' => 'Misceláneos',
      'Merge' => 'Generar',
      'None' => '(vacío)',
      'Home' => 'Inicio',
      'by' => 'por',
      'Saved' => 'Guardado',
      'Error' => 'Error',
      'Select' => 'Seleccionar',
      'Not valid' => 'No válido',
      'Please wait...' => 'Por favor espere...',
      'Please wait' => 'Por favor espere',
      'Loading...' => 'Cargando...',
      'Uploading...' => 'Subiendo...',
      'Sending...' => 'Enviando...',
      'Merging...' => 'Fusionando...',
      'Merged' => 'Generado',
      'Removed' => 'Eliminado',
      'Posted' => 'Publicado',
      'Linked' => 'Ligado',
      'Unlinked' => 'Desligado',
      'Done' => 'Hecho',
      'Access denied' => 'Acceso denegado',
      'Not found' => 'No encontrado',
      'Access' => 'Acceso',
      'Are you sure?' => '¿Está seguro?',
      'Record has been removed' => 'Registro Eliminado',
      'Wrong username/password' => 'Nombre de usuario/contraseña incorrectos',
      'Post cannot be empty' => 'La entrada no puede estar vacia',
      'Removing...' => 'Removiendo...',
      'Unlinking...' => 'Desligando...',
      'Posting...' => 'Publicando...',
      'Username can not be empty!' => '¡El nombre del usuario no puede estar vacío!',
      'Cache is not enabled' => 'El Cache no está habilitado',
      'Cache has been cleared' => 'Se borró el Cache correctamente',
      'Rebuild has been done' => 'Se ha reconstruido',
      'Return to Application' => 'Regresar a la Aplicación',
      'Saving...' => 'Guardando...',
      'Modified' => 'Modificado',
      'Created' => 'Creado(a)',
      'Create' => 'Crear',
      'create' => 'crear ',
      'Overview' => 'Vista',
      'Details' => 'Detalles',
      'Add Field' => 'Agregar Campo',
      'Add Dashlet' => 'Agregar Panel',
      'Filter' => 'Filtro',
      'Edit Dashboard' => 'Editar Tablero',
      'Add' => 'Agregar',
      'Add Item' => 'Agregar Elemento',
      'Reset' => 'Restablecer',
      'Menu' => 'Menú',
      'More' => 'Más',
      'Search' => 'Buscar',
      'Only My' => 'Sólo míos',
      'Open' => 'Abiertos',
      'Admin' => 'Administrador',
      'About' => 'Acerca de EspoCRM',
      'Refresh' => 'Actualizar',
      'Remove' => 'Eliminar',
      'Restore' => 'Restore',
      'Options' => 'Opciones',
      'Username' => 'Nombre de Usuario',
      'Password' => 'Contraseña',
      'Login' => 'Entrar',
      'Log Out' => 'Salir',
      'Preferences' => 'Preferencias',
      'State' => 'Estado/Distrito',
      'Street' => 'Calle',
      'Country' => 'País',
      'City' => 'Ciudad',
      'PostalCode' => 'Código Postal',
      'Followed' => 'Con Seguimiento',
      'Follow' => 'Seguir',
      'Followers' => 'Seguidores',
      'Clear Local Cache' => 'Borrar Cache Local',
      'Actions' => 'Acciones',
      'Delete' => 'Borrar',
      'Update' => 'Guardar',
      'Save' => 'Guardar',
      'Edit' => 'Editar',
      'View' => 'Ver',
      'Cancel' => 'Cancelar',
      'Apply' => 'Aplicar',
      'Unlink' => 'Desligar',
      'Mass Update' => 'Actualización Masiva',
      'Export' => 'Exportar',
      'No Data' => '(vacío)',
      'No Access' => 'Sin Acceso',
      'All' => 'Todos',
      'Active' => 'Activo',
      'Inactive' => 'Inactivo',
      'Write your comment here' => 'Escriba su comentario aquí',
      'Post' => 'Guardar',
      'Stream' => 'Flujo',
      'Show more' => 'Mostrar mas',
      'Dashlet Options' => 'Opciones del Panel',
      'Full Form' => 'Formulario Completo',
      'Insert' => 'Insertar',
      'Person' => 'Persona',
      'First Name' => 'Nombre',
      'Last Name' => 'Apellidos',
      'Middle Name' => 'Middle Name',
      'Original' => 'Original',
      'You' => 'Tu',
      'you' => 'tu',
      'change' => 'cambiar',
      'Change' => 'Cambiar',
      'Primary' => 'Primario',
      'Save Filter' => 'Guardar Filtro',
      'Administration' => 'Administración',
      'Run Import' => 'Ejecutar Importación',
      'Duplicate' => 'Duplicar',
      'Notifications' => 'Notificaciones',
      'Mark all read' => 'Marcar todos como leído',
      'See more' => 'Ver más',
      'Today' => 'Hoy',
      'Tomorrow' => 'Mañana',
      'Yesterday' => 'Ayer',
      'Submit' => 'Enviar',
      'Close' => 'Cerrar',
      'Yes' => 'Si',
      'No' => 'No',
      'Select All Results' => 'Seleccionar Todos',
      'Value' => 'Valor',
      'Current version' => 'Version Actual',
      'List View' => 'Vista de Lista',
      'Tree View' => 'Vista de árbol',
      'Unlink All' => 'Desligar todo',
      'Total' => 'Total',
      'Print to PDF' => 'Imprimir PDF',
      'Default' => 'Default',
      'Number' => 'Número',
      'From' => 'De',
      'To' => 'Para',
      'Create Post' => 'Crear Entrada',
      'Previous Entry' => 'Entrada Previa',
      'Next Entry' => 'Siguiente Entrada',
      'View List' => 'Ver Lista',
      'Attach File' => 'Adjuntar archivo',
      'Skip' => 'Saltar',
      'Attribute' => 'Atributo',
      'Function' => 'Función',
      'Self-Assign' => 'Auto-Asignar',
      'Self-Assigned' => 'Auto-Asignado',
      'Expand' => 'Expander',
      'Collapse' => 'Cerrar',
      'New notifications' => 'Nuevas notificaciones',
      'Manage Categories' => 'Administrar Categorías',
      'Manage Folders' => 'Administrar Carpetas',
      'Convert to' => 'Convertir a',
      'View Personal Data' => 'Ver Datos Personales',
      'Personal Data' => 'Datos Personales',
      'Erase' => 'Borrar',
      'View Followers' => 'View Followers',
      'Convert Currency' => 'Convert Currency',
      'View on Map' => 'View on Map',
      'Move Over' => 'Mover',
      'Create InboundEmail' => 'Crear Correo Entrante',
      'Activities' => 'Actividades',
      'History' => 'Historial',
      'Attendees' => 'Asistentes',
      'Schedule Meeting' => 'Agendar Presentación',
      'Schedule Call' => 'Agendar LLamada',
      'Compose Email' => 'Escribir Correo',
      'Log Meeting' => 'Registrar Presentación',
      'Log Call' => 'Registrar Llamada',
      'Archive Email' => 'Archivar Correo',
      'Create Task' => 'Crear Tarea',
      'Tasks' => 'Tareas',
    ),
    'messages' => 
    array (
      'pleaseWait' => 'Por favor espere...',
      'posting' => 'Publicando...',
      'loading' => 'Cargando...',
      'saving' => 'Guardando...',
      'confirmLeaveOutMessage' => '¿Realmente desea salir del formulario?',
      'notModified' => 'No ha modificado el registro',
      'duplicate' => 'El registro que estás creando ya puede existir.',
      'dropToAttach' => 'Haga drop para adjuntar',
      'fieldIsRequired' => '{field} es requerido',
      'fieldShouldBeEmail' => '{field} debería ser un correo válido',
      'fieldShouldBeFloat' => '{field} debería ser un número válido',
      'fieldShouldBeInt' => '{field} debería ser un entero válido',
      'fieldShouldBeDate' => '{field} debería ser una fecha válida',
      'fieldShouldBeDatetime' => '{field} deber{ia ser una fecha/hr válida',
      'fieldShouldAfter' => '{field} debe estar después de {otherField}',
      'fieldShouldBefore' => '{field} debe estar antes de {otherField}',
      'fieldShouldBeBetween' => '{field} debe estar entre {min} y {max}',
      'fieldShouldBeLess' => '{field} no debe ser mayor a {value}',
      'fieldShouldBeGreater' => '{field} no debe ser menor que {value}',
      'fieldBadPasswordConfirm' => '{field} confirmado de forma incorrecta',
      'fieldMaxFileSizeError' => 'El archivo no debe exceder {max} Mb',
      'fieldValueDuplicate' => 'Duplicate value',
      'fieldIsUploading' => 'Carga en prograso',
      'fieldExceedsMaxCount' => 'Count exceeds max allowed {maxCount}',
      'resetPreferencesDone' => 'Se han restablecido las preferencias default',
      'confirmation' => '¿Está seguro?',
      'unlinkAllConfirmation' => '¿Realmente desea desvincular todos los registros relacionados?',
      'resetPreferencesConfirmation' => '¿Realmente desea restablecer las preferencias default?',
      'removeRecordConfirmation' => '¿Realmente desea eliminar registros?',
      'unlinkRecordConfirmation' => '¿Realmente quiere desligar este registro?',
      'removeSelectedRecordsConfirmation' => '¿Realmente desea eliminar los registros seleccionados?',
      'unlinkSelectedRecordsConfirmation' => 'Estas seguro que deseas desligar los registros seleccionados ?',
      'massUpdateResult' => '{count} registro(s) actualizado(s)',
      'massUpdateResultSingle' => '{count} registro actualizado',
      'recalculateFormulaConfirmation' => 'Are you sure you want to recalculate formula for selected records?',
      'noRecordsUpdated' => 'Ningún registro fue actualizado',
      'massRemoveResult' => '{count} registro(s) eliminado(s)',
      'massRemoveResultSingle' => '{count} registro eliminado',
      'noRecordsRemoved' => 'Ningún registro fue eliminado',
      'clickToRefresh' => 'Clic para actualizar',
      'writeYourCommentHere' => 'Escriba su comentario aquí',
      'writeMessageToUser' => 'Escribir un mensaje a {user}',
      'writeMessageToSelf' => 'Escribe un mensaje en tu flujo',
      'typeAndPressEnter' => 'Teclear y oprimir enter',
      'checkForNewNotifications' => 'Ver si hay nuevas notificaciones',
      'checkForNewNotes' => 'Verificar si hay nuevos flujos',
      'internalPost' => 'La publicación sólo será vista por los usuarios internos',
      'internalPostTitle' => 'Lo publicado sólo lo verán los usuarios internos',
      'done' => 'Enviados',
      'notUpdated' => 'Not updated',
      'confirmMassFollow' => '¿Realmente quieres marcar con seguimiento a los registros seleccionados?',
      'confirmMassUnfollow' => '¿Realmente quieres marcar sin seguimiento a los registros seleccionados?',
      'massFollowResult' => '{count} registro(s) ahora tienen seguimento',
      'massUnfollowResult' => '{count} registro(s) ya no tienen seguimiento',
      'massFollowResultSingle' => '{count} nuevo(s) registro(s) tienen seguimiento',
      'massUnfollowResultSingle' => 'El registro {count} ya no tiene seguimiento',
      'massFollowZeroResult' => 'Nada tiene seguimiento',
      'massUnfollowZeroResult' => 'A nada se le quitó el seguimiento',
      'erasePersonalDataConfirmation' => '¿Realmente desea borrar permanentemente los campos seleccionados?',
      'maintenanceMode' => 'The application currently is in maintenance mode. Only admin users have access.

Maintenance mode can be disabled at Administration → Settings.',
      'massPrintPdfMaxCountError' => 'No se pueden imprimir mas de {maxCount} registros.',
      'streamPostInfo' => 'Escriba &lt;strong&gt;@username&lt;/strong&gt; para indicar los usuarios de esta publicación.

Sintaxis disponible para los marcadores:
`&lt;code&gt;código&lt;/code&gt;`
**&lt;strong&gt;texto en negrita&lt;/strong&gt;**
*&lt;em&gt;texto en itálica&lt;/em&gt;*
~&lt;del&gt;texto eliminado&lt;/del&gt;~
&gt; marcador de bloque
[texto de la liga](url) ',
    ),
    'boolFilters' => 
    array (
      'onlyMy' => 'Sólo míos',
      'onlyMyTeam' => 'My Team',
      'followed' => 'Con Seguimiento',
    ),
    'presetFilters' => 
    array (
      'followed' => 'Con Seguimiento',
      'all' => 'Todos',
    ),
    'massActions' => 
    array (
      'remove' => 'Eliminar',
      'merge' => 'Generar',
      'massUpdate' => 'Actualización Masiva',
      'unlink' => 'Desligar',
      'export' => 'Exportar',
      'follow' => 'Dar seguimiento',
      'unfollow' => 'Quitar seguimiento',
      'convertCurrency' => 'Convertir Moneda',
      'recalculateFormula' => 'Recalculate Formula',
      'printPdf' => 'Imprimir a PDF',
    ),
    'fields' => 
    array (
      'name' => 'Nombre',
      'firstName' => 'Nombre',
      'lastName' => 'Apellidos',
      'middleName' => 'Middle Name',
      'salutationName' => 'Saludo',
      'assignedUser' => 'Usuario Asignado',
      'assignedUsers' => 'Usuarios Asignados',
      'emailAddress' => 'Correo electrónico',
      'emailAddressData' => 'Datos de la Dirección de Correo',
      'emailAddressIsOptedOut' => 'La dirección de correo está Confirmada',
      'assignedUserName' => 'Nombre de Usuario Asignado',
      'teams' => 'Equipos',
      'createdAt' => 'Creado en',
      'modifiedAt' => 'Modificado el',
      'createdBy' => 'Creado por',
      'modifiedBy' => 'Modificado Por',
      'description' => 'Descripción',
      'address' => 'Dirección',
      'phoneNumber' => 'Teléfono',
      'phoneNumberMobile' => 'Teléfono (Móvil)',
      'phoneNumberHome' => 'Teléfono (Casa)',
      'phoneNumberFax' => 'Teléfono (Fax)',
      'phoneNumberOffice' => 'Teléfono (Oficina)',
      'phoneNumberOther' => 'Teléfono (Otro)',
      'phoneNumberData' => 'Datos del Número de Teléfono',
      'phoneNumberIsOptedOut' => 'Phone Number is Opted-Out',
      'order' => 'Orden',
      'parent' => 'Padre',
      'children' => 'Hijos',
      'id' => 'ID',
      'ids' => 'ID\'s',
      'type' => 'Tipo',
      'names' => 'Nombres',
      'types' => 'Tipos',
      'targetListIsOptedOut' => 'Se ha Excluido (De la Lista)',
      'billingAddressCity' => 'Ciudad',
      'billingAddressCountry' => 'País',
      'billingAddressPostalCode' => 'Código Postal',
      'billingAddressState' => 'Estado/Distrito',
      'billingAddressStreet' => 'Calle',
      'billingAddressMap' => 'Mapa',
      'addressCity' => 'Ciudad',
      'addressStreet' => 'Calle',
      'addressCountry' => 'País',
      'addressState' => 'Estado/Distrito',
      'addressPostalCode' => 'Código Postal',
      'addressMap' => 'Mapa',
      'shippingAddressCity' => 'Ciudad (Entrega)',
      'shippingAddressStreet' => 'Calle (Entrega)',
      'shippingAddressCountry' => 'País (Entrega)',
      'shippingAddressState' => 'Estado (Entrega)',
      'shippingAddressPostalCode' => 'Código Postal (Entrega)',
      'shippingAddressMap' => 'Mapa (Entrega)',
    ),
    'links' => 
    array (
      'assignedUser' => 'Usuario Asignado',
      'createdBy' => 'Creado por',
      'modifiedBy' => 'Modificado Por',
      'team' => 'Equipo',
      'roles' => 'Roles',
      'teams' => 'Equipos',
      'users' => 'Usuarios',
      'parent' => 'Padre',
      'children' => 'Hijos',
      'contacts' => 'Contactos',
      'opportunities' => 'Oportunidades',
      'leads' => 'Referencias',
      'meetings' => 'Presentaciones',
      'calls' => 'Llamadas',
      'tasks' => 'Tareas',
      'emails' => 'Correos',
      'accounts' => 'Cuentas',
      'cases' => 'Casos',
      'documents' => 'Documentos',
      'account' => 'Cuenta',
      'opportunity' => 'Oportunidad',
      'contact' => 'Contacto',
    ),
    'dashlets' => 
    array (
      'Stream' => 'Flujo',
      'Emails' => 'Mi Bandeja de Entrada',
      'Iframe' => 'Iframe',
      'Records' => 'Lista de Registros',
      'Leads' => 'Mis Referencias',
      'Opportunities' => 'Mis Oportunidades',
      'Tasks' => 'Mis Tareas',
      'Cases' => 'Mis Casos',
      'Calendar' => 'Calendario',
      'Calls' => 'Mis Llamadas',
      'Meetings' => 'Mis Presentaciones',
      'OpportunitiesByStage' => 'Oportunidades por Etapa',
      'OpportunitiesByLeadSource' => 'Oportunidades de Fuente de Referencias',
      'SalesByMonth' => 'Ventas por Mes',
      'SalesPipeline' => 'Canalización de Ventas',
      'Activities' => 'Mis Actividades',
    ),
    'notificationMessages' => 
    array (
      'assign' => '{entityType} {entity} ha sido asignado a usted',
      'emailReceived' => 'Correo recibido de {from}',
      'entityRemoved' => '{user} ha eliminado {entityType} {entity}',
    ),
    'streamMessages' => 
    array (
      'post' => '{user} a publicado en {entityType} {entity}',
      'attach' => '{user} adjuntado en {entityType} {entity}',
      'status' => '{user} ha actualizado {field} en {entityType} {entity}',
      'update' => '{user} ha actualizado {entityType} {entity}',
      'postTargetTeam' => '{user} publicó en equipo {target}',
      'postTargetTeams' => '{user} publicó en equipos {target}',
      'postTargetPortal' => '{user} publicó en el portal {target}',
      'postTargetPortals' => '{user} publicó en los portales {target}',
      'postTarget' => '{user} publicó en {target}',
      'postTargetYou' => '{user} publicado por usted',
      'postTargetYouAndOthers' => '{user} envió a {target} y a usted',
      'postTargetAll' => '{user} envió a todos',
      'postTargetSelf' => '{user} auto-publicado',
      'postTargetSelfAndOthers' => '{user} publicó en {target} con copia a si mismo',
      'mentionInPost' => '{user} mencionado {mentioned} en {entityType} {entity}',
      'mentionYouInPost' => '{user} te ha mencionado en {entityType} {entity}',
      'mentionInPostTarget' => '{user} mencionó a {mentioned} en el post',
      'mentionYouInPostTarget' => '{user} te ha mencionado en post para {target}',
      'mentionYouInPostTargetAll' => '{user} te ha mencionado en post para todos',
      'mentionYouInPostTargetNoTarget' => '{user} te menciona en el post',
      'create' => '{user} creó {entityType} {entity}',
      'createThis' => '{user} Creó un(a) nuevo(a) {entityType}',
      'createAssignedThis' => '{user} creó este(a) {entityType} asignado(a) a {assignee}',
      'createAssigned' => '{user} creó {entityType} {entity} asignado(a) a {assignee}',
      'createAssignedYou' => '{user} creó {entityType} {entity} y te la asignó',
      'createAssignedThisSelf' => '{user} creó este(a) {entityType} auto-asignado(a)',
      'createAssignedSelf' => '{user} creó {entityType} {entity} auto-asignado(a)',
      'assign' => '{user} ha asignado {entityType} {entity} a {assignee}',
      'assignThis' => '{user} asignar este {entityType} a {assignee}',
      'assignYou' => '{user} te asignó {entityType} {entity}',
      'assignThisVoid' => '{user} desasignó esta {entityType}',
      'assignVoid' => '{user} desasignó {entityType} {entity}',
      'assignThisSelf' => '{user} auto-asignó esta {entityType}',
      'assignSelf' => '{user} auto-asignó {entityType} {entity}',
      'postThis' => '{user} publicado',
      'attachThis' => '{user} adjunto',
      'statusThis' => '{user} actualizado {field}',
      'updateThis' => '{user} actualizado a este {entityType}',
      'createRelatedThis' => '{user} creó {relatedEntityType} {relatedEntity} ligado a este(a) {entityType}',
      'createRelated' => '{user} creó un(a) {relatedEntityType} {relatedEntity} ligado(a) a {entityType} {entity}',
      'relate' => '{user} ligó {relatedEntityType} {relatedEntity} con {entityType} {entity}',
      'relateThis' => '{user} ligó {relatedEntityType} {relatedEntity} con este {entityType}',
      'emailReceivedFromThis' => 'Correo recibido de {from}',
      'emailReceivedInitialFromThis' => 'Correo recibido de {from}, este(a) {entityType} creado(a)',
      'emailReceivedThis' => 'El correo {email} ha sido recibido',
      'emailReceivedInitialThis' => 'Correo recibido, este(a) {entityType} ha sido creado(a)',
      'emailReceivedFrom' => 'Correo recibido de {from}, relacionado a {entityType} {entity}',
      'emailReceivedFromInitial' => 'Correo recibido de {from}, {entityType} {entity} creado(a)',
      'emailReceived' => 'Se recibió el correo {email} para su {entityType} {entity}',
      'emailReceivedInitial' => 'Correo recibido: {entityType} {entity} creado(a)',
      'emailReceivedInitialFrom' => 'Correo recibido de {from}, {entityType} {entity} creado(a)',
      'emailSent' => '{by} envió un correo relacionado a {entityType} {entity}',
      'emailSentThis' => '{by} envió un correo',
    ),
    'streamMessagesMale' => 
    array (
      'postTargetSelfAndOthers' => '{user} publicó a {target} con copia para sí mismo',
    ),
    'streamMessagesFemale' => 
    array (
      'postTargetSelfAndOthers' => '{user} publicó a {target} y a sí mismo',
    ),
    'lists' => 
    array (
      'monthNames' => 
      array (
        0 => 'Enero',
        1 => 'Febrero',
        2 => 'Marzo',
        3 => 'Abril',
        4 => 'Mayo',
        5 => 'Junio',
        6 => 'Julio',
        7 => 'Agosto',
        8 => 'Septiembre',
        9 => 'Octubre',
        10 => 'Noviembre',
        11 => 'Diciembre',
      ),
      'monthNamesShort' => 
      array (
        0 => 'Ene',
        1 => 'Feb',
        2 => 'Mar',
        3 => 'Abr',
        4 => 'May',
        5 => 'Jun',
        6 => 'Jul',
        7 => 'Ago',
        8 => 'Sep',
        9 => 'Oct',
        10 => 'Nov',
        11 => 'Dic',
      ),
      'dayNames' => 
      array (
        0 => 'Domingo',
        1 => 'Lunes',
        2 => 'Martes',
        3 => 'Miércoles',
        4 => 'Jueves',
        5 => 'Viernes',
        6 => 'Sábado',
      ),
      'dayNamesShort' => 
      array (
        0 => 'Dom',
        1 => 'Lun',
        2 => 'Mar',
        3 => 'Mie',
        4 => 'Jue',
        5 => 'Vie',
        6 => 'Sab',
      ),
      'dayNamesMin' => 
      array (
        0 => 'Do',
        1 => 'Lu',
        2 => 'Ma',
        3 => 'Mi',
        4 => 'Ju',
        5 => 'Vi',
        6 => 'Sa',
      ),
    ),
    'durationUnits' => 
    array (
      'd' => 'd',
      'h' => 'h',
      'm' => 'm',
      's' => 's',
    ),
    'options' => 
    array (
      'salutationName' => 
      array (
        'Mr.' => 'Sr.',
        'Mrs.' => 'Sra.',
        'Ms.' => 'Srta.',
        'Dr.' => 'Dr.',
      ),
      'language' => 
      array (
        'af_ZA' => 'Afrikáans',
        'az_AZ' => 'Azerbaiyán',
        'be_BY' => 'Bielorruso',
        'bg_BG' => 'Bulgaro',
        'bn_IN' => 'Bengalí',
        'bs_BA' => 'Bosnio',
        'ca_ES' => 'Catalán',
        'cs_CZ' => 'Checo',
        'cy_GB' => 'Galés',
        'da_DK' => 'Danés',
        'de_DE' => 'Alemán',
        'el_GR' => 'Griego',
        'en_GB' => 'Inglés (UK)',
        'es_MX' => 'Español (México)',
        'en_US' => 'Inglés (US)',
        'es_ES' => 'Español (España)',
        'et_EE' => 'Estonio',
        'eu_ES' => 'Vasco',
        'fa_IR' => 'Persa',
        'fi_FI' => 'Finlandés',
        'fo_FO' => 'Feroés',
        'fr_CA' => 'Francés (Canada)',
        'fr_FR' => 'Francés (Francia)',
        'ga_IE' => 'Irlandés',
        'gl_ES' => 'Gallego',
        'gn_PY' => 'Guaraní',
        'he_IL' => 'Hebreo',
        'hi_IN' => 'Hindi',
        'hr_HR' => 'Croata',
        'hu_HU' => 'Hungaro',
        'hy_AM' => 'Armenio',
        'id_ID' => 'Indonesio',
        'is_IS' => 'Islandés',
        'it_IT' => 'Italiano',
        'ja_JP' => 'Japonés',
        'ka_GE' => 'Georgiano',
        'km_KH' => 'Camboyano',
        'ko_KR' => 'Coreano',
        'ku_TR' => 'Kurdo',
        'lt_LT' => 'Lituano',
        'lv_LV' => 'Latón',
        'mk_MK' => 'Macedonio',
        'ml_IN' => 'Malabar',
        'ms_MY' => 'Malayo',
        'nb_NO' => 'Noruego Bokmål',
        'nn_NO' => 'Noruego Nynorsk',
        'ne_NP' => 'Nepalí',
        'nl_NL' => 'Holandés',
        'pa_IN' => 'Punyabí',
        'pl_PL' => 'Polaco',
        'ps_AF' => 'Pastún',
        'pt_BR' => 'Portugués (Brasil)',
        'pt_PT' => 'Portugués (Portugal)',
        'ro_RO' => 'Rumano',
        'ru_RU' => 'Ruso',
        'sk_SK' => 'Eslovaco',
        'sl_SI' => 'Esloveno',
        'sq_AL' => 'Albanés',
        'sr_RS' => 'Serbio',
        'sv_SE' => 'Sueco',
        'sw_KE' => 'Suajili',
        'ta_IN' => 'Tamil',
        'te_IN' => 'Télugu',
        'th_TH' => 'Tailandés',
        'tl_PH' => 'Tagalo',
        'tr_TR' => 'Turco',
        'uk_UA' => 'Ucraniano',
        'ur_PK' => 'Urdu',
        'vi_VN' => 'Vietnamita',
        'zh_CN' => 'Chino Simplificado (China)',
        'zh_HK' => 'Chino Tradicional (Hong Kong)',
        'zh_TW' => 'Chino Traditional (Taiwán)',
      ),
      'dateSearchRanges' => 
      array (
        'on' => 'En',
        'notOn' => 'No está en',
        'after' => 'Después',
        'before' => 'Antes',
        'between' => 'Entre',
        'today' => 'Hoy',
        'past' => 'Pasado',
        'future' => 'Futuro',
        'currentMonth' => 'Mes Actual',
        'lastMonth' => 'Mes Pasado',
        'nextMonth' => 'Siguiente mes',
        'currentQuarter' => 'Trimestre Actual',
        'lastQuarter' => 'Trimestre Pasado',
        'currentYear' => 'Año Actual',
        'lastYear' => 'Año Pasado',
        'lastSevenDays' => 'Últimos 7 Días',
        'lastXDays' => 'Últimos X Días',
        'nextXDays' => 'Próximos X Días',
        'ever' => 'Nunca',
        'isEmpty' => 'Está Vacío',
        'olderThanXDays' => 'Mayor de "X" Días',
        'afterXDays' => 'Después de "X" Días',
        'currentFiscalYear' => 'Año Fiscal Actual',
        'lastFiscalYear' => 'Último Año Fiscal',
        'currentFiscalQuarter' => 'Trimestre Fiscal Actual',
        'lastFiscalQuarter' => 'Último Trimestre Fiscal',
      ),
      'searchRanges' => 
      array (
        'is' => 'Es',
        'isEmpty' => 'Está vacío',
        'isNotEmpty' => 'No Está Vacío',
        'isOneOf' => 'Cualquiera',
        'isFromTeams' => 'Es del Equipo',
        'isNot' => 'No Es',
        'isNotOneOf' => 'Ninguno De',
        'anyOf' => 'Cualquiera',
        'allOf' => 'All Of',
        'noneOf' => 'Ninguno De',
        'any' => 'Any',
      ),
      'varcharSearchRanges' => 
      array (
        'equals' => 'Equivale',
        'like' => 'Es Como (%)',
        'notLike' => 'No es como (%)',
        'startsWith' => 'Comienza con',
        'endsWith' => 'Termina Con',
        'contains' => 'Contiene',
        'notContains' => 'No Contiene',
        'isEmpty' => 'Está vacío',
        'isNotEmpty' => 'No Está Vacío',
        'notEquals' => 'No es Igual a',
      ),
      'intSearchRanges' => 
      array (
        'equals' => 'Equivale',
        'notEquals' => 'Diferentes',
        'greaterThan' => 'Mayor que',
        'lessThan' => 'Menor que',
        'greaterThanOrEquals' => 'Mayor o igual que',
        'lessThanOrEquals' => 'Menor o igual que',
        'between' => 'Entre',
        'isEmpty' => 'Está vacío',
        'isNotEmpty' => 'No está vacío',
      ),
      'autorefreshInterval' => 
      array (
        0 => 'Ninguno',
        '0.5' => '30 segundos',
        1 => '1 minuto',
        2 => '2 minutos',
        5 => '5 minutos',
        10 => '10 minutos',
      ),
      'phoneNumber' => 
      array (
        'Mobile' => 'Teléfono móvil',
        'Office' => 'Oficina',
        'Fax' => 'Fax',
        'Home' => 'Hogar',
        'Other' => 'Otro',
      ),
      'reminderTypes' => 
      array (
        'Popup' => 'Ventana emergente',
        'Email' => 'Correo electrónico',
      ),
    ),
    'sets' => 
    array (
      'summernote' => 
      array (
        'NOTICE' => 'Usted puede encontrar aquí la traducción: https://github.com/HackerWins/summernote/tree/master/lang',
        'font' => 
        array (
          'bold' => 'Negrita',
          'italic' => 'Itálico',
          'underline' => 'Subrayado',
          'strike' => 'Tachado',
          'clear' => 'Quitar Estilo de Fuente',
          'height' => 'Alto de línea',
          'name' => 'Familia de Fuente',
          'size' => 'Tamaño de Fuente',
        ),
        'image' => 
        array (
          'image' => 'Visualización',
          'insert' => 'Insertar Imagen',
          'resizeFull' => 'Cambiar el tamaño a completo',
          'resizeHalf' => 'Cambiar el tamaño a la mitad',
          'resizeQuarter' => 'Cambiar el tamaño a un cuarto',
          'floatLeft' => 'Flotante (izq)',
          'floatRight' => 'Flotante (der)',
          'floatNone' => 'Sin Flotar',
          'dragImageHere' => 'Arrastre la imagen aquí',
          'selectFromFiles' => 'Seleccionar desde Archivo',
          'url' => 'Url de Imagen',
          'remove' => 'Eliminar Imagen',
        ),
        'link' => 
        array (
          'link' => 'Enlace',
          'insert' => 'Insertar Enlace',
          'unlink' => 'Desligar',
          'edit' => 'Editar',
          'textToDisplay' => 'Texto a mostrar',
          'url' => '¿A que URL debería ir este enlace?',
          'openInNewWindow' => 'Abrir en nueva ventana',
        ),
        'video' => 
        array (
          'video' => 'Video',
          'videoLink' => 'Enlace al Video',
          'insert' => 'Insertar Video',
          'url' => '¿URL del Video?',
          'providers' => '(YouTube, Vimeo, Vine, Instagram, or DailyMotion)',
        ),
        'table' => 
        array (
          'table' => 'Tabla',
        ),
        'hr' => 
        array (
          'insert' => 'Insertar regla horizontal',
        ),
        'style' => 
        array (
          'style' => 'Estilo',
          'normal' => 'Normal',
          'blockquote' => 'Cita',
          'pre' => 'Código',
          'h1' => 'Encabezado 1',
          'h2' => 'Encabezado 2',
          'h3' => 'Encabezado 3',
          'h4' => 'Encabezado 4',
          'h5' => 'Encabezado 5',
          'h6' => 'Encabezado 6',
        ),
        'lists' => 
        array (
          'unordered' => 'Lista sin Ordenar',
          'ordered' => 'Lista Ordenada',
        ),
        'options' => 
        array (
          'help' => 'Ayuda',
          'fullscreen' => 'Pantalla Completa',
          'codeview' => 'Ver Código',
        ),
        'paragraph' => 
        array (
          'paragraph' => 'Párrafo',
          'outdent' => 'Anular sangría',
          'indent' => 'Sangría',
          'left' => 'Alinear Izquierda',
          'center' => 'Alinear Centro',
          'right' => 'Alinear Derecha',
          'justify' => 'Justificado',
        ),
        'color' => 
        array (
          'recent' => 'Color Reciente',
          'more' => 'Mas Colores',
          'background' => 'Color de Fondo',
          'foreground' => 'Color de Fuente',
          'transparent' => 'Transparente',
          'setTransparent' => 'Definir como transparente',
          'reset' => 'Restablecer',
          'resetToDefault' => 'Restablecer el original',
        ),
        'shortcut' => 
        array (
          'shortcuts' => 'Atajos de teclado',
          'close' => 'Cerrar',
          'textFormatting' => 'Formato de texto',
          'action' => 'Acción',
          'paragraphFormatting' => 'Formato de párrafo',
          'documentStyle' => 'Estilo de Documento',
        ),
        'history' => 
        array (
          'undo' => 'Deshacer',
          'redo' => 'Rehacer',
        ),
      ),
    ),
    'listViewModes' => 
    array (
      'list' => 'Lista',
      'kanban' => 'Tarjetas',
    ),
    'themes' => 
    array (
      'Espo' => 'Espo Clásico',
      'EspoRtl' => 'RTL Espo',
      'Sakura' => 'Sakura Clásico',
      'EspoVertical' => 'Espo Vertical',
      'SakuraVertical' => 'Sakura Vertical',
      'Violet' => 'Violeta Clásico',
      'VioletVertical' => 'Violeta Vertical',
      'Hazyblue' => 'Azul Clásico',
      'HazyblueVertical' => 'Azul Vertical',
    ),
  ),
  'Import' => 
  array (
    'labels' => 
    array (
      'Revert Import' => 'Revertir Importación',
      'Return to Import' => 'Regresar a Importación',
      'Run Import' => 'Ejecutar Importación',
      'Back' => 'Anterior',
      'Field Mapping' => 'Mapeo de Campo',
      'Default Values' => 'Valores Default',
      'Add Field' => 'Agregar Campo',
      'Created' => 'Creado(a)',
      'Updated' => 'Actualizado',
      'Result' => 'Resultado',
      'Show records' => 'Mostrar registros',
      'Remove Duplicates' => 'Eliminar Duplicados	',
      'importedCount' => 'Importado (recuento)',
      'duplicateCount' => 'Duplicados (recuento)',
      'updatedCount' => 'Actualizado (recuento)',
      'Create Only' => 'Sólo Crear',
      'Create and Update' => 'Crear y Actualizar',
      'Update Only' => 'Sólo Actualizar',
      'Update by' => 'Actualizado por',
      'Set as Not Duplicate' => 'Establecer como No Duplicado',
      'File (CSV)' => 'Archivo (CSV)',
      'First Row Value' => 'Valor del Primer Renglón',
      'Skip' => 'Saltar',
      'Header Row Value' => 'Valor del Encabezado',
      'Field' => 'Campo',
      'What to Import?' => '¿Qué va a importar?',
      'Entity Type' => 'Tipo de Entidad',
      'What to do?' => '¿Qué hacer?',
      'Properties' => 'Propiedades',
      'Header Row' => 'Renglón de Encabezado',
      'Person Name Format' => 'Formato del Nombre de la Persona',
      'John Smith' => 'Pedro Pérez',
      'Smith John' => 'Pérez Pedro',
      'Smith, John' => 'Perez, Pedro',
      'Field Delimiter' => 'Delimitante del Campo',
      'Date Format' => 'Formato de la Fecha',
      'Decimal Mark' => 'Separador Decimal',
      'Text Qualifier' => 'Calificador del Texto',
      'Time Format' => 'Formato de Hora',
      'Currency' => 'Moneda',
      'Preview' => 'Vista previa',
      'Next' => 'Siguiente',
      'Step 1' => 'Paso 1',
      'Step 2' => 'Paso 2',
      'Double Quote' => 'Comillas dobles',
      'Single Quote' => 'Comillas sencillas',
      'Imported' => 'Importado',
      'Duplicates' => 'Duplicados',
      'Skip searching for duplicates' => 'No buscar duplicados',
      'Timezone' => 'Zona horaria',
      'Remove Import Log' => 'Eliminar Historial de Importaciones',
      'New Import' => 'Nueva Importación',
      'Import Results' => 'Resultados de la Importación',
      'Silent Mode' => 'Modo silencioso',
    ),
    'messages' => 
    array (
      'utf8' => 'Debe ser codificado en UTF-8',
      'duplicatesRemoved' => 'Duplicados removidos',
      'inIdle' => 'Ejecutar fuera de la sesión (para grandes volúmenes de datos, vía cron-job)',
      'revert' => 'Esta acción eliminará permanentemente todos los registros importados.',
      'removeDuplicates' => 'Esta acción eliminará permanentemente todos los registros importados que sean duplicados.',
      'confirmRevert' => '¿Realmente desea eliminar permanentemente todos los registros importados?',
      'confirmRemoveDuplicates' => '¿Realmente desea eliminar permanentemente todos los registros importados que sean duplicados?',
      'confirmRemoveImportLog' => 'Esta acción eliminará el historial de importación. Todos los registros importados se conservarán, pero ya no podrá deshacer la importación. ¿Realmente desea hacerlo?',
      'removeImportLog' => 'Esta acción eliminará el historial de importación. Todos los registros importados se conservarán.  Hágalo sólo si la importación fue correcta.',
    ),
    'fields' => 
    array (
      'file' => 'Archivo',
      'entityType' => 'Tipo de Entidad',
      'imported' => 'Registros Importados',
      'duplicates' => 'registros Duplicados',
      'updated' => 'registros Actualizados',
      'status' => 'Estátus',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Failed' => 'Falló',
        'In Process' => 'En Proceso',
        'Complete' => 'Terminó',
      ),
      'personNameFormat' => 
      array (
        'f l' => 'First Last',
        'l f' => 'Last First',
        'f m l' => 'First Middle Last',
        'l f m' => 'Last First Middle',
        'l, f' => 'Last, First',
      ),
    ),
  ),
  'InboundEmail' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'emailAddress' => 'Correo Electrónico',
      'team' => 'Equipo del Interés',
      'status' => 'Estado',
      'assignToUser' => 'Asignar al Usuario',
      'host' => 'Servidor',
      'username' => 'Nombre de Usuario',
      'password' => 'Contraseña',
      'port' => 'Puerto',
      'monitoredFolders' => 'Carpetas supervisadas',
      'trashFolder' => 'Carpeta del Basurero',
      'ssl' => 'SSL',
      'createCase' => 'Crear Caso',
      'reply' => 'Respuesta Automática',
      'caseDistribution' => 'Distribución de Caso',
      'replyEmailTemplate' => 'Plantilla de Respuesta de Correo',
      'replyFromAddress' => 'Respuesta de la Dirección',
      'replyToAddress' => 'Responder a la Dirección',
      'replyFromName' => 'Respuesta de Nombre',
      'targetUserPosition' => 'Interés Posición Usuario',
      'fetchSince' => 'Obtener Desde',
      'addAllTeamUsers' => 'Para todos los usuarios del equipo',
      'teams' => 'Equipos',
      'sentFolder' => 'Carpeta Enviada',
      'storeSentEmails' => 'Guardar correos enviados',
      'keepFetchedEmailsUnread' => 'Keep Fetched Emails Unread',
      'useImap' => 'Obtener Correos',
      'useSmtp' => 'Usar SMTP',
      'smtpHost' => 'Servidor SMTP',
      'smtpPort' => 'Puerto SMTP',
      'smtpAuth' => 'Configuración SMTP',
      'smtpSecurity' => 'Seguridad SMTP',
      'smtpAuthMechanism' => 'SMTP Auth Mechanism',
      'smtpUsername' => 'Nombre SMTP',
      'smtpPassword' => 'Contraseña SMTP',
      'fromName' => 'Remitente',
      'smtpIsShared' => 'SMTP es compartido',
      'smtpIsForMassEmail' => 'SMTP es para correo masivo',
    ),
    'tooltips' => 
    array (
      'reply' => 'Notifique a los remitentes de correo que han recibido sus mensajes.

 Sólo un correo será enviado a un destinatario particular durante un período de tiempo para evitar bucles.',
      'createCase' => 'Crear un caso automaticamente, al recibir correos entrantes.',
      'replyToAddress' => 'Especifique la dirección de correo de este buzón para hacer que las respuestas vegan aquí.',
      'caseDistribution' => '¿Cómo serán asignados a los casos? Asignados directamente a un usuario o al equipo.',
      'assignToUser' => 'Los casos del usuario serán reasignados.',
      'team' => 'Los casos del equipo serán reasignados.',
      'teams' => 'Los correos del equipo serán reasignados.',
      'targetUserPosition' => 'Los Usuarios con una posición específica serán distribuidos en los casos.',
      'addAllTeamUsers' => 'Los correos aparecerán en el buzón de entrada de todos los usuarios de los equipos especificados.',
      'monitoredFolders' => 'Si usa varias carpetas, sepárelas con coma',
      'smtpIsShared' => 'Si está marcado, los usuarios podrán enviar correos usando este servicio de SMTP.  La disponibilidad se controla con los Roles, a través de los permisos de Grupos de Cuentas de Correo.',
      'smtpIsForMassEmail' => 'Si lo marca, el SMTP estará disponible para envíos masivos de correo.',
      'storeSentEmails' => 'Los correos enviados serán guardados en el servidor IMAP.',
    ),
    'links' => 
    array (
      'filters' => 'Filtros',
      'emails' => 'Correos',
      'assignToUser' => 'Asignar a Usuario',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Active' => 'Activo',
        'Inactive' => 'Inactivo',
      ),
      'caseDistribution' => 
      array (
        '' => 'Ninguno',
        'Direct-Assignment' => 'Asignación directa',
        'Round-Robin' => 'Round-Robin',
        'Least-Busy' => 'Menos Ocupado',
      ),
      'smtpAuthMechanism' => 
      array (
        'plain' => 'PLAIN',
        'login' => 'LOGIN',
        'crammd5' => 'CRAM-MD5',
      ),
    ),
    'labels' => 
    array (
      'Create InboundEmail' => 'Crear Cuenta de Correo',
      'IMAP' => 'IMAP',
      'Actions' => 'Acciones',
      'Main' => 'Principal',
    ),
    'messages' => 
    array (
      'couldNotConnectToImap' => 'No se pudo conectar con el servidor IMAP',
    ),
  ),
  'Integration' => 
  array (
    'fields' => 
    array (
      'enabled' => 'Activado',
      'clientId' => 'ID Cliente',
      'clientSecret' => 'Secreto Cliente',
      'redirectUri' => 'Redireccionar URI',
      'apiKey' => 'Llave API',
      'createEmails' => 'Fetch sent Emails',
      'markEmailsOptedOut' => 'Mark email addresses as Opted Out for Unsubscribed Recipients',
      'hardBouncedAction' => 'Hard Bounced Handling',
    ),
    'titles' => 
    array (
      'GoogleMaps' => 'Mapas de Google',
    ),
    'messages' => 
    array (
      'selectIntegration' => 'Seleccionar una integración en menú',
      'noIntegrations' => 'No hay integraciones disponibles',
    ),
    'help' => 
    array (
      'Google' => '&lt;p&gt;&lt;b&gt;Obtener las credenciales de  OAuth 2.0 desde la Consola de Google Developers.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Visita &lt;a href="https://console.developers.google.com/project"&gt;Consola Google Developers&lt;/a&gt; para obtener las credenciales de  OAuth 2.0 tales como  ID Cliente y Secreto de Cliente que son conocidos por ambos Google y la aplicación EspoCRM.&lt;/p&gt;',
      'GoogleMaps' => '
 &lt;p&gt;Obtenga la llave API &lt;a href="https://developers.google.com/maps/documentation/javascript/get-api-key"&gt;aquí&lt;/a&gt;.&lt;/p&gt; ',
      'MailChimp' => '&lt;p&gt;&lt;b&gt;MailChimp Configuration&lt;/b&gt;&lt;/p&gt;&lt;p&gt;You can get your API Key in your MailChimp Account, in Extras tab select API keys, or click &lt;a href=\'https://admin.mailchimp.com/account/api/\' target=\'_blank\'&gt;here&lt;/a&gt;&lt;/p&gt;',
    ),
    'options' => 
    array (
      'hardBouncedAction' => 
      array (
        'setAsInvalid' => 'Set Email Address as Invalid',
        'removeFromList' => 'Remove from List',
        'setAsInvalidAndRemove' => 'Both of Actions',
      ),
    ),
    'tooltips' => 
    array (
      'createEmails' => 'Fetch sent emails from MailChimp and relate to your Recipients in EspoCRM. Recommendation: Do not fetch emails if you use big recipient lists.',
    ),
  ),
  'Job' => 
  array (
    'fields' => 
    array (
      'status' => 'Estado',
      'executeTime' => 'Ejecutar a',
      'executedAt' => 'Executed At',
      'startedAt' => 'Started At',
      'attempts' => 'Intentos Izquierda',
      'failedAttempts' => 'Intentos Fallidos',
      'serviceName' => 'Servicio',
      'method' => 'Método (obsoleto)',
      'methodName' => 'Método',
      'scheduledJob' => 'Tarea Agendada',
      'scheduledJobJob' => 'Nombre del Trabajo Agendado',
      'data' => 'Datos',
      'targetType' => 'Target Type',
      'targetId' => 'Target ID',
      'number' => 'Number',
      'queue' => 'Queue',
      'job' => 'Job',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Pending' => 'Pendiente',
        'Success' => 'Correcto',
        'Running' => 'en ejecución...',
        'Failed' => 'Falló',
      ),
    ),
  ),
  'LayoutManager' => 
  array (
    'fields' => 
    array (
      'width' => 'Ancho (%)',
      'link' => 'Enlace',
      'notSortable' => 'No ordenable',
      'align' => 'Alinear',
      'panelName' => 'Nombre del Panel',
      'style' => 'Estilo',
      'sticked' => 'Pegado',
      'isLarge' => 'Tamaño de fuente grande',
      'dynamicLogicVisible' => 'Condiciones que hacen visible el panel',
    ),
    'options' => 
    array (
      'align' => 
      array (
        'left' => 'Izquierda',
        'right' => 'Derecha',
      ),
      'style' => 
      array (
        'default' => 'Default',
        'success' => 'Correcto',
        'danger' => 'Peligro',
        'info' => 'Info',
        'warning' => 'Precaución',
        'primary' => 'Primario',
      ),
    ),
    'labels' => 
    array (
      'New panel' => 'Nuevo panel',
      'Layout' => 'Formato',
    ),
    'tooltips' => 
    array (
      'link' => 'If checked, then a field value will be displayed as a link pointing to the detail view of the record. Usually it is used for *Name* fields.',
    ),
  ),
  'LeadCapture' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'campaign' => 'Campaña',
      'isActive' => 'Está Activo',
      'subscribeToTargetList' => 'Suscribirse a Lista de Intereses',
      'subscribeContactToTargetList' => 'Suscribirse al Contacto, si existe',
      'targetList' => 'Lista de Intereses',
      'fieldList' => 'Campos de Propiedades',
      'optInConfirmation' => 'Doble Opt-In',
      'optInConfirmationEmailTemplate' => 'Plantilla de correo para confirmar Opt-In',
      'optInConfirmationLifetime' => 'Rango de Validez (en horas) de la confirmación Opt-In',
      'optInConfirmationSuccessMessage' => 'Texto para mostrar después de la confirmación Opt-In',
      'leadSource' => 'Referencia Orígen',
      'apiKey' => 'Llave API',
      'targetTeam' => 'Equipo Interesante',
      'exampleRequestMethod' => 'Método',
      'exampleRequestUrl' => 'URL',
      'exampleRequestPayload' => 'Propiedades',
      'createLeadBeforeOptInConfirmation' => 'Create Lead before confirmation',
      'skipOptInConfirmationIfSubscribed' => 'Skip confirmation if lead is already in target list',
      'smtpAccount' => 'SMTP Account',
      'inboundEmail' => 'Group Email Account',
      'duplicateCheck' => 'Duplicate Check',
    ),
    'links' => 
    array (
      'targetList' => 'Lista de Intereses',
      'campaign' => 'Campaña',
      'optInConfirmationEmailTemplate' => 'Plantilla de confirmación de Opt-In',
      'targetTeam' => 'Equipo Interesante',
      'inboundEmail' => 'Group Email Account',
      'logRecords' => 'Historial',
    ),
    'labels' => 
    array (
      'Create LeadCapture' => 'Crear Punto de Entrada',
      'Generate New API Key' => 'Generar Nueva Llave API',
      'Request' => 'Solicitud',
      'Confirm Opt-In' => 'Confirmar Opt-In',
    ),
    'messages' => 
    array (
      'generateApiKey' => 'Crear Nueva Llave API',
      'optInConfirmationExpired' => 'La liga para confirmación de Opt-In ha expirado.',
      'optInIsConfirmed' => 'El Opt-In se ha confirmado.',
    ),
    'tooltips' => 
    array (
      'optInConfirmationSuccessMessage' => 'Soporta Reducción',
    ),
  ),
  'LeadCaptureLogRecord' => 
  array (
    'fields' => 
    array (
      'number' => 'Número',
      'data' => 'Dato',
      'target' => 'Interés',
      'leadCapture' => 'Capturar Referencia',
      'createdAt' => 'Ingresado el',
      'isCreated' => 'La Referencia fue creada',
    ),
    'links' => 
    array (
      'leadCapture' => 'Capturar Referencia',
      'target' => 'Interés',
    ),
  ),
  'Note' => 
  array (
    'fields' => 
    array (
      'post' => 'Guardar',
      'attachments' => 'Adjuntos',
      'targetType' => 'Interés',
      'teams' => 'Equipos',
      'users' => 'Usuarios',
      'portals' => 'Portales',
      'type' => 'Tipo',
      'isGlobal' => 'Es Global',
      'isInternal' => 'Es interno (para usuarios internos)',
      'related' => 'Relacionada',
      'createdByGender' => 'Creado(a) por Género',
      'data' => 'Datos',
      'number' => 'Número',
    ),
    'filters' => 
    array (
      'all' => 'Todos',
      'posts' => 'Entradas',
      'updates' => 'Actualizaciones',
    ),
    'options' => 
    array (
      'targetType' => 
      array (
        'self' => 'a mi mismo',
        'users' => 'a usuario(s) en particular',
        'teams' => 'a equipo(s) en particular',
        'all' => 'a todos los usuarios internos',
        'portals' => 'a los usuarios del portal',
      ),
      'type' => 
      array (
        'Post' => 'Publicar',
      ),
    ),
    'messages' => 
    array (
      'writeMessage' => 'Escriba su mensaje aquí',
    ),
    'links' => 
    array (
      'superParent' => 'Super Padre',
      'related' => 'Relacionado',
    ),
  ),
  'PhoneNumber' => 
  array (
    'fields' => 
    array (
      'type' => 'Type',
      'optOut' => 'Opted Out',
      'invalid' => 'Invalid',
    ),
    'presetFilters' => 
    array (
      'orphan' => 'Orphan',
    ),
  ),
  'Portal' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'logo' => 'Logo',
      'url' => 'URL',
      'portalRoles' => 'Roles',
      'isActive' => 'Está Activo',
      'isDefault' => 'Es Default',
      'tabList' => 'Lista de Tabuladores',
      'quickCreateList' => 'Crear Lista Rápida',
      'companyLogo' => 'Logo',
      'theme' => 'Tema',
      'language' => 'Idioma',
      'dashboardLayout' => 'Diseño del Tablero',
      'dateFormat' => 'Formato de Fecha',
      'timeFormat' => 'Formato de Hora',
      'timeZone' => 'Zona Horaria',
      'weekStart' => 'Primer Día de la Semana',
      'defaultCurrency' => 'Moneda Default',
      'customUrl' => 'URL Personalizado',
      'customId' => 'ID Personalizado',
    ),
    'links' => 
    array (
      'users' => 'Usuarios',
      'portalRoles' => 'Roles',
      'notes' => 'Notas',
      'articles' => 'Artículos de la Base de Conocimientos',
    ),
    'tooltips' => 
    array (
      'portalRoles' => 'Los Roles del Portal indicados se aplicarán a todos los usuarios del portal',
    ),
    'labels' => 
    array (
      'Create Portal' => 'Crear Portal',
      'User Interface' => 'Interfaz del Usuario',
      'General' => 'General',
      'Settings' => 'Configuración',
    ),
  ),
  'PortalRole' => 
  array (
    'fields' => 
    array (
      'exportPermission' => 'Permisos de Exportación',
      'massUpdatePermission' => 'Permiso de Actualización Masiva',
    ),
    'links' => 
    array (
      'users' => 'Usuarios',
    ),
    'tooltips' => 
    array (
      'exportPermission' => 'Define si los usuarios del portal pueden exportar registros.',
      'massUpdatePermission' => 'Define si los usuarios del portal pueden hacer actualizaciones masivas de registros.',
    ),
    'labels' => 
    array (
      'Access' => 'Acceder',
      'Create PortalRole' => 'Crear Rol del Portal',
      'Scope Level' => 'Alcance',
      'Field Level' => 'Nivel del Campo',
    ),
  ),
  'PortalUser' => 
  array (
    'labels' => 
    array (
      'Create PortalUser' => 'Crear un Usuario del Portal',
    ),
  ),
  'Preferences' => 
  array (
    'fields' => 
    array (
      'dateFormat' => 'Formato de fecha',
      'timeFormat' => 'Formato de tiempo',
      'timeZone' => 'Zona Horaria',
      'weekStart' => 'Primer día de la semana',
      'thousandSeparator' => 'Separador de miles',
      'decimalMark' => 'Separador decimal',
      'defaultCurrency' => 'Moneda Default',
      'currencyList' => 'Lista de Moneda',
      'language' => 'Idioma',
      'smtpServer' => 'Servidor',
      'smtpPort' => 'Puerto',
      'smtpAuth' => 'Autorizar',
      'smtpSecurity' => 'Seguridad',
      'smtpUsername' => 'Nombre de Usuario',
      'emailAddress' => 'Correo Electrónico',
      'smtpPassword' => 'Contraseña',
      'smtpEmailAddress' => 'Correo Electrónico',
      'exportDelimiter' => 'Exportar Delimitador',
      'receiveAssignmentEmailNotifications' => 'Notificaciones por correo sobre asignaciones',
      'receiveMentionEmailNotifications' => 'Notificaciones por correo sobre menciones en publicaciones',
      'receiveStreamEmailNotifications' => 'Notificar por correo las publicaciones y actualizaciones de estátus',
      'assignmentNotificationsIgnoreEntityTypeList' => 'In-app assignment notifications',
      'assignmentEmailNotificationsIgnoreEntityTypeList' => 'Email assignment notifications',
      'autoFollowEntityTypeList' => 'Seguimiento-automático Global',
      'signature' => 'Firma de correo',
      'dashboardTabList' => 'Lista de Pestañas',
      'defaultReminders' => 'Recordatorios Default',
      'theme' => 'Tema',
      'useCustomTabList' => 'Lista de Pestañas Personalizada',
      'tabList' => 'Lista de Pestañas',
      'emailReplyToAllByDefault' => 'Responder a todos por default',
      'dashboardLayout' => 'Formato del Tablero',
      'emailReplyForceHtml' => 'Responder correo en HTML',
      'doNotFillAssignedUserIfNotRequired' => 'No pre-llenar el campo de usuario al crear un registro',
      'followEntityOnStreamPost' => 'Seguimiento-automático del registro al publicarlo en el Flujo',
      'followCreatedEntities' => 'Seguimiento-automático de los registros creados',
      'followCreatedEntityTypeList' => 'Seguimiento-automático de los registros de tipos de entidad específicos',
      'emailUseExternalClient' => 'Use un cliente externo de correo',
      'scopeColorsDisabled' => 'Desactivar colores en alcance',
      'tabColorsDisabled' => 'Desactivar colores en pestañas',
    ),
    'links' => 
    array (
    ),
    'options' => 
    array (
      'weekStart' => 
      array (
        0 => 'Domingo',
        1 => 'Lunes',
      ),
    ),
    'labels' => 
    array (
      'Notifications' => 'Notificaciones',
      'User Interface' => 'Interfaz de Usuario',
      'SMTP' => 'SMTP',
      'Misc' => 'Misceláneos',
      'Locale' => 'Localización',
      'Reset Dashboard to Default' => 'Restaurar el Tablero default',
    ),
    'tooltips' => 
    array (
      'autoFollowEntityTypeList' => 'Seguir automáticamente TODOS los nuevos registros (de cualquier usuario) de los tipos de entidad seleccionados.  Así podrá ver información del flujo y recibir notificaciones sobre todo lo registrado en el sistema.',
      'doNotFillAssignedUserIfNotRequired' => 'El registro creado por el usuario asignado no será llenado con el propio usuario, a menos que el campo sea requerido.',
      'followCreatedEntities' => 'Cuando se creen nuevos registros, se seguirán automáticamente, aunque sean asignados a otro usuario.',
      'followCreatedEntityTypeList' => 'Cuando se creen nuevos registros de cierto tipo de entidades, se seguirán automáticamente, aunque sean asignados a otro usuario.',
    ),
  ),
  'Role' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'roles' => 'Roles',
      'assignmentPermission' => 'Asignación de permisos',
      'userPermission' => 'Permisos de Usuario',
      'portalPermission' => 'Permisos del Portal',
      'groupEmailAccountPermission' => 'Permisos de Grupos de Cuentas de Correo',
      'exportPermission' => 'Permisos de exportación',
      'massUpdatePermission' => 'Permiso de Actualización Masiva',
      'dataPrivacyPermission' => 'Permiso de Datos Privados',
    ),
    'links' => 
    array (
      'users' => 'Usuarios',
      'teams' => 'Equipos',
    ),
    'tooltips' => 
    array (
      'assignmentPermission' => 'Permite restringir la habilidad para asignar registros y enviar mensajes a otros usuarios.

todos - sin restricción

equipo - sólo a sus compañeros

no - sólo a sí mismo',
      'userPermission' => 'Permite restringir la capacidad de los usuarios para ver tareas, calendarios y el flujo de otros usuarios.

todos  - pueden ver todo

equipo - pueden ver las actividades de su equipo

no - sólo las propias',
      'portalPermission' => 'Define un acceso a la información del portal, permitiendo enviar mensajes a los usuarios del portal',
      'groupEmailAccountPermission' => 'Define el acceso a los grupos de cuentas de corros, la capacida de enviar correos desde grupos SMTP.',
      'exportPermission' => 'Define si los usuarios pueden exportar registros.',
      'massUpdatePermission' => 'Define si los usuarios pueden hacer actualizaciones masivas de registros.',
      'dataPrivacyPermission' => 'Permite ver y borrar datos personales.',
    ),
    'labels' => 
    array (
      'Access' => 'Acceso',
      'Create Role' => 'Crear Rol',
      'Scope Level' => 'Alcance',
      'Field Level' => 'Nivel del Campo',
    ),
    'options' => 
    array (
      'accessList' => 
      array (
        'not-set' => 'sin definir',
        'enabled' => 'activado',
        'disabled' => 'desactivado',
      ),
      'levelList' => 
      array (
        'all' => 'todos',
        'team' => 'equipo',
        'account' => 'cuenta',
        'contact' => 'contacto',
        'own' => 'propio',
        'no' => 'no',
        'yes' => 'si',
        'not-set' => 'sin definir',
      ),
    ),
    'actions' => 
    array (
      'read' => 'Leer',
      'edit' => 'Editar',
      'delete' => 'Borrar',
      'stream' => 'Flujo',
      'create' => 'Crear',
    ),
    'messages' => 
    array (
      'changesAfterClearCache' => 'Los cambios al Control de Acceso serán aplicados después de borrar el Cache',
    ),
  ),
  'ScheduledJob' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estátus',
      'job' => 'Trabajo',
      'scheduling' => 'Agendar',
    ),
    'links' => 
    array (
      'log' => 'Historial',
    ),
    'labels' => 
    array (
      'Create ScheduledJob' => 'Crear Tarea Agendada',
    ),
    'options' => 
    array (
      'job' => 
      array (
        'Cleanup' => 'Limpiar',
        'CheckInboundEmails' => 'Comprobar Correos Entrantes',
        'CheckEmailAccounts' => 'Compruebe cuentas de correo personales',
        'SendEmailReminders' => 'Enviar Recordatorios por Correo',
        'AuthTokenControl' => 'Control de la Clave de Autorización',
        'SendEmailNotifications' => 'Enviar Notificaciones por Correo',
        'CheckNewVersion' => 'Verificar Nueva Versión',
        'ProcessWebhookQueue' => 'Process Webhook Queue',
        'ProcessMassEmail' => 'Enviar Correo Masivo',
        'ControlKnowledgeBaseArticleStatus' => 'Controlar Estátus de la Base de Conocimientos',
        'SynchronizeEventsWithGoogleCalendar' => 'Google Calendar Sync',
        'MailChimpSyncData' => ' MailChimp Sync',
        'ReportTargetListSync' => 'Sync Target Lists with Reports',
        'ScheduleReportSending' => 'Schedule Report Sending',
        'RunScheduledWorkflows' => 'Run Scheduled Workflows',
      ),
      'cronSetup' => 
      array (
        'linux' => '&lt;b&gt;Nota&lt;/b&gt;: Agregue esta línea al archivo crontab de su servidor para que ejecute los trabajos agendados de EspoCRM:',
        'mac' => '&lt;b&gt;Nota&lt;/b&gt;: Agregue esta línea al archivo crontab de su servidor para que ejecute los trabajos agendados de EspoCRM:',
        'windows' => '&lt;b&gt;Nota&lt;/b&gt;: Genere un archivo por lotes con los siguientes comandos para ejecutar trabajos programados de EspoCRM mediante el Programador de Tareas de Windows:',
        'default' => 'Nota: Agregue este comando a su CronJob (Tarea Agendada):',
      ),
      'status' => 
      array (
        'Active' => 'Activo',
        'Inactive' => 'Inactivo',
      ),
    ),
    'tooltips' => 
    array (
      'scheduling' => 'Notación CRONTAB.  Indica la frecuencia de ejecución.

*/5 * * * * - cada 5 minutos

0 */2 * * * - cada 2 horas

30 1 * * * - a la 01:30 diariamente

0 0 1 * * - el primer día del mes',
    ),
  ),
  'ScheduledJobLogRecord' => 
  array (
    'fields' => 
    array (
      'status' => 'Estátus',
      'executionTime' => 'Tiempo de Ejecución',
      'target' => 'Interés',
    ),
  ),
  'Settings' => 
  array (
    'fields' => 
    array (
      'useCache' => 'Usar Cache',
      'dateFormat' => 'Formato de Fecha',
      'timeFormat' => 'Formato de Hora',
      'timeZone' => 'Zona Horaria',
      'weekStart' => 'Primer día de la semana',
      'thousandSeparator' => 'Separador de miles',
      'decimalMark' => 'Separador Decimal',
      'defaultCurrency' => 'Moneda Default',
      'baseCurrency' => 'Moneda Base',
      'currencyRates' => 'Valores Tarifa',
      'currencyList' => 'Lista de Moneda',
      'language' => 'Idioma',
      'companyLogo' => 'Logo Compañia',
      'smtpServer' => 'Servidor',
      'smtpPort' => 'Puerto',
      'smtpAuth' => 'Autorizar',
      'smtpSecurity' => 'Seguridad',
      'smtpUsername' => 'Nombre de Usuario',
      'emailAddress' => 'Correo electrónico',
      'smtpPassword' => 'Contraseña',
      'outboundEmailFromName' => 'De (Nombre)',
      'outboundEmailFromAddress' => 'De (Dirección)',
      'outboundEmailIsShared' => 'Es Compartido',
      'recordsPerPage' => 'Registros por Página',
      'recordsPerPageSmall' => 'Registros Por Página (Pequeño)',
      'tabList' => 'Lista de Pestañas',
      'quickCreateList' => 'Crear Lista Rápida',
      'exportDelimiter' => 'Exportar Delimitador',
      'globalSearchEntityList' => 'Lista Búsqueda Global Entidad',
      'authenticationMethod' => 'Método de Autorización',
      'ldapHost' => 'Servidor',
      'ldapPort' => 'Puerto',
      'ldapAuth' => 'Autorizar',
      'ldapUsername' => 'Nombre Completo del Usuario ND',
      'ldapPassword' => 'Contraseña',
      'ldapBindRequiresDn' => 'Requiere ND para relacionarse',
      'ldapBaseDn' => 'ND Base',
      'ldapAccountCanonicalForm' => 'Forma Canónica de la Cuenta',
      'ldapAccountDomainName' => 'Nombre de Dominio de la Cuenta',
      'ldapTryUsernameSplit' => 'Intentar dividir el nombre de Usuario',
      'ldapPortalUserLdapAuth' => 'Usar Autenticación LDAP para Usuarios del Portal',
      'ldapCreateEspoUser' => 'Crear Usuario en EspoCRM',
      'ldapSecurity' => 'Seguridad',
      'ldapUserLoginFilter' => 'Filtro de Entrada del Usuario',
      'ldapAccountDomainNameShort' => 'Nombre Dominio Corto para la Cuenta',
      'ldapOptReferrals' => 'Referencias validadas',
      'ldapUserNameAttribute' => 'Atributo "Nombre Del Usuario"',
      'ldapUserObjectClass' => 'ObjectClass del Usuario',
      'ldapUserTitleAttribute' => 'Atributo "Título del Usuario"',
      'ldapUserFirstNameAttribute' => 'Atributo "Nombre del Usuario"',
      'ldapUserLastNameAttribute' => 'Atributo "Apellido del Usuario"',
      'ldapUserEmailAddressAttribute' => 'Atributo "Correo del Usuario"',
      'ldapUserTeams' => 'Equipos del Usuario',
      'ldapUserDefaultTeam' => 'Equipo default del Usuario',
      'ldapUserPhoneNumberAttribute' => 'Atributo "Teléfono del Usuario"',
      'ldapPortalUserPortals' => 'Portales Default del Usuario de Portal',
      'ldapPortalUserRoles' => 'Roles Default del Usuario de Portal',
      'exportDisabled' => 'Desactivar Exportación (Solo admin)',
      'assignmentNotificationsEntityList' => 'Entidades a las que se notificará sobre la asignación',
      'assignmentEmailNotifications' => 'Notificaciones sobre la asignación',
      'assignmentEmailNotificationsEntityList' => 'Alcances de las notificaciones por correo de la asignación',
      'streamEmailNotifications' => 'Notificaciones sobre actualizaciones en el flujo para usuarios internos',
      'portalStreamEmailNotifications' => 'Notificaciones de actualizaciones en el flujo para los usuarios del portal',
      'streamEmailNotificationsEntityList' => 'Alcances de las notificaciones por correo del flujo',
      'streamEmailNotificationsTypeList' => 'Que cosa notificar',
      'emailNotificationsDelay' => 'Delay of email notifications (in seconds)',
      'b2cMode' => 'Modo B2C',
      'avatarsDisabled' => 'Desactivar Avatars',
      'followCreatedEntities' => 'Seguir los registros creados',
      'displayListViewRecordCount' => 'Mostrar el Total de Registros (en las vistas tipo lista)',
      'theme' => 'Tema',
      'userThemesDisabled' => 'Desactivar Temas de Usuarios',
      'emailMessageMaxSize' => 'Tamaño máximo del Correo (MB)',
      'massEmailMaxPerHourCount' => 'Número mäximo de correos enviados por hora',
      'personalEmailMaxPortionSize' => 'Porción máxima recuperable de correo de cuentas personales',
      'inboundEmailMaxPortionSize' => 'Porción máxima recuperable de correo de cuentas de grupo',
      'maxEmailAccountCount' => 'Máximo número de cuentas de correo personal por usuario',
      'authTokenLifetime' => 'Vida de la Clave de Autorización (horas)',
      'authTokenMaxIdleTime' => 'Máximo tiempo de inactividad de la Clave de Autorización (horas)',
      'dashboardLayout' => 'Diseño del Tablero (default)',
      'siteUrl' => 'URL del Sitio',
      'addressPreview' => 'Vista previa de la Dirección',
      'addressFormat' => 'Formato de la Dirección',
      'personNameFormat' => 'Person Name Format',
      'notificationSoundsDisabled' => 'Desactivar las Notificaciones con Sonido',
      'newNotificationCountInTitle' => 'Display new notification number in page title',
      'applicationName' => 'Nombre de la Aplicación',
      'calendarEntityList' => 'Lista de Entidades del Calendario',
      'mentionEmailNotifications' => 'Enviar correos de notificación sobre comentarios publicados',
      'massEmailDisableMandatoryOptOutLink' => 'Desactivar liga de confirmación obligatoria',
      'massEmailOpenTracking' => 'Email Open Tracking',
      'massEmailVerp' => 'Use VERP',
      'activitiesEntityList' => 'Lista de Entidades de Actividades',
      'historyEntityList' => 'Lista de Entidades del Historial',
      'currencyFormat' => 'Formato Moneda',
      'currencyDecimalPlaces' => 'Decimales en Moneda',
      'aclStrictMode' => 'Modo estricto ACL',
      'aclAllowDeleteCreated' => 'Permitir la eliminación de registros creados',
      'adminNotifications' => 'Notificaciones del sistema en el panel de administración',
      'adminNotificationsNewVersion' => 'Notificar cuando haya una nueva versión disponible de EspoCRM',
      'adminNotificationsNewExtensionVersion' => 'Notificar cuando haya nuevas versiones disponibles de extensiones',
      'textFilterUseContainsForVarchar' => 'Use el operador \'contiene\' para filtrar campos alfanuméricos',
      'authTokenPreventConcurrent' => 'Sólo se puede una clave de aut. por usuario',
      'scopeColorsDisabled' => 'Desactivar colores en alcance',
      'tabColorsDisabled' => 'Desactivar Colores en Pestañas',
      'tabIconsDisabled' => 'Desactivar Iconos en Pestañas',
      'emailAddressIsOptedOutByDefault' => 'Marcar direcciones como confirmadas',
      'outboundEmailBccAddress' => 'Direcciones CCO para clientes externos',
      'cleanupDeletedRecords' => 'Eliminar los registros borrados',
      'addressCountryList' => 'Lista para Autocompletar Direcciones de Países',
      'addressCityList' => 'Address City Autocomplete List',
      'addressStateList' => 'Address State Autocomplete List',
      'fiscalYearShift' => 'Inicio del Año Fiscal',
      'jobRunInParallel' => 'Jobs Run in Parallel',
      'jobMaxPortion' => 'Jobs Max Portion',
      'jobPoolConcurrencyNumber' => 'Jobs Pool Concurrency Number',
      'daemonInterval' => 'Daemon Interval',
      'daemonMaxProcessNumber' => 'Daemon Max Process Number',
      'daemonProcessTimeout' => 'Daemon Process Timeout',
      'cronDisabled' => 'Disable Cron',
      'maintenanceMode' => 'Modo de Mantenimiento',
      'useWebSocket' => 'Use WebSocket',
      'passwordRecoveryDisabled' => 'Disable password recovery',
      'passwordRecoveryForAdminDisabled' => 'Disable password recovery for admin users',
      'passwordGenerateLength' => 'Length of generated passwords',
      'passwordStrengthLength' => 'Minimum password length',
      'passwordStrengthLetterCount' => 'Number of letters required in password',
      'passwordStrengthNumberCount' => 'Number of digits required in password',
      'passwordStrengthBothCases' => 'Password must contain letters of both upper and lower case',
      'auth2FA' => 'Enable 2-Factor Authentication',
      'auth2FAMethodList' => 'Available 2FA methods',
    ),
    'options' => 
    array (
      'weekStart' => 
      array (
        0 => 'Domingo',
        1 => 'Lunes',
      ),
      'currencyFormat' => 
      array (
        1 => '10 MXP',
      ),
      'personNameFormat' => 
      array (
        'firstLast' => 'First Last',
        'lastFirst' => 'Last First',
        'firstMiddleLast' => 'First Middle Last',
        'lastFirstMiddle' => 'Last First Middle',
      ),
      'streamEmailNotificationsTypeList' => 
      array (
        'Post' => 'Publicaciones',
        'Status' => 'Actualizaciones de Estátus',
        'EmailReceived' => 'Correos recibidos',
      ),
      'auth2FAMethodList' => 
      array (
        'Totp' => 'TOTP',
      ),
    ),
    'tooltips' => 
    array (
      'massEmailVerp' => 'Variable envelope return path. For better handling of bounced messages. Make sure that your SMTP provider supports it.',
      'recordsPerPage' => 'Número de registros a desplegar inicialmente en las vistas',
      'recordsPerPageSmall' => 'Contador de registros en los paneles de información',
      'outboundEmailIsShared' => 'Permitir a los usuarios enviar correos desde esta dirección',
      'followCreatedEntities' => 'Los usuarios seguirán automáticamente los registros que ellos hayan creado.',
      'emailMessageMaxSize' => 'Los correos de entrada que excedan el máximo sólo tendrán asunto (sin texto ni adjuntos).',
      'authTokenLifetime' => 'Define cuanto duran las claves de aut.
0 - significa que no caduca.',
      'authTokenMaxIdleTime' => 'Define cuándo caduca la clave luego del último acceso.
0 - significa que no caduca.',
      'userThemesDisabled' => 'Si está marcado, los usuarios no podrán seleccionar otro tema',
      'ldapUsername' => 'The full system user DN which allows to search other users. E.g. "CN=LDAP System User,OU=users,OU=espocrm, DC=test,DC=lan". ',
      'ldapPassword' => 'Contraseña de acceso al servidor LDAP.',
      'ldapAuth' => 'Credenciales de acceso al servidor LDAP.',
      'ldapUserNameAttribute' => 'El atributo para identificar el usuario.  Por ejemplo, "userPrincipalName" o "sAMAcountName" para Active Directory.  "uid" en OpenLDAP.',
      'ldapUserObjectClass' => 'Atributo ObjectClass para buscar usuarios.  Por ejemplo, "person" para AD, "inetOrgPerson" para OpenLDAP.',
      'ldapAccountCanonicalForm' => 'The type of your account canonical form. There are 4 options:

- \'Dn\' - the form in the format \'CN=tester,OU=espocrm,DC=test, DC=lan\'.

- \'Username\' - the form \'tester\'.

- \'Backslash\' - the form \'COMPANY\\tester\'.

- \'Principal\' - the form \'tester@company.com\'.',
      'ldapBindRequiresDn' => 'La opción para formatear el nombre del usuario en forma ND.',
      'ldapBaseDn' => 'La base de datos default DN usada para buscar usuarios.  Por ejemplo, "OU=users,OU=espocrm,DC=test, DC=lan".',
      'ldapTryUsernameSplit' => 'Opción para separar el nombre de usuario del dominio.',
      'ldapOptReferrals' => 'si deben seguirse las referencias del cliente LDAP.',
      'ldapPortalUserLdapAuth' => 'Permitir a los usuarios del portal utilizar autenticación LDAP en vez de la de EspoCRM.',
      'ldapCreateEspoUser' => 'Esta opción permite que EspoCRM genere un usuario del LDAP.',
      'ldapUserFirstNameAttribute' => 'Atributo LDAP utilizado para determinar el nombre del usuario.  Por ejemplo, "givenname".',
      'ldapUserLastNameAttribute' => 'Atributo LDAP usado para determinar el apellido del usuario.  Por ejemplo, "sn".',
      'ldapUserTitleAttribute' => 'Atributo LDAP usado para determinar el título del usuario.  Por ejemplo, "title".',
      'ldapUserEmailAddressAttribute' => 'El atributo LDAP usado para indicar la dirección de correo del usuario.  Por ejemplo, "mail".',
      'ldapUserPhoneNumberAttribute' => 'El atributo LDAP usado para indicar el número de teléfono del usuario.  Por ejemplo, "telephoneNumber".',
      'ldapUserLoginFilter' => 'Filtro que permite restringir los usuarios que pueden usar EspoCRM.  Por ejemplo, "memberOf=CN=espoGroup, OU=groups,OU=espocrm, DC=test,DC=lan". ',
      'ldapAccountDomainName' => 'Dominio utilizado para acceder al servidor LDAP.',
      'ldapAccountDomainNameShort' => 'El dominio corto usado para acceder al servidor LDAP.',
      'ldapUserTeams' => 'Equipos creados por el usuario.  Para ver más, consulte el perfil del usuario.',
      'ldapUserDefaultTeam' => 'Equipo default creado por el Usuario.  Si requiere más información, consulte el perfil del Usuario.',
      'ldapPortalUserPortals' => 'Portales Default para el Usuario de Portal creado',
      'ldapPortalUserRoles' => 'Roles Default para el Usuario de Portal creado',
      'b2cMode' => 'EspoCRM viene configurado para B2B por default.  Puede cambiarlo a B2C.',
      'currencyDecimalPlaces' => 'Posiciones decimales. Si está vacío, se mostrarán todos los decimales',
      'aclStrictMode' => 'Activado: El acceso a los alcances estará prohibido si no se especifica en los roles
Desactivado: El acceso a los alcances será permitido si no se especifica en los roles',
      'aclAllowDeleteCreated' => 'Los usuarios podrán eliminar los registros que hayan creado, aunque no tengan permiso de borrado.',
      'textFilterUseContainsForVarchar' => 'Si no lo marca, se usará el operador \'starts with\' (inicia con).  Puede utilizar el comodín \'%\'.',
      'streamEmailNotificationsEntityList' => 'Notificaciones de actualización de registros del flujo.  Los Usuarios recibirán notificaciones por correo sólo para los tipos de entidad especificados.',
      'authTokenPreventConcurrent' => 'Los usuarios no podrán ingresar en distintos dispositivos al mismo tiempo',
      'emailAddressIsOptedOutByDefault' => 'Las nuevas direcciones de correo serán marcadas como confirmadas.',
      'cleanupDeletedRecords' => 'Los registros borrados serán eliminados de la base de datos después de un tiempo.',
      'jobRunInParallel' => 'Las tareas serán ejecutadas en paralelo',
      'jobPoolConcurrencyNumber' => 'Max número de procesos ejecutados simultaneamente',
      'jobMaxPortion' => 'Max número de tareas por ejecución',
      'daemonInterval' => 'Interval between process cron runs in seconds.',
      'daemonMaxProcessNumber' => 'Max number of cron processes run simultaneously.',
      'daemonProcessTimeout' => 'Max execution time (in seconds) allocated for a single cron process.',
      'cronDisabled' => 'Cron will not run.',
      'maintenanceMode' => 'Unicamente administradores pueden accesar el sistema',
    ),
    'labels' => 
    array (
      'System' => 'Sistema',
      'Locale' => 'Localización',
      'Search' => 'Busqueda',
      'Misc' => 'Miscelaneos',
      'SMTP' => 'SMTP',
      'Configuration' => 'Configuración',
      'In-app Notifications' => 'Notificaciones del CRM',
      'Email Notifications' => 'Notificaciones por Correo',
      'Currency Settings' => 'Configuración Moneda',
      'Currency Rates' => 'Tipo de Cambio por Divisa',
      'Mass Email' => 'Correo Masivo',
      'Test Connection' => 'Probar Conexión',
      'Connecting' => 'Conectando...',
      'Activities' => 'Actividades',
      'Admin Notifications' => 'Notificaciones al Administrador',
      'Passwords' => 'Passwords',
      '2-Factor Authentication' => '2-Factor Authentication',
    ),
    'messages' => 
    array (
      'ldapTestConnection' => 'La conexión se ha establecido satisfactoriamente',
    ),
  ),
  'Stream' => 
  array (
    'messages' => 
    array (
      'infoMention' => 'Type **@username** to mention user in the post.',
      'infoSyntax' => 'Available markdown syntax',
    ),
    'syntaxItems' => 
    array (
      'code' => 'code',
      'multilineCode' => 'multiline code',
      'strongText' => 'strong text',
      'emphasizedText' => 'emphasized text',
      'deletedText' => 'deleted text',
      'blockquote' => 'blockquote',
      'link' => 'link',
    ),
  ),
  'Team' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'roles' => 'Roles',
      'positionList' => 'Lista de Posiciones',
    ),
    'links' => 
    array (
      'users' => 'Usuarios',
      'notes' => 'Notas',
      'roles' => 'Roles',
      'inboundEmails' => 'Agrupar Cuentas de Correo',
    ),
    'tooltips' => 
    array (
      'roles' => 'Todos los usuarios de este equipo tendrán acceso a la configuración desde los roles seleccionados',
      'positionList' => 'Posiciones disponibles en este equipo. Por ejemplo Vendedor, Gerente.',
    ),
    'labels' => 
    array (
      'Create Team' => 'Crear Equipo',
    ),
  ),
  'Template' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'body' => 'Cuerpo',
      'entityType' => 'Tipo de Entidad',
      'header' => 'Encabezado',
      'footer' => 'Pié',
      'leftMargin' => 'Margen Izquierdo',
      'topMargin' => 'Margen Superior',
      'rightMargin' => 'Margen Derecho',
      'bottomMargin' => 'Margen Inferior',
      'printFooter' => 'Imprimir Pié',
      'footerPosition' => 'Posición del Pié',
      'variables' => 'Marcadores Disponibles',
      'pageOrientation' => 'Orientación de la Página',
      'pageFormat' => 'Formato de Papel',
      'pageWidth' => 'Page Width (mm)',
      'pageHeight' => 'Page Height (mm)',
      'fontFace' => 'Fuente',
      'accounts' => 'Accounts',
    ),
    'links' => 
    array (
      'accounts' => 'Accounts',
    ),
    'labels' => 
    array (
      'Create Template' => 'Crear Plantilla',
    ),
    'options' => 
    array (
      'pageOrientation' => 
      array (
        'Portrait' => 'Vertical',
        'Landscape' => 'Horizontal',
      ),
      'pageFormat' => 
      array (
        'Custom' => 'Custom',
      ),
      'placeholders' => 
      array (
        'pagebreak' => 'Page break',
        'today' => 'Hoy (fecha)',
        'now' => 'Ahora (fecha-hr)',
      ),
      'fontFace' => 
      array (
        'aealarabiya' => 'AlArabiya',
        'aefurat' => 'Aefurat',
        'cid0cs' => 'CID-0 cs',
        'cid0ct' => 'CID-0 ct',
        'cid0jp' => 'CID-0 jp',
        'cid0kr' => 'CID-0 kr',
        'courier' => 'Courier',
        'dejavusans' => 'DejaVuSans',
        'dejavusanscondensed' => 'DejaVu Sans Condensed',
        'dejavusansextralight' => 'DejaVu Sans Condensed',
        'dejavusansmono' => 'DejaVu Sans Mono',
        'dejavuserif' => 'DejaVu Serif',
        'dejavuserifcondensed' => 'DejaVu Serif Condensed',
        'freemono' => 'FreeMono',
        'freesans' => 'FreeSans',
        'freeserif' => 'FreeSerif',
        'helvetica' => 'Helvetica',
        'hysmyeongjostdmedium' => 'Hysmyeongjostd Medium',
        'kozgopromedium' => 'Kozgo Pro Medium',
        'kozminproregular' => 'Kozmin Pro Regular',
        'msungstdlight' => 'Msung Std Light',
        'pdfacourier' => 'PDFA Courier',
        'pdfahelvetica' => 'PDFA Helvetica',
        'pdfasymbol' => 'PDFA Symbol',
        'pdfatimes' => 'PDFA Times',
        'stsongstdlight' => 'STSong Std Light',
        'symbol' => 'Symbol',
        'times' => 'Times',
      ),
    ),
    'tooltips' => 
    array (
      'footer' => 'Use {pageNumber} para imprimir el número de página.',
      'variables' => 'Copiar/Pegar necesita un marcador para el Encabezado, Cuerpo o Pie.',
    ),
  ),
  'User' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'userName' => 'Nombre Usuario',
      'title' => 'Título',
      'type' => 'Tipo',
      'isAdmin' => 'Es Administrador',
      'defaultTeam' => 'Equipo Default',
      'emailAddress' => 'Correo electrónico',
      'phoneNumber' => 'Teléfono',
      'roles' => 'Roles',
      'portals' => 'Portales',
      'portalRoles' => 'Roles del Portal',
      'teamRole' => 'Posición',
      'password' => 'Contraseña',
      'currentPassword' => 'Contraseña Actual',
      'passwordConfirm' => 'Confirmar Contraseña',
      'newPassword' => 'Nueva Contraseña',
      'newPasswordConfirm' => 'Confirmar Contraseña Nueva',
      'yourPassword' => 'Your current password',
      'avatar' => 'Avatar',
      'isActive' => 'Está Activo',
      'isPortalUser' => 'Es Usuario del Portal',
      'contact' => 'Contacto',
      'accounts' => 'Cuentas',
      'account' => 'Cuenta (principal)',
      'sendAccessInfo' => 'Enviar al Usuario un correo con su Información de Acceso',
      'portal' => 'Portal',
      'gender' => 'Género',
      'position' => 'Puesto en el equipo',
      'ipAddress' => 'Dirección IP',
      'passwordPreview' => 'Contraseña Generada:',
      'isSuperAdmin' => 'Es Super-Administrador',
      'lastAccess' => 'Último Acceso',
      'apiKey' => 'Llave API',
      'secretKey' => 'Llave Secreta',
      'dashboardTemplate' => 'Dashboard Template',
      'authMethod' => 'Método de Autenticación',
      'auth2FA' => 'Enable 2-Factor Authentication',
      'auth2FAMethod' => '2FA Method',
      'auth2FATotpSecret' => '2FA TOTP Secret',
      'acceptanceStatus' => 'Estatus de Aprobación',
      'acceptanceStatusMeetings' => 'Estatus de Aceptación (Presentaciones)',
      'acceptanceStatusCalls' => 'Estátus de Aceptación (Llamadas)',
      'assignedUser1' => 'assignedUser1',
      'opportunities' => 'Opportunities',
      'opportunity' => 'Opportunity',
      'opportunities2' => 'Opportunities',
      'userParent' => 'UserParent',
      'usuariosAsignados' => 'Usuarios asignados',
    ),
    'links' => 
    array (
      'defaultTeam' => 'Equipo default',
      'teams' => 'Equipos',
      'roles' => 'Roles',
      'notes' => 'Notas',
      'portals' => 'Portales',
      'portalRoles' => 'Roles del Portal',
      'contact' => 'Contacto',
      'accounts' => 'Cuentas',
      'account' => 'Cuenta (principal)',
      'tasks' => 'Tareas',
      'dashboardTemplate' => 'Dashboard Template',
      'targetLists' => 'Listas de Intereses',
      'assignedUser2' => 'assignedUser2',
      'assignedUser1' => 'assignedUser1',
      'opportunities' => 'Opportunities',
      'opportunity' => 'Opportunity',
      'opportunities2' => 'Opportunities',
      'userParent' => 'UserParent',
      'usuariosAsignados' => 'Usuarios asignados',
    ),
    'labels' => 
    array (
      'Create User' => 'Crear Usuario',
      'Generate' => 'Generar',
      'Access' => 'Acceso',
      'Preferences' => 'Preferencias',
      'Change Password' => 'Cambiar Contraseña',
      'Teams and Access Control' => 'Equipos y Control de Acceso',
      'Forgot Password?' => '¿Olvidó la Contraseña?',
      'Password Change Request' => 'Solicitar Cambio de Contraseña',
      'Email Address' => 'Correo Electrónico',
      'External Accounts' => 'Cuentas Externas',
      'Email Accounts' => 'Cuentas de Correo',
      'Portal' => 'Portal',
      'Create Portal User' => 'Crear Usuario del Portal',
      'Proceed w/o Contact' => 'Proceder sin Contacto',
      'Generate New API Key' => 'Generar Nueva Llave API',
      'Generate New Password' => 'Generate New Password',
      'Code' => 'Code',
      'Back to login form' => 'Back to login form',
      'Requirements' => 'Requirements',
      'Security' => 'Security',
      'Reset 2FA' => 'Reset 2FA',
      'Secret' => 'Secret',
    ),
    'tooltips' => 
    array (
      'defaultTeam' => 'Todos los registros creados por este usuario serán relacionados a este equipo default.',
      'userName' => 'Letras a-z, números 0-9 y guiones bajos están permitidos',
      'isAdmin' => 'El usuario administrador puede tener acceso a todo.',
      'isActive' => 'Si lo desmarca, el usuario no podrá iniciar sesión.',
      'teams' => 'Equipos a los que este usuario pertenece. Nivel de control de acceso se hereda de los roles de equipo.',
      'roles' => 'Roles de acceso adicionales. Úselo si el usuario no pertenece a ningún equipo o si necesita ampliar el nivel de control de acceso sólo para este usuario.',
      'portalRoles' => 'Roles adicionales del portal.  Utilícelos para extender el nivel de acceso exclusivamente para este Usuario',
      'portals' => 'Portales a los que este Usuario tiene acceso',
    ),
    'messages' => 
    array (
      'passwordStrengthLength' => 'Must be at least {length} characters long.',
      'passwordStrengthLetterCount' => 'Must contain at least {count} letter(s).',
      'passwordStrengthNumberCount' => 'Must contain at least {count} digit(s).',
      'passwordStrengthBothCases' => 'Must contain letters of both upper and lower case.',
      'passwordWillBeSent' => 'La Contraseña será enviada al correo electrónico del usuario',
      'passwordChanged' => 'La Contraseña ha sido cambiada',
      'userCantBeEmpty' => 'El nombre de usuario no puede estar vacío',
      'wrongUsernamePasword' => 'Nombre de usuario/contraseña incorrectos',
      'emailAddressCantBeEmpty' => 'La dirección de correo no puede estar vacía',
      'userNameEmailAddressNotFound' => 'Nombre de Usuario/Correo no encontrado',
      'forbidden' => 'Prohibido, por favor intente después',
      'uniqueLinkHasBeenSent' => 'El enlace único ha sido enviado a la dirección de correo electrónico especificada.',
      'passwordChangedByRequest' => 'La contraseña ha sido cambiada.',
      'setupSmtpBefore' => 'Necesita configurar correctamente su &lt;a href="{url}"&gt;Servicio SMTP&lt;/a&gt; para que el sistema pueda enviarle su contraseña por correo.',
      'userNameExists' => 'Ese Usuario ya existe',
      'wrongCode' => 'Wrong code',
      'codeIsRequired' => 'Code is required',
      'enterTotpCode' => 'Enter a code from your authenticator app.',
      'verifyTotpCode' => 'Scan the QR-code with your mobile authenticator app. If you have a trouble with scanning, you can enter the secret manually. After that you will see a 6-digit code in your application. Enter this code in the field below.',
      'generateAndSendNewPassword' => 'A new password will be generated and sent to the user\'s email address.',
      'security2FaResetConfimation' => 'Are you sure you want to reset the current 2FA settings?',
      'ldapUserInEspoNotFound' => 'User is not found in EspoCRM. Contact your administrator to create the user.',
    ),
    'options' => 
    array (
      'gender' => 
      array (
        '' => 'No Definido',
        'Male' => 'Masculino',
        'Female' => 'Femenino',
        'Neutral' => 'Neutral',
      ),
      'type' => 
      array (
        'regular' => 'Regular',
        'admin' => 'Administrador',
        'portal' => 'Portal',
        'system' => 'Sistema',
        'super-admin' => 'Super-Administrador',
        'api' => 'API',
      ),
      'authMethod' => 
      array (
        'ApiKey' => 'Llave API',
        'Hmac' => 'HMAC',
      ),
    ),
    'boolFilters' => 
    array (
      'onlyMyTeam' => 'Sólo mi equipo',
    ),
    'presetFilters' => 
    array (
      'active' => 'Activo',
      'activePortal' => 'Portal Activo',
      'activeApi' => 'API Active',
    ),
  ),
  'Webhook' => 
  array (
    'labels' => 
    array (
      'Create Webhook' => 'Create Webhook',
    ),
    'fields' => 
    array (
      'event' => 'Event',
      'url' => 'URL',
      'isActive' => 'Is Active',
      'user' => 'API User',
      'entityType' => 'Entity Type',
      'field' => 'Field',
      'secretKey' => 'Secret Key',
    ),
    'links' => 
    array (
      'user' => 'User',
    ),
  ),
  'Account' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'emailAddress' => 'E-mail',
      'website' => 'Sito Web',
      'phoneNumber' => 'Teléfono',
      'billingAddress' => 'Dirección de Facturación',
      'shippingAddress' => 'Dirección de Entrega',
      'description' => 'Descripción',
      'sicCode' => 'Código SIC',
      'industry' => 'Industria',
      'type' => 'Tipo',
      'contactRole' => 'Título',
      'contactIsInactive' => 'Inactivo',
      'campaign' => 'Campaña',
      'targetLists' => 'Listas de Intereses',
      'targetList' => 'Lista de Intereses',
      'originalLead' => 'Referencia Original',
      'institutionalDiscount' => '% Descuento institucional',
      'billingEmail' => 'E-mail de facturación',
      'termsAdvancePercent' => '% Anticipo',
      'termsDeliveryPercent' => '% Entrega',
      'termsInstalationPercent' => '% Instalación',
      'termsAdvanceDays' => 'Anticipo (Días)',
      'termsDeliveryDays' => 'Entrega (Días)',
      'termsInstalationDays' => 'Instalación (Días)',
      'deliveryDays' => 'Días de entrega',
      'partialDeliveries' => 'Entregas parciales',
      'deliverySchedules' => 'Horarios de entrega',
      'iva' => 'IVA',
      'logo' => 'Logo',
      'rfc' => 'R.F.C',
      'metodoDePago' => 'Metodo de pago',
      'formaDePago' => 'Forma de pago',
      'usoDeCFDI' => 'Uso de CFDI',
      'informacionCuentasPorPagar' => 'Informacion cuentas por pagar',
      'templates' => 'Templates',
      'tecnologiass' => 'Tecnologiass',
      'solucioness' => 'Solucioness',
      'soluciones' => 'Soluciones',
      'razonSocial' => 'Razon Social',
      'parqueinds' => 'Parque Industrial',
    ),
    'links' => 
    array (
      'contacts' => 'Contactos',
      'opportunities' => 'Oportunidades',
      'cases' => 'Casos',
      'documents' => 'Documentos',
      'meetingsPrimary' => 'Presentaciones (ampliado)',
      'callsPrimary' => 'Llamadas (ampliado)',
      'tasksPrimary' => 'Tareas (ampliado)',
      'emailsPrimary' => 'Correos (ampliado)',
      'targetLists' => 'Listas de Intereses',
      'campaignLogRecords' => 'Historial de Campañas',
      'campaign' => 'Campaña',
      'portalUsers' => 'Usuarios del Portal',
      'originalLead' => 'Referencia Original',
      'quotes' => 'Quotes',
      'templates' => 'Templates',
      'tecnologiass' => 'Tecnologiass',
      'solucioness' => 'Solucioness',
      'soluciones' => 'Soluciones',
    ),
    'options' => 
    array (
      'type' => 
      array (
        'Customer' => 'Cliente',
        'Investor' => 'Inversor',
        'Partner' => 'Socio',
        'Reseller' => 'Revendedor',
        '' => '',
        'Interno' => 'Interno',
      ),
      'industry' => 
      array (
        'Aerospace' => 'Aeroespacial',
        'Agriculture' => 'Agricultura',
        'Advertising' => 'Publicidad',
        'Apparel & Accessories' => 'Ropa y Accesorios',
        'Architecture' => 'Arquitectura',
        'Automotive' => 'Automotriz',
        'Banking' => 'Banca',
        'Biotechnology' => 'Biotecnolodía',
        'Building Materials & Equipment' => 'Materiales de construcción y equipamiento',
        'Chemical' => 'Química',
        'Construction' => 'Construcción',
        'Computer' => 'Computación',
        'Defense' => 'Defensa',
        'Creative' => 'Creativo',
        'Culture' => 'Cultura',
        'Consulting' => 'Consultando',
        'Education' => 'Educación',
        'Electronics' => 'Electrónicos',
        'Electric Power' => 'Energía eléctrica',
        'Energy' => 'Energía',
        'Entertainment & Leisure' => 'Entretenimiento y Ocio',
        'Finance' => 'Finanzas',
        'Food & Beverage' => 'Alimentación y bebidas',
        'Grocery' => 'Comestibles',
        'Hospitality' => 'Hospitalidad',
        'Healthcare' => 'Cuidado de la Salud',
        'Insurance' => 'Seguros',
        'Legal' => 'Jurídico',
        'Manufacturing' => 'Fabricación',
        'Mass Media' => 'Medios masivos',
        'Mining' => 'Minería',
        'Music' => 'Música',
        'Marketing' => 'Marketing',
        'Publishing' => 'Publicaciones',
        'Petroleum' => 'Petróleo',
        'Real Estate' => 'Bienes Raices',
        'Retail' => 'Menudeo',
        'Shipping' => 'Entrega',
        'Service' => 'Servicio',
        'Support' => 'Soporte',
        'Sports' => 'Deportes',
        'Software' => 'Software',
        'Technology' => 'Tecnología',
        'Telecommunications' => 'Telecomunicaciones',
        'Television' => 'Televisión',
        'Testing, Inspection & Certification' => 'Prueba, Inspección y Certificación',
        'Transportation' => 'Transporte',
        'Travel' => 'Viaje',
        'Venture Capital' => 'Capital de Riesgo',
        'Wholesale' => 'Compra Total',
        'Water' => 'Agua',
      ),
      'deliveryDays' => 
      array (
        'Lunes' => 'Lunes',
        'Martes' => 'Martes',
        'Miercoles' => 'Miercoles',
        'Jueves' => 'Jueves',
        'Viernes' => 'Viernes',
      ),
      'partialDeliveries' => 
      array (
        'Si' => 'Si',
      ),
      'deliverySchedules' => 
      array (
      ),
      'parqueinds' => 
      array (
        'Pacifico' => 'Pacifico',
        'TIP' => 'TIP',
        'Bellas Artes' => 'Bellas Artes',
        'Otay Industrial' => 'Otay Industrial',
        'La Mesa' => 'La Mesa',
        'Maran' => 'Maran',
        'Los Pinos' => 'Los Pinos',
        'Las Brisas' => 'Las Brisas',
      ),
    ),
    'labels' => 
    array (
      'Create Account' => 'Crear Cuenta',
      'Copy Billing' => 'Copia Facturación',
      'Set Primary' => 'Es Primario',
    ),
    'presetFilters' => 
    array (
      'customers' => 'Clientes',
      'partners' => 'Socios',
      'recentlyCreated' => 'Recientemente Creado(a)',
    ),
    'tooltips' => 
    array (
      'iva' => 'Solo incluir el numero sin el simbolo (%)',
    ),
  ),
  'Calendar' => 
  array (
    'modes' => 
    array (
      'month' => 'Mes',
      'week' => 'Semana',
      'day' => 'Día',
      'agendaWeek' => 'Semana',
      'agendaDay' => 'Día',
      'timeline' => 'Cronograma',
    ),
    'labels' => 
    array (
      'Today' => 'Hoy',
      'Create' => 'Crear',
      'Shared' => 'Compartido',
      'Add User' => 'Agregar Usuario',
      'current' => 'actual',
      'time' => 'hora',
      'User List' => 'Lista de Usuarios',
      'Manage Users' => 'Usuarios',
      'View Calendar' => 'Ver Calendario',
      'Create Shared View' => 'Crear Vista Compartida',
    ),
  ),
  'Call' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'parent' => 'Padre',
      'status' => 'Estátus',
      'dateStart' => 'Fecha de Comienzo',
      'dateEnd' => 'Fecha de Finalización',
      'direction' => 'Dirección',
      'duration' => 'Duración',
      'description' => 'Descripción',
      'users' => 'Usuarios',
      'contacts' => 'Contactos',
      'leads' => 'Referencias',
      'reminders' => 'Recordatorios',
      'account' => 'Cuenta',
      'acceptanceStatus' => 'Estátus de Aprobación',
    ),
    'links' => 
    array (
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Planned' => 'Planeadas',
        'Held' => 'Retenida',
        'Not Held' => 'Pendiente',
      ),
      'direction' => 
      array (
        'Outbound' => 'Saliente',
        'Inbound' => 'Entrante',
      ),
      'acceptanceStatus' => 
      array (
        'None' => 'Ninguno',
        'Accepted' => 'Aprobado',
        'Declined' => 'Rechazado',
        'Tentative' => 'Tentativa',
      ),
    ),
    'massActions' => 
    array (
      'setHeld' => 'Marcar como Retenida',
      'setNotHeld' => 'Marcar como Pendiente',
    ),
    'labels' => 
    array (
      'Create Call' => 'Crear Llamada',
      'Set Held' => 'Marcar como Retenida',
      'Set Not Held' => 'Marcar como Pendiente',
      'Send Invitations' => 'Enviar Invitaciones',
    ),
    'presetFilters' => 
    array (
      'planned' => 'Planeadas',
      'held' => 'Retenida',
      'todays' => 'De Hoy',
    ),
  ),
  'Campaign' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'description' => 'Descripción',
      'status' => 'Estátus',
      'type' => 'Tipo',
      'startDate' => 'Fecha de Inicio',
      'endDate' => 'Fecha de Fin',
      'targetLists' => 'Lista de Intereses',
      'excludingTargetLists' => 'Listas de Intereses Excluidas',
      'sentCount' => 'Enviado',
      'openedCount' => 'Abierto',
      'clickedCount' => 'Leídos',
      'optedOutCount' => 'Rechazado',
      'bouncedCount' => 'Rebotados',
      'optedInCount' => 'Opt-In aceptado',
      'hardBouncedCount' => 'No Existen',
      'softBouncedCount' => 'No Aceptados',
      'leadCreatedCount' => 'Referencias Creadas',
      'revenue' => 'Ingresos',
      'revenueConverted' => 'ingresos (convertido)',
      'budget' => 'Presupuesto',
      'budgetConverted' => 'Presupuesto (convertido)',
      'budgetCurrency' => 'Budget Currency',
      'contactsTemplate' => 'Formato de Contactos',
      'leadsTemplate' => 'Formato de Referencias',
      'accountsTemplate' => 'Formato de Cuentas',
      'usersTemplate' => 'Formato de Usuarios',
      'mailMergeOnlyWithAddress' => 'Saltar registros sin dirección capturada',
    ),
    'links' => 
    array (
      'targetLists' => 'Listas de Intereses',
      'excludingTargetLists' => 'Listas de Intereses Excluidas',
      'accounts' => 'Cuentas',
      'contacts' => 'Contactos',
      'leads' => 'Referencias',
      'opportunities' => 'Oportunidades',
      'campaignLogRecords' => 'Historial',
      'massEmails' => 'Correos Masivos',
      'trackingUrls' => 'Seguimiento a URLs',
      'contactsTemplate' => 'Formato de Contactos',
      'leadsTemplate' => 'Formato de Referencias',
      'accountsTemplate' => 'Formato de Cuentas',
      'usersTemplate' => 'Formato de Usuarios',
    ),
    'options' => 
    array (
      'type' => 
      array (
        'Email' => 'Correo electrónico',
        'Web' => 'Web',
        'Television' => 'Televisión',
        'Radio' => 'Radio',
        'Newsletter' => 'Periódico',
        'Mail' => 'Correo',
      ),
      'status' => 
      array (
        'Planning' => 'Planificación',
        'Active' => 'Activo',
        'Inactive' => 'Inactivo',
        'Complete' => 'Completada',
      ),
    ),
    'labels' => 
    array (
      'Create Campaign' => 'Crear Campaña',
      'Target Lists' => 'Listas de Intereses',
      'Statistics' => 'Estadísticas',
      'hard' => 'duro',
      'soft' => 'suave',
      'Unsubscribe' => 'Cancelar suscripción',
      'Mass Emails' => 'Correos Masivos',
      'Email Templates' => 'Correo Modelo',
      'Unsubscribe again' => 'Cancelar otra vez la suscripción',
      'Subscribe again' => 'Volverse a suscribir',
      'Create Target List' => 'Crear Lista de Obejtivos',
      'Mail Merge' => 'Generar Correos',
      'Generate Mail Merge PDF' => 'Generar PDF para Correos',
      'MailChimp Campaign Sync' => 'MailChimp Campaign Sync',
      'MailChimp Sync' => 'MailChimp Sync',
    ),
    'presetFilters' => 
    array (
      'active' => 'Activo',
    ),
    'messages' => 
    array (
      'unsubscribed' => 'Usted ha cancelado la suscripción a nuestra lista de correo.',
      'subscribedAgain' => 'Usted se ha vuelto a suscribir.',
    ),
    'tooltips' => 
    array (
      'targetLists' => 'Intereses que deben recibir mensajes.',
      'excludingTargetLists' => 'Los intereses que no deben recibir mensajes.',
    ),
  ),
  'CampaignLogRecord' => 
  array (
    'fields' => 
    array (
      'action' => 'Acción',
      'actionDate' => 'Fecha',
      'data' => 'Datos',
      'campaign' => 'Campaña',
      'parent' => 'Interés',
      'object' => 'Objeto',
      'application' => 'Aplicacion',
      'queueItem' => 'Item de la Lista',
      'stringData' => 'Datos Alfanuméricos',
      'stringAdditionalData' => 'Datos Alfanuméricos Adicionales',
      'isTest' => 'Es una prueba',
    ),
    'links' => 
    array (
      'queueItem' => 'Elemento de la Cola',
      'parent' => 'Padre',
      'object' => 'Objeto',
      'campaign' => 'Campaña',
    ),
    'options' => 
    array (
      'action' => 
      array (
        'Sent' => 'Enviado',
        'Opened' => 'Abierto',
        'Opted Out' => 'Rechazado',
        'Bounced' => 'Rebotados',
        'Clicked' => 'Leído',
        'Lead Created' => 'Referencias Creadas',
        'Opted In' => 'Opt-In aceptado',
      ),
    ),
    'labels' => 
    array (
      'All' => 'Todos',
    ),
    'presetFilters' => 
    array (
      'sent' => 'Enviado',
      'opened' => 'Abierto',
      'optedOut' => 'Rechazado',
      'optedIn' => 'Opt-In aceptado',
      'bounced' => 'Rebotados',
      'clicked' => 'Leído',
      'leadCreated' => 'Referencia Creada',
    ),
  ),
  'CampaignTrackingUrl' => 
  array (
    'fields' => 
    array (
      'url' => 'URL',
      'action' => 'Action',
      'urlToUse' => 'Código para insertar en lugar de la URL',
      'message' => 'Message',
      'campaign' => 'Campaña',
    ),
    'links' => 
    array (
      'campaign' => 'Campaña',
    ),
    'labels' => 
    array (
      'Create CampaignTrackingUrl' => 'Crear Seguimiento a URLs',
    ),
    'options' => 
    array (
      'action' => 
      array (
        'Redirect' => 'Redirect',
        'Show Message' => 'Show Message',
      ),
    ),
    'tooltips' => 
    array (
      'url' => 'The recipient will be redirected to this location after they follow the link.',
      'message' => 'The message will be shown to the recipient after they follow the link. Markdown is supported.',
    ),
  ),
  'Case' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'number' => 'Número',
      'status' => 'Estátus',
      'account' => 'Cuenta',
      'contact' => 'Contacto',
      'contacts' => 'Contactos',
      'priority' => 'Prioridad',
      'type' => 'Tipo',
      'description' => 'Descripción',
      'inboundEmail' => 'Cuenta de Correo de Grupo',
      'lead' => 'Referencia',
      'attachments' => 'Adjuntos',
    ),
    'links' => 
    array (
      'inboundEmail' => 'Cuenta de Correo de Grupo',
      'account' => 'Cuenta',
      'contact' => 'Contacto (Primario)',
      'Contacts' => 'Contactos',
      'meetings' => 'Presentaciones',
      'calls' => 'Llamadas',
      'tasks' => 'Tareas',
      'emails' => 'Correos',
      'articles' => 'Artículos de la Base de Conocimientos',
      'lead' => 'Referencia',
      'attachments' => 'Adjuntos',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'New' => 'Nuevo',
        'Assigned' => 'Asignado',
        'Pending' => 'Pendiente',
        'Closed' => 'Cerrados',
        'Rejected' => 'Rechazado',
        'Duplicate' => 'Duplicar',
      ),
      'priority' => 
      array (
        'Low' => 'Baja',
        'Normal' => 'Normal',
        'High' => 'Alta',
        'Urgent' => 'Urgente',
      ),
      'type' => 
      array (
        'Question' => 'Pregunta',
        'Incident' => 'Incidente',
        'Problem' => 'Problema',
      ),
    ),
    'labels' => 
    array (
      'Create Case' => 'Crear Caso',
      'Close' => 'Cerrar',
      'Reject' => 'Rechazar',
      'Closed' => 'Cerrados',
      'Rejected' => 'Rechazado',
    ),
    'presetFilters' => 
    array (
      'open' => 'Abiertos',
      'closed' => 'Cerrados',
    ),
  ),
  'Contact' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'emailAddress' => 'Correo electrónico',
      'title' => 'Título de la Cuenta Primaria',
      'account' => 'Cuenta',
      'accounts' => 'Cuentas',
      'phoneNumber' => 'Teléfono',
      'accountType' => 'Tipo de Cuenta',
      'doNotCall' => 'No Llamar',
      'address' => 'Dirección',
      'opportunityRole' => 'Rol de Oportunidad',
      'accountRole' => 'Area',
      'description' => 'Descripción',
      'campaign' => 'Campaña',
      'targetLists' => 'Listas de Intereses',
      'targetList' => 'Lista de Intereses',
      'portalUser' => 'Usuario del Portal',
      'originalLead' => 'Referencia Original',
      'acceptanceStatus' => 'Estatus de Aprobación',
      'accountIsInactive' => 'Cuenta Inactiva',
      'acceptanceStatusMeetings' => 'Estatus de Aceptación (Presentaciones)',
      'acceptanceStatusCalls' => 'Estátus de Aceptación (Llamadas)',
      'jobTitle' => 'Puesto Laboral',
    ),
    'links' => 
    array (
      'opportunities' => 'Oportunidades',
      'cases' => 'Casos',
      'targetLists' => 'Listas de Intereses',
      'campaignLogRecords' => 'Registrar Campaña',
      'campaign' => 'Campaña',
      'account' => 'Cuenta (Primaria)',
      'accounts' => 'Cuentas',
      'casesPrimary' => 'Casos (Primario)',
      'tasksPrimary' => 'Tareas (extendidas)',
      'opportunitiesPrimary' => 'Opportunities (Primary)',
      'portalUser' => 'Usuario del Portal',
      'originalLead' => 'Referencia Original',
      'documents' => 'Documentos',
      'quotesBilling' => 'Quotes (Billing)',
      'quotesShipping' => 'Quotes (Shipping)',
    ),
    'labels' => 
    array (
      'Create Contact' => 'Crear Contacto',
    ),
    'options' => 
    array (
      'opportunityRole' => 
      array (
        '' => '',
        'Decision Maker' => 'Tomador de Desiciones',
        'Evaluator' => 'Evaluador',
        'Influencer' => 'Factor de Influencia',
      ),
    ),
    'presetFilters' => 
    array (
      'portalUsers' => 'Usuarios del Portal',
      'notPortalUsers' => 'No son Usuarios del Portal',
      'accountActive' => 'Activo',
    ),
  ),
  'Document' => 
  array (
    'labels' => 
    array (
      'Create Document' => 'Crear Documento',
      'Details' => 'Detalles',
    ),
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estátus',
      'file' => 'Archivo',
      'type' => 'Tipo',
      'publishDate' => 'Publicar Fecha',
      'expirationDate' => 'Fecha de Expiración',
      'description' => 'Descripción',
      'accounts' => 'Cuentas',
      'folder' => 'Carpeta',
    ),
    'links' => 
    array (
      'accounts' => 'Cuentas',
      'opportunities' => 'Oportunidades',
      'folder' => 'Carpeta',
      'leads' => 'Referencias',
      'contacts' => 'Contactos',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Active' => 'Activo',
        'Draft' => 'Borrador',
        'Expired' => 'Expirado',
        'Canceled' => 'Cancelado',
      ),
      'type' => 
      array (
        '' => 'Ninguno',
        'Contract' => 'Contrato',
        'NDA' => 'AdC',
        'EULA' => 'EULA',
        'License Agreement' => 'Contrato de Licencia',
      ),
    ),
    'presetFilters' => 
    array (
      'active' => 'Activo',
      'draft' => 'Borrador',
    ),
  ),
  'DocumentFolder' => 
  array (
    'labels' => 
    array (
      'Create DocumentFolder' => 'Crear Carpeta de Documentos',
      'Manage Categories' => 'Carpetas',
      'Documents' => 'Documentos',
    ),
    'links' => 
    array (
      'documents' => 'Documentos',
    ),
  ),
  'EmailQueueItem' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estátus',
      'target' => 'Interés',
      'sentAt' => 'Enviado',
      'attemptCount' => 'Intentos',
      'emailAddress' => 'Correo Electrónico',
      'massEmail' => 'Correo Masivo',
      'isTest' => 'Es una prueba',
    ),
    'links' => 
    array (
      'target' => 'Interés',
      'massEmail' => 'Correo Masivo',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Pending' => 'Pendiente',
        'Sent' => 'Enviado',
        'Failed' => 'Falló',
        'Sending' => 'Enviando',
      ),
    ),
    'presetFilters' => 
    array (
      'pending' => 'Pendiente',
      'sent' => 'Enviado',
      'failed' => 'Falló',
    ),
  ),
  'KnowledgeBaseArticle' => 
  array (
    'labels' => 
    array (
      'Create KnowledgeBaseArticle' => 'Crear Artículo',
      'Any' => 'Cualquiera',
      'Send in Email' => 'Enviar por Correo',
      'Move Up' => 'Mover Arriba',
      'Move Down' => 'Mover Abajo',
      'Move to Top' => 'Mover al Principio',
      'Move to Bottom' => 'Mover al Final',
    ),
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estátus',
      'type' => 'Tipo',
      'attachments' => 'Datos adjuntos',
      'publishDate' => 'Fecha de Publicación',
      'expirationDate' => 'Fecha de Expiración',
      'description' => 'Descripción',
      'body' => 'Cuerpo',
      'categories' => 'Categorías',
      'language' => 'Idioma',
      'portals' => 'Portales',
    ),
    'links' => 
    array (
      'cases' => 'Casos',
      'opportunities' => 'Oportunidades',
      'categories' => 'Categorías',
      'portals' => 'Portales',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'In Review' => 'En Revisión',
        'Draft' => 'Borrador',
        'Archived' => 'Arcivado',
        'Published' => 'Publicado',
      ),
      'type' => 
      array (
        'Article' => 'Artículo',
      ),
    ),
    'tooltips' => 
    array (
      'portals' => 'El Artículo estará disponible sólo en algunos portales.',
    ),
    'presetFilters' => 
    array (
      'published' => 'Publicado',
    ),
  ),
  'KnowledgeBaseCategory' => 
  array (
    'labels' => 
    array (
      'Create KnowledgeBaseCategory' => 'Crear Categoría',
      'Manage Categories' => 'Categorías',
      'Articles' => 'Artículos',
    ),
    'links' => 
    array (
      'articles' => 'Artículos',
    ),
  ),
  'Lead' => 
  array (
    'labels' => 
    array (
      'Converted To' => 'Convertido a',
      'Create Lead' => 'Crear Referencia',
      'Convert' => 'Convertir',
      'convert' => 'Convertir',
    ),
    'fields' => 
    array (
      'name' => 'Nombre',
      'emailAddress' => 'Correo electrónico',
      'title' => 'Título',
      'website' => 'Sito Web',
      'phoneNumber' => 'Teléfono',
      'accountName' => 'Nombre de Cuenta',
      'doNotCall' => 'No Llamar',
      'address' => 'Dirección',
      'status' => 'Estátus',
      'source' => 'Fuente',
      'opportunityAmount' => 'Costo de Oportunidad',
      'opportunityAmountConverted' => 'Costo de Oportunidad (convertido)',
      'description' => 'Descripción',
      'createdAccount' => 'Cuenta',
      'createdContact' => 'Contacto',
      'createdOpportunity' => 'Oportunidad',
      'convertedAt' => 'Converted At',
      'campaign' => 'Campaña',
      'targetLists' => 'Listas de Intereses',
      'targetList' => 'Lista de Intereses',
      'industry' => 'Industria',
      'acceptanceStatus' => 'Estatus de Aprobación',
      'opportunityAmountCurrency' => 'Importe de la Oportunidad',
      'acceptanceStatusMeetings' => 'Estatus de Aceptación (Presentaciones)',
      'acceptanceStatusCalls' => 'Estátus de Aceptación (Llamadas)',
    ),
    'links' => 
    array (
      'targetLists' => 'Listas de Intereses',
      'campaignLogRecords' => 'Registrar Campaña',
      'campaign' => 'Campaña',
      'createdAccount' => 'Cuenta',
      'createdContact' => 'Contacto',
      'createdOpportunity' => 'Oportunidad',
      'cases' => 'Casos',
      'documents' => 'Documentos',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'New' => 'Nuevo',
        'Assigned' => 'Asignado',
        'In Process' => 'En Proceso',
        'Converted' => 'Convertidos',
        'Recycled' => 'Reciclado',
        'Dead' => 'Muerto',
      ),
      'source' => 
      array (
        '' => 'Ninguno',
        'Call' => 'Llamada',
        'Email' => 'Correo electrónico',
        'Existing Customer' => 'Cliente Existente',
        'Partner' => 'Socio',
        'Public Relations' => 'Relaciones Públicas',
        'Web Site' => 'Sitio Web',
        'Campaign' => 'Campaña',
        'Other' => 'Otro',
      ),
    ),
    'presetFilters' => 
    array (
      'active' => 'Activo',
      'actual' => 'Actuales',
      'converted' => 'Convertidos',
    ),
  ),
  'MassEmail' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'status' => 'Estado',
      'storeSentEmails' => 'Almacenar Correos Enviados',
      'startAt' => 'Fecha de Comienzo',
      'fromAddress' => 'De (Dirección)',
      'fromName' => 'De (Nombre)',
      'replyToAddress' => 'Responder a la dirección',
      'replyToName' => 'Responder al Nombre',
      'campaign' => 'Campaña',
      'emailTemplate' => 'Plantilla de Correo',
      'inboundEmail' => 'Cuenta de correo',
      'targetLists' => 'Lista de Intereses',
      'excludingTargetLists' => 'Listas de Intereses Excluídos',
      'optOutEntirely' => 'Confirmación Completada',
      'smtpAccount' => 'Cuenta SMTP',
    ),
    'links' => 
    array (
      'targetLists' => 'Listas de Intereses',
      'excludingTargetLists' => 'Listas de Intereses Excluídos',
      'queueItems' => 'Items en cola',
      'campaign' => 'Campaña',
      'emailTemplate' => 'Plantilla de Correo',
      'inboundEmail' => 'Cuenta de correo',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Draft' => 'Borrador',
        'Pending' => 'Pendiente',
        'In Process' => 'En Proceso',
        'Complete' => 'Completado',
        'Canceled' => 'Cancelado',
        'Failed' => 'Falló',
      ),
    ),
    'labels' => 
    array (
      'Create MassEmail' => 'Crear correo masivo',
      'Send Test' => 'Enviar prueba',
      'System SMTP' => 'Sistema SMTP',
      'system' => 'sistema',
      'group' => 'grupo',
    ),
    'messages' => 
    array (
      'selectAtLeastOneTarget' => 'Seleccione al menos un interés',
      'testSent' => 'Correo(s) de prueba que se enviarán',
    ),
    'tooltips' => 
    array (
      'optOutEntirely' => 'Los correos de destinatarios que cancelaron su suscripción serán marcados como rechazados y ya no recibirán correos masivos.',
      'targetLists' => 'Los intereses que deben recibir los mensajes.',
      'excludingTargetLists' => 'Los intereses que no deben recibir mensajes.',
      'storeSentEmails' => 'Los correos se guardarán en el CRM.',
    ),
    'presetFilters' => 
    array (
      'actual' => 'Actual',
      'complete' => 'Completo',
    ),
  ),
  'Meeting' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'parent' => 'Padre',
      'status' => 'Estatus',
      'dateStart' => 'Fecha de Comienzo',
      'dateEnd' => 'Fecha de Finalización',
      'duration' => 'Duración',
      'description' => 'Descripción',
      'users' => 'Usuarios',
      'contacts' => 'Contactos',
      'leads' => 'Referencias',
      'reminders' => 'Recordatorios',
      'account' => 'Cuenta',
      'acceptanceStatus' => 'Estátus de aprobación',
      'dateStartDate' => 'Date Start (all day)',
      'dateEndDate' => 'Date End (all day)',
      'isAllDay' => 'Is All-Day',
      'Acceptance' => 'Aceptación',
    ),
    'links' => 
    array (
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Planned' => 'Planeadas',
        'Held' => 'Retenida',
        'Not Held' => 'Pendiente',
      ),
      'acceptanceStatus' => 
      array (
        'None' => 'Ninguno',
        'Accepted' => 'Aprobado',
        'Declined' => 'Rechazado',
        'Tentative' => 'Tentativa',
      ),
    ),
    'massActions' => 
    array (
      'setHeld' => 'Marcar como Retenida',
      'setNotHeld' => 'Marcar como Pendiente',
    ),
    'labels' => 
    array (
      'Create Meeting' => 'Crear Presentación',
      'Set Held' => 'Marcar como Retenida',
      'Set Not Held' => 'Marcar como Pendiente',
      'Send Invitations' => 'Enviar Invitaciones',
      'on time' => 'a tiempo',
      'before' => 'antes',
      'All-Day' => 'All-Day',
    ),
    'presetFilters' => 
    array (
      'planned' => 'Planeadas',
      'held' => 'Retenidas',
      'todays' => 'De Hoy',
    ),
    'messages' => 
    array (
      'selectAcceptanceStatus' => 'Set your acceptance status.',
      'nothingHasBeenSent' => 'No se ha enviado nada',
    ),
  ),
  'Opportunity' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'account' => 'Cuenta',
      'stage' => 'Etapa',
      'amount' => 'Cantidad',
      'probability' => 'Probabilidad, %',
      'leadSource' => 'Orígen de la Referencia',
      'doNotCall' => 'No Llamar',
      'closeDate' => 'Fecha de cierre',
      'contacts' => 'Contactos',
      'contact' => 'Contact (Primary)',
      'description' => 'Descripción',
      'amountConverted' => 'Cantidad (convertido)',
      'amountWeightedConverted' => 'Cantidad Ponderada',
      'campaign' => 'Campaña',
      'originalLead' => 'Referencia original',
      'amountCurrency' => 'Importe en Moneda',
      'contactRole' => 'Rol del Contacto',
      'lastStage' => 'Última Etapa',
      'itemList' => 'Item List',
      'fob' => 'FOB',
      'iva' => '% IVA',
      'deliveryTime' => 'Tiempo estimado de entrega',
      'termsDeliveryPercent' => '% Entrega',
      'termsAdvancePercent' => '% Anticipo',
      'termsInstalationPercent' => '% Instalación',
      'termsAdvanceDays' => 'Anticipo (Días)',
      'termsDeliveryDays' => 'Entrega (Días)',
      'termsInstalationDays' => 'Instalación (Días)',
      'deliveryDays' => 'Días de entrega',
      'partialDeliveries' => 'Acepta entregas parciales',
      'generalTerms' => 'Utilizar términos generales',
      'useGeneralIva' => 'Utilizar IVA general',
      'fup' => 'Numero de FUP',
      'assignedUser' => 'Usuario Asignado',
      'users2' => 'Users',
      'usuariosAsignados' => 'Usuarios asignados',
    ),
    'links' => 
    array (
      'contacts' => 'Contactos',
      'contact' => 'Contact (Primary)',
      'documents' => 'Documentos',
      'campaign' => 'Campaña',
      'originalLead' => 'Referencia original',
      'quotes' => 'Quotes',
      'users' => 'Usuarios asignados',
      'users2' => 'Users',
      'usuariosAsignados' => 'Usuarios asignados',
    ),
    'options' => 
    array (
      'stage' => 
      array (
        'Prospecting' => 'Prospección',
        'Qualification' => 'Calificación',
        'Proposal' => 'Cotización con Propuesta',
        'Negotiation' => 'Negociación',
        'Needs Analysis' => 'Análisis de Necesidades',
        'Value Proposition' => 'Propuesta de Valor',
        'Id. Decision Makers' => 'Id. Tomadores de Decisiones',
        'Perception Analysis' => 'Análisis de la Percepción',
        'Proposal/Price Quote' => 'Cotización con Propuesta/Precio',
        'Negotiation/Review' => 'Negociación/Revisión',
        'Closed Won' => 'Cerrado Ganado',
        'Closed Lost' => 'Cerrado Perdido',
        'Customer Interest / Modification' => 'Customer Interest / Modification',
      ),
      'deliveryDays' => 
      array (
        'Lunes' => 'Lunes',
        'Martes' => 'Martes',
        'Miercoles' => 'Miercoles',
        'Jueves' => 'Jueves',
        'Viernes' => 'Viernes',
      ),
      'partialDeliveries' => 
      array (
        'Si' => 'Si',
      ),
      'generalTerms' => 
      array (
        'Si' => 'Si',
      ),
      'useGeneralIva' => 
      array (
        'Si' => 'Si',
      ),
    ),
    'labels' => 
    array (
      'Create Opportunity' => 'Crear Oportunidad',
      'Items' => 'Items',
      'Select Product' => 'Select Product',
      'Add Item' => 'Add Item',
    ),
    'presetFilters' => 
    array (
      'open' => 'Abiertos',
      'won' => 'Ganados',
      'lost' => 'Perdido',
    ),
  ),
  'TargetList' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'description' => 'Descripción',
      'entryCount' => 'Contador de entradas',
      'optedOutCount' => 'Contador de Exclusiones',
      'campaigns' => 'Campañas',
      'endDate' => 'Fecha de Fin',
      'targetLists' => 'Listas de Intereses',
      'includingActionList' => 'Incluyendo',
      'excludingActionList' => 'Excluyendo',
      'targetStatus' => 'Status del Interés',
      'isOptedOut' => 'Se ha Excluido',
      'syncWithReports' => 'Reports',
      'syncWithReportsEnabled' => 'Enabled',
      'syncWithReportsUnlink' => 'Unlink',
    ),
    'links' => 
    array (
      'accounts' => 'Cuentas',
      'contacts' => 'Contactos',
      'leads' => 'Referencias',
      'campaigns' => 'Campañas',
      'massEmails' => 'Correos Masivos',
      'syncWithReports' => 'Sync with Reports',
    ),
    'options' => 
    array (
      'type' => 
      array (
        'Email' => 'Correo electrónico',
        'Web' => 'Web',
        'Television' => 'Televisión',
        'Radio' => 'Radio',
        'Newsletter' => 'Newsletter',
      ),
      'targetStatus' => 
      array (
        'Opted Out' => 'Excluido',
        'Listed' => 'Listado',
      ),
    ),
    'labels' => 
    array (
      'Create TargetList' => 'Crear lista de Intereses',
      'Opted Out' => 'Rechazado',
      'Cancel Opt-Out' => 'Cancelar Confirmación',
      'Opt-Out' => 'Confirmar',
      'MailChimp List Settings' => 'MailChimp List Sync',
      'MailChimp Sync' => 'MailChimp Sync',
      'Sync with Reports' => 'Sync with Reports',
    ),
    'tooltips' => 
    array (
      'syncWithReportsUnlink' => 'Entries which are not contained in report results will be unlinked from Target List.',
      'syncWithReports' => 'Target List will be synced with results of selected reports.',
    ),
  ),
  'Task' => 
  array (
    'fields' => 
    array (
      'name' => 'Nombre',
      'parent' => 'Padre',
      'status' => 'Estátus',
      'dateStart' => 'Fecha de Comienzo',
      'dateEnd' => 'Fecha de vencimiento',
      'dateStartDate' => 'Fecha de Inicio (todo el día)',
      'dateEndDate' => 'Fecha de fin (todo el día)',
      'priority' => 'Prioridad',
      'description' => 'Descripción',
      'isOverdue' => 'Atrasado',
      'account' => 'Cuenta',
      'dateCompleted' => 'Fecha de completado',
      'attachments' => 'Adjuntos',
      'reminders' => 'Recordatorios',
      'contact' => 'Contacto',
    ),
    'links' => 
    array (
      'attachments' => 'Adjuntos',
      'account' => 'Cuenta',
      'contact' => 'Contacto',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Not Started' => 'Sin Empezar',
        'Started' => 'Comenzada',
        'Completed' => 'Completada',
        'Canceled' => 'Cancelada',
        'Deferred' => 'Diferida',
      ),
      'priority' => 
      array (
        'Low' => 'Baja',
        'Normal' => 'Normal',
        'High' => 'Alta',
        'Urgent' => 'Urgente',
      ),
    ),
    'labels' => 
    array (
      'Create Task' => 'Crear Tarea',
      'Complete' => 'Completada',
    ),
    'presetFilters' => 
    array (
      'actual' => 'Actuales',
      'completed' => 'Completado',
      'deferred' => 'Diferida',
      'todays' => 'De Hoy',
      'overdue' => 'Atrazadas',
    ),
  ),
  'GoogleCalendar' => 
  array (
    'messages' => 
    array (
      'fieldLabelIsRequired' => 'Only one entity could haven\'t Identification Label',
      'emptyNotDefaultEnitityLabel' => 'Identification Label of not by default Entity can\'t be empty',
      'defaultEntityIsRequiredInList' => 'Default Entity is required in the Sync Entities List',
    ),
  ),
  'MailChimp' => 
  array (
    'labels' => 
    array (
      'synced with MailChimp' => 'synchronized with MailChimp',
      'failed synced with MailChimp' => 'did not synchronize with MailChimp. Try to schedule a synchronization one more time.',
      'Espo Campaign' => 'Espo Campaign',
      'Espo TargetList' => 'Espo Target List',
      'MailChimp Campaign' => 'MailChimp Campaign',
      'MailChimp TargetList' => 'MailChimp List',
      'MailChimp TargetListGroup' => 'MailChimp Group',
      'MailChimp Sync' => 'MailChimp Sync',
      'Proceed Setup on MailChimp' => 'Proceed Setup on MailChimp',
      'Save and Sync Now' => 'Save & Sync Now',
      'Sync Now' => 'Sync Now',
      'Scheduling Synchronization' => 'Scheduling Synchronization...',
      'Synchronization is scheduled' => 'Synchronization is scheduled',
      'Selected grouping' => 'You selected a grouping. Select a group?',
    ),
    'tooltips' => 
    array (
      'mailChimpGroup' => 'Select a list group (second level in the tree), if you want to add all recipients of Target List to the specified Interest Group',
    ),
  ),
  'MailChimpCampaign' => 
  array (
    'fields' => 
    array (
      'name' => 'Name',
      'type' => 'Type',
      'list' => 'List',
      'subject' => 'Email Subject',
      'fromEmail' => 'From Email Address',
      'fromName' => 'From Name',
      'toName' => 'Personalize the "To:" field',
      'status' => 'Status',
      'dateSent' => 'Date Sent',
      'content' => 'Content?',
    ),
    'options' => 
    array (
      'type' => 
      array (
        'regular' => 'Regular',
        'plaintext' => 'Plain-Text',
        'rss' => 'RSS-Driven',
        'absplit' => 'A/B Split',
        'auto' => 'Auto',
      ),
      'status' => 
      array (
        'sent' => 'Sent',
        'save' => 'Save',
        'paused' => 'Paused',
        'schedule' => 'Schedule',
        'sending' => 'Sending',
      ),
    ),
  ),
  'MailChimpList' => 
  array (
    'fields' => 
    array (
      'name' => 'Name',
      'company' => 'Company / Organization',
      'address1' => 'Address',
      'address2' => 'Other Address',
      'city' => 'City',
      'state' => 'State / Province / Region',
      'zip' => 'Zip / Postal Code',
      'country' => 'Country',
      'phone' => 'Phone',
      'reminder' => 'Remind people how they got on your list',
      'subject' => 'Email Subject',
      'fromEmail' => 'From Email Address',
      'fromName' => 'From Name',
      'subscribers' => 'Subcribers',
      'language' => 'Language',
    ),
    'options' => 
    array (
      'country' => 
      array (
        'AD' => 'Andorra',
        'AE' => 'United Arab Emirates',
        'AF' => 'Afghanistan',
        'AG' => 'Antigua and Barbuda',
        'AI' => 'Anguilla',
        'AL' => 'Albania',
        'AM' => 'Armenia',
        'AO' => 'Angola',
        'AQ' => 'Antarctica',
        'AR' => 'Argentina',
        'AS' => 'American Samoa',
        'AT' => 'Austria',
        'AU' => 'Australia',
        'AW' => 'Aruba',
        'AX' => 'Åland Islands',
        'AZ' => 'Azerbaijan',
        'BA' => 'Bosnia and Herzegovina',
        'BB' => 'Barbados',
        'BD' => 'Bangladesh',
        'BE' => 'Belgium',
        'BF' => 'Burkina Faso',
        'BG' => 'Bulgaria',
        'BH' => 'Bahrain',
        'BI' => 'Burundi',
        'BJ' => 'Benin',
        'BL' => 'Saint Barthélemy',
        'BM' => 'Bermuda',
        'BN' => 'Brunei Darussalam',
        'BO' => 'Bolivia, Plurinational State of',
        'BQ' => 'Bonaire, Sint Eustatius and Saba',
        'BR' => 'Brazil',
        'BS' => 'Bahamas',
        'BT' => 'Bhutan',
        'BV' => 'Bouvet Island',
        'BW' => 'Botswana',
        'BY' => 'Belarus',
        'BZ' => 'Belize',
        'CA' => 'Canada',
        'CC' => 'Cocos (Keeling) Islands',
        'CD' => 'Congo, the Democratic Republic of the',
        'CF' => 'Central African Republic',
        'CG' => 'Congo',
        'CH' => 'Switzerland',
        'CI' => 'Côte d\'Ivoire',
        'CK' => 'Cook Islands',
        'CL' => 'Chile',
        'CM' => 'Cameroon',
        'CN' => 'China',
        'CO' => 'Colombia',
        'CR' => 'Costa Rica',
        'CU' => 'Cuba',
        'CV' => 'Cabo Verde',
        'CW' => 'Curaçao',
        'CX' => 'Christmas Island',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DE' => 'Germany',
        'DJ' => 'Djibouti',
        'DK' => 'Denmark',
        'DM' => 'Dominica',
        'DO' => 'Dominican Republic',
        'DZ' => 'Algeria',
        'EC' => 'Ecuador',
        'EE' => 'Estonia',
        'EG' => 'Egypt',
        'EH' => 'Western Sahara',
        'ER' => 'Eritrea',
        'ES' => 'Spain',
        'ET' => 'Ethiopia',
        'FI' => 'Finland',
        'FJ' => 'Fiji',
        'FK' => 'Falkland Islands (Malvinas)',
        'FM' => 'Micronesia, Federated States of',
        'FO' => 'Faroe Islands',
        'FR' => 'France',
        'GA' => 'Gabon',
        'GB' => 'United Kingdom of Great Britain and Northern Ireland',
        'GD' => 'Grenada',
        'GE' => 'Georgia',
        'GF' => 'French Guiana',
        'GG' => 'Guernsey',
        'GH' => 'Ghana',
        'GI' => 'Gibraltar',
        'GL' => 'Greenland',
        'GM' => 'Gambia',
        'GN' => 'Guinea',
        'GP' => 'Guadeloupe',
        'GQ' => 'Equatorial Guinea',
        'GR' => 'Greece',
        'GS' => 'South Georgia and the South Sandwich Islands',
        'GT' => 'Guatemala',
        'GU' => 'Guam',
        'GW' => 'Guinea-Bissau',
        'GY' => 'Guyana',
        'HK' => 'Hong Kong',
        'HM' => 'Heard Island and McDonald Islands',
        'HN' => 'Honduras',
        'HR' => 'Croatia',
        'HT' => 'Haiti',
        'HU' => 'Hungary',
        'ID' => 'Indonesia',
        'IE' => 'Ireland',
        'IL' => 'Israel',
        'IM' => 'Isle of Man',
        'IN' => 'India',
        'IO' => 'British Indian Ocean Territory',
        'IQ' => 'Iraq',
        'IR' => 'Iran, Islamic Republic of',
        'IS' => 'Iceland',
        'IT' => 'Italy',
        'JE' => 'Jersey',
        'JM' => 'Jamaica',
        'JO' => 'Jordan',
        'JP' => 'Japan',
        'KE' => 'Kenya',
        'KG' => 'Kyrgyzstan',
        'KH' => 'Cambodia',
        'KI' => 'Kiribati',
        'KM' => 'Comoros',
        'KN' => 'Saint Kitts and Nevis',
        'KP' => 'Korea, Democratic People\'s Republic of',
        'KR' => 'Korea, Republic of',
        'KW' => 'Kuwait',
        'KY' => 'Cayman Islands',
        'KZ' => 'Kazakhstan',
        'LA' => 'Lao People\'s Democratic Republic',
        'LB' => 'Lebanon',
        'LC' => 'Saint Lucia',
        'LI' => 'Liechtenstein',
        'LK' => 'Sri Lanka',
        'LR' => 'Liberia',
        'LS' => 'Lesotho',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
        'LV' => 'Latvia',
        'LY' => 'Libya',
        'MA' => 'Morocco',
        'MC' => 'Monaco',
        'MD' => 'Moldova, Republic of',
        'ME' => 'Montenegro',
        'MF' => 'Saint Martin (French part)',
        'MG' => 'Madagascar',
        'MH' => 'Marshall Islands',
        'MK' => 'Macedonia, the former Yugoslav Republic of',
        'ML' => 'Mali',
        'MM' => 'Myanmar',
        'MN' => 'Mongolia',
        'MO' => 'Macao',
        'MP' => 'Northern Mariana Islands',
        'MQ' => 'Martinique',
        'MR' => 'Mauritania',
        'MS' => 'Montserrat',
        'MT' => 'Malta',
        'MU' => 'Mauritius',
        'MV' => 'Maldives',
        'MW' => 'Malawi',
        'MX' => 'Mexico',
        'MY' => 'Malaysia',
        'MZ' => 'Mozambique',
        'NA' => 'Namibia',
        'NC' => 'New Caledonia',
        'NE' => 'Niger',
        'NF' => 'Norfolk Island',
        'NG' => 'Nigeria',
        'NI' => 'Nicaragua',
        'NL' => 'Netherlands',
        'NO' => 'Norway',
        'NP' => 'Nepal',
        'NR' => 'Nauru',
        'NU' => 'Niue',
        'NZ' => 'New Zealand',
        'OM' => 'Oman',
        'PA' => 'Panama',
        'PE' => 'Peru',
        'PF' => 'French Polynesia',
        'PG' => 'Papua New Guinea',
        'PH' => 'Philippines',
        'PK' => 'Pakistan',
        'PL' => 'Poland',
        'PM' => 'Saint Pierre and Miquelon',
        'PN' => 'Pitcairn',
        'PR' => 'Puerto Rico',
        'PS' => 'Palestine, State of',
        'PT' => 'Portugal',
        'PW' => 'Palau',
        'PY' => 'Paraguay',
        'QA' => 'Qatar',
        'RE' => 'Réunion',
        'RO' => 'Romania',
        'RS' => 'Serbia',
        'RU' => 'Russian Federation',
        'RW' => 'Rwanda',
        'SA' => 'Saudi Arabia',
        'SB' => 'Solomon Islands',
        'SC' => 'Seychelles',
        'SD' => 'Sudan',
        'SE' => 'Sweden',
        'SG' => 'Singapore',
        'SH' => 'Saint Helena, Ascension and Tristan da Cunha',
        'SI' => 'Slovenia',
        'SJ' => 'Svalbard and Jan Mayen',
        'SK' => 'Slovakia',
        'SL' => 'Sierra Leone',
        'SM' => 'San Marino',
        'SN' => 'Senegal',
        'SO' => 'Somalia',
        'SR' => 'Suriname',
        'SS' => 'South Sudan',
        'ST' => 'Sao Tome and Principe',
        'SV' => 'El Salvador',
        'SX' => 'Sint Maarten (Dutch part)',
        'SY' => 'Syrian Arab Republic',
        'SZ' => 'Swaziland',
        'TC' => 'Turks and Caicos Islands',
        'TD' => 'Chad',
        'TF' => 'French Southern Territories',
        'TG' => 'Togo',
        'TH' => 'Thailand',
        'TJ' => 'Tajikistan',
        'TK' => 'Tokelau',
        'TL' => 'Timor-Leste',
        'TM' => 'Turkmenistan',
        'TN' => 'Tunisia',
        'TO' => 'Tonga',
        'TR' => 'Turkey',
        'TT' => 'Trinidad and Tobago',
        'TV' => 'Tuvalu',
        'TW' => 'Taiwan, Province of China',
        'TZ' => 'Tanzania, United Republic of',
        'UA' => 'Ukraine',
        'UG' => 'Uganda',
        'UM' => 'United States Minor Outlying Islands',
        'US' => 'United States of America',
        'UY' => 'Uruguay',
        'UZ' => 'Uzbekistan',
        'VA' => 'Holy See',
        'VC' => 'Saint Vincent and the Grenadines',
        'VE' => 'Venezuela, Bolivarian Republic of',
        'VG' => 'Virgin Islands, British',
        'VI' => 'Virgin Islands, U.S.',
        'VN' => 'Viet Nam',
        'VU' => 'Vanuatu',
        'WF' => 'Wallis and Futuna',
        'WS' => 'Samoa',
        'YE' => 'Yemen',
        'YT' => 'Mayotte',
        'ZA' => 'South Africa',
        'ZM' => 'Zambia',
        'ZW' => 'Zimbabwe',
      ),
    ),
  ),
  'OpportunityItem' => 
  array (
    'fields' => 
    array (
      'name' => 'Name',
      'qty' => 'Qty',
      'quantity' => 'Quantity',
      'unitPrice' => 'Unit Price',
      'amount' => 'Amount',
      'product' => 'Product',
      'order' => 'Line Number',
      'opportunity' => 'Opportunity',
      'description' => 'Description',
    ),
    'links' => 
    array (
      'opportunity' => 'Opportunity',
    ),
  ),
  'Product' => 
  array (
    'labels' => 
    array (
      'Create Product' => 'Create Product',
      'Price' => 'Price',
      'Brands' => 'Brands',
      'Categories' => 'Categories',
    ),
    'fields' => 
    array (
      'status' => 'Status',
      'brand' => 'Brand',
      'partNumber' => 'Part Number',
      'category' => 'Category',
      'pricingType' => 'Pricing Type',
      'pricingFactor' => 'Pricing Factor',
      'costPrice' => 'Cost Price',
      'listPrice' => 'List Price',
      'unitPrice' => 'Unit Price',
      'costPriceConverted' => 'Cost Price (Converted)',
      'listPriceConverted' => 'List Price (Converted)',
      'unitPriceConverted' => 'Unit Price (Converted)',
      'url' => 'URL',
      'weight' => 'Weight',
    ),
    'links' => 
    array (
      'brand' => 'Brand',
      'category' => 'Category',
    ),
    'options' => 
    array (
      'pricingType' => 
      array (
        'Same as List' => 'Same as List',
        'Fixed' => 'Fixed',
        'Discount from List' => 'Discount from List',
        'Markup over Cost' => 'Markup over Cost',
        'Profit Margin' => 'Profit Margin',
      ),
    ),
    'presetFilters' => 
    array (
      'available' => 'Available',
    ),
  ),
  'ProductBrand' => 
  array (
    'labels' => 
    array (
      'Create ProductBrand' => 'Create Brand',
    ),
    'fields' => 
    array (
      'website' => 'Website',
    ),
    'links' => 
    array (
      'products' => 'Products',
    ),
  ),
  'ProductCategory' => 
  array (
    'labels' => 
    array (
      'Create ProductCategory' => 'Create Category',
      'Manage Categories' => 'Manage Categories',
    ),
    'fields' => 
    array (
      'order' => 'Order',
    ),
    'links' => 
    array (
      'products' => 'Products',
    ),
  ),
  'Quote' => 
  array (
    'labels' => 
    array (
      'Create Quote' => 'Create Quote',
      'Taxes' => 'Taxes',
      'Shipping Providers' => 'Shipping Providers',
      'Add Item' => 'Add Item',
      'Templates' => 'Templates',
    ),
    'fields' => 
    array (
      'status' => 'Status',
      'number' => 'Quote Number',
      'invoiceNumber' => 'Invoice Number',
      'account' => 'Account',
      'opportunity' => 'Opportunity',
      'billingAddress' => 'Billing Address',
      'shippingAddress' => 'Shipping Address',
      'billingContact' => 'Billing Contact',
      'shippingContact' => 'Shipping Contact',
      'tax' => 'Tax',
      'taxRate' => 'Tax Rate',
      'shippingCost' => 'Shipping Cost',
      'shippingProvider' => 'Shipping Provider',
      'taxAmount' => 'Tax Amount',
      'discountAmount' => 'Discount Amount',
      'amount' => 'Amount',
      'preDiscountedAmount' => 'Pre-Discounted Amount',
      'grandTotalAmount' => 'Grand Total Amount',
      'itemList' => 'Item List',
      'dateQuoted' => 'Date Quoted',
      'dateInvoiced' => 'Date Invoiced',
      'weight' => 'Weight',
    ),
    'links' => 
    array (
      'items' => 'Items',
      'billingContact' => 'Billing Contact',
      'shippingContact' => 'Shipping Contact',
      'opportunity' => 'Opportunity',
      'account' => 'Account',
      'tax' => 'Tax',
    ),
    'options' => 
    array (
      'status' => 
      array (
        'Draft' => 'Draft',
        'In Review' => 'In Review',
        'Presented' => 'Presented',
        'Approved' => 'Approved',
        'Rejected' => 'Rejected',
        'Canceled' => 'Canceled',
      ),
    ),
    'presetFilters' => 
    array (
      'actual' => 'Actual',
      'approved' => 'Approved',
    ),
  ),
  'QuoteItem' => 
  array (
    'fields' => 
    array (
      'name' => 'Name',
      'qty' => 'Qty',
      'quantity' => 'Quantity',
      'listPrice' => 'List Price',
      'unitPrice' => 'Unit Price',
      'amount' => 'Amount',
      'taxRate' => 'Tax Rate',
      'product' => 'Product',
      'order' => 'Line Number',
      'quote' => 'Quote',
      'weight' => 'Weight',
      'unitWeight' => 'Unit Weight',
      'description' => 'Description',
    ),
    'links' => 
    array (
      'quote' => 'Quote',
    ),
  ),
  'Report' => 
  array (
    'labels' => 
    array (
      'Create Report' => 'Create Report',
      'Run' => 'Run',
      'Total' => 'Total',
      '-Empty-' => '-Empty-',
      'Parameters' => 'Parameters',
      'Filters' => 'Filters',
      'Chart' => 'Chart',
      'List Report' => 'List Report',
      'Grid Report' => 'Grid Report',
      'days' => 'days',
      'never' => 'never',
      'Get Csv' => 'Get Csv',
      'EmailSending' => 'Email Sending',
      'View Report' => 'View Report',
    ),
    'fields' => 
    array (
      'type' => 'Type',
      'entityType' => 'Entity Type',
      'description' => 'Description',
      'groupBy' => 'Group by',
      'columns' => 'Columns',
      'orderBy' => 'Order by',
      'filters' => 'Filters',
      'runtimeFilters' => 'Runtime Filters',
      'chartType' => 'Chart Type',
      'emailSendingInterval' => 'Interval',
      'emailSendingTime' => 'Time',
      'emailSendingUsers' => 'Users',
      'emailSendingSettingDay' => 'Day',
      'emailSendingSettingMonth' => 'Month',
      'emailSendingSettingWeekdays' => 'Days',
      'emailSendingDoNotSendEmptyReport' => 'Don\'t send if report is empty',
      'chartColorList' => 'Chart Colors',
      'chartColor' => 'Chart Color',
      'orderByList' => 'List Order',
    ),
    'tooltips' => 
    array (
      'emailSendingUsers' => 'Users report result will be sent to',
      'chartColorList' => 'Custom colors for specific groups.',
    ),
    'functions' => 
    array (
      'COUNT' => 'Count',
      'SUM' => 'Sum',
      'AVG' => 'Avg',
      'MIN' => 'Min',
      'MAX' => 'Max',
      'YEAR' => 'Year',
      'MONTH' => 'Month',
      'DAY' => 'Day',
    ),
    'orders' => 
    array (
      'ASC' => 'ASC',
      'DESC' => 'DESC',
      'LIST' => 'LIST',
    ),
    'options' => 
    array (
      'chartType' => 
      array (
        'BarHorizontal' => 'Bar (horizontal)',
        'BarVertical' => 'Bar (vertical)',
        'Pie' => 'Pie',
        'Line' => 'Line',
      ),
      'emailSendingInterval' => 
      array (
        '' => 'None',
        'Daily' => 'Daily',
        'Weekly' => 'Weekly',
        'Monthly' => 'Monthly',
        'Yearly' => 'Yearly',
      ),
      'emailSendingSettingDay' => 
      array (
        32 => 'Last day of month',
      ),
      'type' => 
      array (
        'Grid' => 'Grid',
        'List' => 'List',
      ),
    ),
    'messages' => 
    array (
      'validateMaxCount' => 'Count should not be greater than {maxCount}',
      'gridReportDescription' => 'Group by one or two columns and see summations. Can be displayed as a chart.',
      'listReportDescription' => 'Simple list of records which meet filters criteria.',
    ),
    'presetFilters' => 
    array (
      'list' => 'List',
      'grid' => 'Grid',
      'listTargets' => 'List (Targets)',
      'listAccounts' => 'List (Accounts)',
      'listContacts' => 'List (Contacts)',
      'listLeads' => 'List (Leads)',
      'listUsers' => 'List (Users)',
    ),
    'errorMessages' => 
    array (
      'error' => 'Error',
      'noChart' => 'No chart selected for the report',
      'selectReport' => 'Select Report in dashlet options',
    ),
  ),
  'ShippingProvider' => 
  array (
    'labels' => 
    array (
      'Create ShippingProvider' => 'Create Shipping Provider',
    ),
    'fields' => 
    array (
      'website' => 'Website',
    ),
  ),
  'Tax' => 
  array (
    'labels' => 
    array (
      'Create Tax' => 'Create Tax',
    ),
    'fields' => 
    array (
      'rate' => 'Rate',
    ),
  ),
  'Workflow' => 
  array (
    'fields' => 
    array (
      'Name' => 'Name',
      'entityType' => 'Target Entity',
      'type' => 'Trigger Type',
      'isActive' => 'Active',
      'description' => 'Description',
      'usersToMakeToFollow' => 'Users to make to follow the record',
      'whatToFollow' => 'What to Follow',
      'portalOnly' => 'Portal Only',
      'portal' => 'Portal',
      'targetReport' => 'Target Report',
      'scheduling' => 'Scheduling',
    ),
    'links' => 
    array (
      'portal' => 'Portal',
      'targetReport' => 'Target Report',
      'workflowLogRecords' => 'Log',
    ),
    'tooltips' => 
    array (
      'portalOnly' => 'If checked workflow will be triggered only in portal.',
      'portal' => 'Specific portal where workflow will be triggered. Leave empty if you need it to work in any portal.',
      'scheduling' => 'Crontab notation. Defines frequency of job runs.

*/5 * * * * - every 5 minutes

0 */2 * * * - every 2 hours

30 1 * * * - at 01:30 once a day

0 0 1 * * - on the first day of the month',
    ),
    'labels' => 
    array (
      'Create Workflow' => 'Create Rule',
      'General' => 'General',
      'Conditions' => 'Conditions',
      'Actions' => 'Actions',
      'All' => 'All',
      'Any' => 'Any',
      'Email Address' => 'Email Address',
      'Email Template' => 'Email Template',
      'From' => 'From',
      'To' => 'To',
      'immediately' => 'Immediately',
      'later' => 'Later',
      'today' => 'now',
      'plus' => 'plus',
      'minus' => 'minus',
      'days' => 'days',
      'hours' => 'hours',
      'months' => 'months',
      'minutes' => 'minutes',
      'Link' => 'Link',
      'Entity' => 'Entity',
      'Add Field' => 'Add Field',
      'equals' => 'equals',
      'wasEqual' => 'was equal',
      'notEquals' => 'not equals',
      'wasNotEqual' => 'was not equal',
      'changed' => 'changed',
      'notEmpty' => 'not empty',
      'isEmpty' => 'empty',
      'value' => 'value',
      'field' => 'field',
      'true' => 'true',
      'false' => 'false',
      'greaterThan' => 'greater than',
      'lessThan' => 'less than',
      'greaterThanOrEquals' => 'greater than or equals',
      'lessThanOrEquals' => 'less than or equals',
      'between' => 'between',
      'on' => 'on',
      'before' => 'before',
      'after' => 'after',
      'beforeToday' => 'before today',
      'afterToday' => 'after today',
      'recipient' => 'Recipient',
      'has' => 'has',
      'messageTemplate' => 'Message Template',
      'users' => 'Users',
      'Target Entity' => 'Target Entity',
      'Workflow' => 'Workflow',
      'Workflows Log' => 'Workflows Log',
    ),
    'emailAddressOptions' => 
    array (
      'currentUser' => 'Current User',
      'specifiedEmailAddress' => 'Specified Email Address',
      'assignedUser' => 'Assigned User',
      'targetEntity' => 'Target Entity',
      'specifiedUsers' => 'Specified Users',
      'teamUsers' => 'Team Users',
      'followers' => 'Followers',
      'followersExcludingAssignedUser' => 'Followers excluding Assigned User',
      'specifiedTeams' => 'Users of specified Teams',
    ),
    'options' => 
    array (
      'type' => 
      array (
        'afterRecordSaved' => 'After record saved',
        'afterRecordCreated' => 'After record created',
        'scheduled' => 'Scheduled',
        'sequential' => 'Sequential',
      ),
      'subjectType' => 
      array (
        'value' => 'value',
        'field' => 'field',
        'today' => 'today/now',
      ),
    ),
    'actionTypes' => 
    array (
      'sendEmail' => 'Send Email',
      'createEntity' => 'Create Entity',
      'createRelatedEntity' => 'Create Related Entity',
      'updateEntity' => 'Update Entity',
      'updateRelatedEntity' => 'Update Related Entity',
      'makeFollowed' => 'Make Followed',
      'createNotification' => 'Create Notification',
      'triggerWorkflow' => 'Trigger Another Workflow',
    ),
    'texts' => 
    array (
      'allMustBeMet' => 'All must be met',
      'atLeastOneMustBeMet' => 'At least one must be met',
    ),
    'messages' => 
    array (
      'loopNotice' => 'Be careful about a possible looping through two or more workflow rules continuously.',
      'messageTemplateHelpText' => 'Available variables:
{entity} - target record,
{user} - current user.',
    ),
  ),
  'WorkflowLogRecord' => 
  array (
    'labels' => 
    array (
    ),
    'fields' => 
    array (
      'target' => 'Target',
      'workflow' => 'Workflow',
    ),
  ),
  'Soluciones' => 
  array (
    'fields' => 
    array (
      'account' => 'Account',
      'accounts' => 'Accounts',
    ),
    'links' => 
    array (
      'account' => 'Account',
      'accounts' => 'Accounts',
    ),
    'labels' => 
    array (
      'Create Soluciones' => 'Crear Solucion',
    ),
  ),
);
?>