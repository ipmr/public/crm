<?php
/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

namespace Espo\Modules\Advanced\Core\Workflow\Actions;

use Espo\ORM\Entity;

class UpdateRelatedEntity extends BaseEntity
{
    protected function run(Entity $entity, array $actionData)
    {
        $link = $actionData['link'];

        $relatedEntity = $this->getRelatedEntity($entity, $link);

        if (!($relatedEntity instanceof \Espo\ORM\Entity)) {
            return;
        }

        $update = true;

        $relationDefs = $entity->getRelations();

        if (isset($relationDefs[$link]) && $relationDefs[$link]['type'] == 'belongsToParent' && !empty($actionData['parentEntity'])) {

            if ($actionData['parentEntity'] != $relatedEntity->getEntityType()) {
                $update = false;
            }
        }

        if ($update) {
            $this->fillData($relatedEntity, $actionData['fields']);
            return $this->getEntityManager()->saveEntity($relatedEntity);
        }

        return true;
    }

    /**
     * Get Related Entity
     *
     * @param  \Espo\ORM\Entity $entity
     * @param  string $link
     *
     * @return \Espo\ORM\Entity | null
     */
    protected function getRelatedEntity(Entity $entity, $link)
    {
        if (empty($link) || !$entity->hasRelation($link)) {
            return;
        }

        $relationDefs = $entity->getRelations();
        $linkDefs = $relationDefs[$link];

        $relatedEntity = null;

        switch ($linkDefs['type']) {
            case 'belongsToParent':
                $parentType = $entity->get($link . 'Type');
                $parentId = $entity->get($link . 'Id');
                if (!empty($parentType) && !empty($parentId)) {
                    try {
                        $relatedEntity = $this->getEntityManager()->getEntity($parentType, $parentId);
                    } catch (\Exception $e) {
                        $GLOBALS['log']->info('Workflow[UpdateRelatedEntity]: Cannot getRelatedEntity(), error: '. $e->getMessage());
                    }
                }
                break;

            default:
                try {
                    $relatedEntity = $entity->get($link);
                } catch (\Exception $e) {
                    $GLOBALS['log']->info('Workflow[UpdateRelatedEntity]: Cannot getRelatedEntity(), error: '. $e->getMessage());
                }
                break;
        }

        return $relatedEntity;
    }
}