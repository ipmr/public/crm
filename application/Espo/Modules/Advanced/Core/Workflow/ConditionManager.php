<?php
/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

namespace Espo\Modules\Advanced\Core\Workflow;

use Espo\Core\Exceptions\Error;

class ConditionManager extends BaseManager
{
    protected $dirName = 'Conditions';

    protected $requiredOptions = array(
        'comparison',
        'fieldToCompare',
    );

    /**
     * Check conditions "Any"
     *
     * @param  array  $conditions
     * @return bool
     */
    public function compareConditionsAny(array $conditions)
    {
        if (!isset($conditions) || empty($conditions)) {
            return true;
        }

        foreach ($conditions as $condition) {
            if ($this->compare($condition)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check conditions "Any"
     *
     * @param  array  $conditions
     * @return bool
     */
    public function compareConditionsAll(array $conditions)
    {
        if (!isset($conditions)) {
            return true;
        }

        foreach ($conditions as $condition) {
            if (!$this->compare($condition)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Compare a single condition
     *
     * @param  Entity $entity
     * @param  array $conditions
     * @return bool
     */
    protected function compare(array $condition)
    {
        $entity = $this->getEntity();
        $entityName = $entity->getEntityName();

        if (!$this->validate($condition)) {
            $GLOBALS['log']->warning('Workflow['.$this->getWorkflowId().']: Condition data is broken for the Entity ['.$entityName.'].');
            return false;
        }

        $compareClass = $this->getClass($condition['comparison']);
        if (isset($compareClass)) {
            return $compareClass->process($entity, $condition);
        }

        return false;
    }
}