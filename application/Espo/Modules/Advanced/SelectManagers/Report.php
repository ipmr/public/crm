<?php
/************************************************************************
 * This file is part of EspoCRM.
 *
 * EspoCRM - Open Source CRM application.
 * Copyright (C) 2014-2015 Yuri Kuznetsov, Taras Machyshyn, Oleksiy Avramenko
 * Website: http://www.espocrm.com
 *
 * EspoCRM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EspoCRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EspoCRM. If not, see http://www.gnu.org/licenses/.
 ************************************************************************/

namespace Espo\Modules\Advanced\SelectManagers;

class Report extends \Espo\Core\SelectManagers\Base
{
    protected function filterListTargets(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'List',
            'entityType' => ['Contact', 'Lead', 'User', 'Account']
        );
    }

    protected function filterListAccounts(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'List',
            'entityType' => 'Account'
        );
    }

    protected function filterListContacts(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'List',
            'entityType' => 'Contact'
        );
    }

    protected function filterListLeads(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'List',
            'entityType' => 'Lead'
        );
    }

    protected function filterListUsers(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'List',
            'entityType' => 'User'
        );
    }

    protected function filterList(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'List'
        );
    }

    protected function filterGrid(&$result)
    {
        $result['whereClause'][] = array(
            'type=' => 'Grid'
        );
    }

 }

