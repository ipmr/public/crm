<?php
/************************************************************************
 * This file is part of EspoCRM.
 *
 * EspoCRM - Open Source CRM application.
 * Copyright (C) 2014  Yuri Kuznetsov, Taras Machyshyn, Oleksiy Avramenko
 * Website: http://www.espocrm.com
 *
 * EspoCRM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EspoCRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EspoCRM. If not, see http://www.gnu.org/licenses/.
 ************************************************************************/

namespace Espo\Modules\Advanced\EntryPoints;

use \Espo\Core\Utils\Util;

use \Espo\Core\Exceptions\NotFound;
use \Espo\Core\Exceptions\Forbidden;
use \Espo\Core\Exceptions\BadRequest;
use \Espo\Core\Exceptions\Error;

class ReportAsCsv extends \Espo\Core\EntryPoints\Base
{
    public static $authRequired = true;

    public function run()
    {
        if (empty($_GET['id'])) {
            throw new BadRequest();
        }

        $id = $_GET['id'];


        $where = null;
        if (!empty($_GET['where'])) {
            $where = $_GET['where'];
        }

        $column = null;
        if (!empty($_GET['column'])) {
            $column = $_GET['column'];
        }

        $service = $this->getServiceFactory()->create('Report');

        if (!empty($where)) {
            $where = json_decode($where, true);
        }

        $contents = $service->getCsv($id, $where, $column);

        $report = $this->getEntityManager()->getEntity('Report', $id);

        $name = $report->get('name');
        $name = str_replace(' ', '_', $name);
        $name = preg_replace("/([^\w\s\d\-_~,;:\[\]\(\).])/u", '_', $name);

        $fileName = $name . '.csv';


        ob_clean();
        header("Content-type:text/csv");
        header("Content-Disposition:attachment;filename=\"{$fileName}\"");
        echo $contents;
    }
}

