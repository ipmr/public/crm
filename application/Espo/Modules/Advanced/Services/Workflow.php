<?php
/************************************************************************
 * This file is part of EspoCRM.
 *
 * EspoCRM - Open Source CRM application.
 * Copyright (C) 2014  Yuri Kuznetsov, Taras Machyshyn, Oleksiy Avramenko
 * Website: http://www.espocrm.com
 *
 * EspoCRM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EspoCRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EspoCRM. If not, see http://www.gnu.org/licenses/.
 ************************************************************************/

namespace Espo\Modules\Advanced\Services;

use Espo\Core\Exceptions\Error;
use Espo\ORM\Entity;

class Workflow extends \Espo\Services\Record
{
    protected function init()
    {
        $this->dependencies[] = 'mailSender';
        $this->dependencies[] = 'workflowHelper';
        $this->dependencies[] = 'container';
    }

    protected function getMailSender()
    {
        return $this->getInjection('mailSender');
    }

    protected function getWorkflowHelper()
    {
        return $this->getInjection('workflowHelper');
    }

    /**
     * Send email defined in workflow
     *
     * @param  array $data  See validateSendEmailData method
     * @return bool
     */
    public function sendEmail(array $data)
    {
        if (!$this->validateSendEmailData($data)) {
            throw new Error('Workflow['.$data['workflowId'].'][sendEmail]: Email data is broken.');
        }

        $entityManager = $this->getEntityManager();

        $workflow = $entityManager->getEntity('Workflow', $data['workflowId']);
        if (!$workflow) return;

        if (!$workflow->get('isActive')) {
            return;
        }

        $entity = $entityManager->getEntity($data['entityName'], $data['entityId']);
        if (!isset($entity)) {
            throw new Error('Workflow['.$data['workflowId'].'][sendEmail]: Entity is not found.');
        }

        $entityService = $this->getServiceFactory()->create($entity->getEntityName());
        $entityService->loadAdditionalFields($entity);

        $toEmail = $this->getEmailAddress($data['to']);
        $fromEmail = $this->getEmailAddress($data['from']);
        if (empty($toEmail) || empty($fromEmail)) {
            throw new Error('Workflow['.$data['workflowId'].'][sendEmail]: Email address is empty.');
        }

        $entityHash = array(
            $data['entityName'] => $entity,
        );

        if (isset($data['to']['entityName']) && $data['to']['entityName'] != $data['entityName']) {
            $toEntity = $data['to']['entityName'];
            $entityHash[$toEntity] = $entityManager->getEntity($toEntity, $data['to']['entityId']);
        }

        if (isset($data['from']['entityName']) && $data['from']['entityName'] == 'User') {
            $entityHash['User'] = $entityManager->getEntity('User', $data['from']['entityId']);
            $fromName = $entityHash['User']->get('name');
        }

        $emailTemplateParams = array(
            'entityHash' => $entityHash,
            'emailAddress' => $toEmail,
        );
        if ($entity->hasField('parentId') && $entity->hasField('parentType')) {
            $emailTemplateParams['parentId'] = $entity->get('parentId');
            $emailTemplateParams['parentType'] = $entity->get('parentType');
        }

        $emailTemplateService = $this->getServiceFactory()->create('EmailTemplate');
        $emailTemplate = $emailTemplateService->parse($data['emailTemplateId'], $emailTemplateParams, true);

        $emailData = array(
            'from' => $fromEmail,
            'to' => $toEmail,
            'subject' => $emailTemplate['subject'],
            'body' => $emailTemplate['body'],
            'isHtml' => $emailTemplate['isHtml'],
            'parentId' => $entity->id,
            'parentType' => $entity->getEntityName(),
        );

        if (isset($fromName)) {
            $emailData['fromName'] = $fromName;
        }

        $email = $entityManager->getEntity('Email');
        $email->set($emailData);

        if (!empty($emailTemplate['attachmentsIds'])) {
            $email->set('attachmentsIds', $emailTemplate['attachmentsIds']);
            $entityManager->saveEntity($email);
        }

        $sendExceptionMessage = null;
        try {
            $result = $this->getMailSender()->send($email);
            $entityManager->saveEntity($email);
        } catch (\Exception $e) {
            $sendExceptionMessage = $e->getMessage();
        }

        if (isset($sendExceptionMessage)) {
            throw new Error('Workflow['.$data['workflowId'].'][sendEmail]: '.$sendExceptionMessage.'.');
        }

        return isset($result) ? $result : false;
    }

    public function jobTriggerWorkflow(array $data)
    {
        $entityId = $data['entityId'];
        $entityType = $data['entityType'];

        if (empty($data['entityId']) || empty($data['entityType']) || empty($data['nextWorkflowId'])) {
            throw new Error('Workflow['.$data['workflowId'].'][triggerWorkflow]: Not sufficient job data.');
        }

        $entity = $this->getEntityManager()->getEntity($entityType, $entityId);

        if (!$entity) {
            throw new Error('Workflow['.$data['workflowId'].'][triggerWorkflow]: Empty job data.');
        }

        if (is_array($data['values'])) {
            foreach ($data['values'] as $attribute => $value) {
                $entity->setFetched($attribute, $value);
            }
        }

        $this->triggerWorkflow($entity, $data['nextWorkflowId']);

        return true;
    }

    public function triggerWorkflow($entity, $workflowId)
    {
        $workflow = $this->getEntityManager()->getEntity('Workflow', $workflowId);
        if (!$workflow) return;

        if (!$workflow->get('isActive')) {
            return;
        }

        $workflowManager = $this->getInjection('container')->get('workflowManager');

        if ($workflowManager->checkConditions($workflow, $entity)) {
            $workflowLogRecord = $this->getEntityManager()->getEntity('WorkflowLogRecord');
            $workflowLogRecord->set(array(
                'workflowId' => $workflowId,
                'targetId' => $entity->id,
                'targetType' => $entity->getEntityType()
            ));
            $this->getEntityManager()->saveEntity($workflowLogRecord);

            $workflowManager->runActions($workflow, $entity);
        }
    }

    /**
     * Validate sendEmail data
     *
     * @param  array  $data
     * @return bool
     */
    protected function validateSendEmailData(array $data)
    {
        $requiredParams = array(
            'entityId',
            'entityName',
            'emailTemplateId',
            'to',
            'from',
        );

        foreach ($requiredParams as $name) {
            if (!isset($data[$name])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get email address depends on inputs
     * @param  array $data
     * @return string
     */
    protected function getEmailAddress(array $data)
    {
        if (isset($data['email'])) {
            return $data['email'];
        }

        if (isset($data['entityName']) && isset($data['entityId'])) {
            $entity = $this->getEntityManager()->getEntity($data['entityName'], $data['entityId']);
        }

        if (isset($data['type'])) {
            $workflowHelper = $this->getWorkflowHelper();

            switch ($data['type']) {
                case 'specifiedTeams':
                    $userIds = $workflowHelper->getUserIdsByTeamIds($data['teamsIds']);
                    return implode('; ', $workflowHelper->getUsersEmailAddress($userIds));
                    break;

                case 'teamUsers':
                    $entity->loadLinkMultipleField('teams');
                    $userIds = $workflowHelper->getUserIdsByTeamIds($entity->get('teamsIds'));
                    return implode('; ', $workflowHelper->getUsersEmailAddress($userIds));
                    break;

                case 'followers':
                    $userIds = $workflowHelper->getFollowerUserIds($entity);
                    return implode('; ', $workflowHelper->getUsersEmailAddress($userIds));
                    break;

                case 'followersExcludingAssignedUser':
                    $userIds = $workflowHelper->getFollowerUserIdsExcludingAssignedUser($entity);
                    return implode('; ', $workflowHelper->getUsersEmailAddress($userIds));
                    break;
            }
        }

        if ($entity instanceof Entity && $entity->hasField('emailAddress')) {
            return $entity->get('emailAddress');
        }
    }

    public function runScheduledWorkflow(array $data)
    {
        $entityManager = $this->getEntityManager();

        $workflow = $entityManager->getEntity('Workflow', $data['workflowId']);
        if (!$workflow instanceof Entity) {
            throw new Error('Workflow['.$data['workflowId'].'][runScheduledWorkflow]: Entity is not found.');
        }

        if (!$workflow->get('isActive')) {
            return;
        }

        $targetReport = $workflow->get('targetReport');
        if (!$targetReport instanceof Entity) {
            throw new Error('Workflow['.$data['workflowId'].'][runScheduledWorkflow]: TargetReport Entity is not found.');
        }

        $reportService = $this->getServiceFactory()->create('Report');
        $result = $reportService->run($targetReport->get('id'));

        $jobEntity = $entityManager->getEntity('Job');

        if (isset($result['collection']) && is_object($result['collection'])) {
            foreach ($result['collection'] as $collectionEntity) {
                $job = clone $jobEntity;
                $job->set(array(
                    'serviceName' => 'Workflow',
                    'method' => 'runScheduledWorkflowForEntity',
                    'data' => array(
                        'workflowId' => $workflow->get('id'),
                        'entityName' => $collectionEntity->getEntityName(),
                        'entityId' => $collectionEntity->get('id'),
                    ),
                    'executeTime' => date('Y-m-d H:i:s'),
                ));
                $entityManager->saveEntity($job);
            }
        }
    }

    public function runScheduledWorkflowForEntity(array $data)
    {
        $entityManager = $this->getEntityManager();

        $entity = $entityManager->getEntity($data['entityName'], $data['entityId']);
        if (!$entity instanceof Entity) {
            throw new Error('Workflow['.$data['workflowId'].'][runActions]: Entity['.$data['entityName'].'] ['.$data['entityId'].'] is not found.');
        }

        $this->triggerWorkflow($entity, $data['workflowId']);
    }
}

