/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

Espo.define('Advanced:Views.Record.ListForReport', 'Views.Record.List', function (Dep) {

    return Dep.extend({

        checkAllResultMassActionList: ['export'],

        export: function () {
            var data = {};
            if (this.allResultIsChecked) {
                data.id = this.options.reportId;

                if ('runtimeWhere' in this.options) {
                    data.where = this.options.runtimeWhere
                }
                if ('groupValue' in this.options) {
                    data.groupValue = this.options.groupValue
                }

                $.ajax({
                    url: 'Report/action/exportList',
                    type: 'GET',
                    data: data,
                    success: function (data) {
                        if ('id' in data) {
                            window.location = '?entryPoint=download&id=' + data.id;
                        }
                    }
                });
            } else {
                data.ids = this.checkedList;

                $.ajax({
                    url: this.scope + '/action/export',
                    type: 'GET',
                    data: data,
                    success: function (data) {
                        if ('id' in data) {
                            window.location = '?entryPoint=download&id=' + data.id;
                        }
                    }
                });
            }
        },

    });

});